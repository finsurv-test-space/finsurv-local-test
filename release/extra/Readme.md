# Optional extra files

The files in this directory are provided optionally as part of the deployment.  Please only execute scripts
in this directory after consultation with Synthesis