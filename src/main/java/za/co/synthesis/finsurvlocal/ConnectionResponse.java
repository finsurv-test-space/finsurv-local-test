package za.co.synthesis.finsurvlocal;

import java.util.List;
import java.util.Map;

public class ConnectionResponse {
    private String channelName;
    private String responseBody;
    private Map<String, List<String>> responseHeaders;
    private int responseCode;

    @Override
    public String toString() {
        return "\nChannel Name='" + channelName + '\'' +
                "\nResponse Body='" + responseBody + '\'' +
                "\nResponse Headers=" + responseHeaders +
                "\nResponse Code=" + responseCode +
                '}';
    }

    public ConnectionResponse(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Map<String, List<String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
