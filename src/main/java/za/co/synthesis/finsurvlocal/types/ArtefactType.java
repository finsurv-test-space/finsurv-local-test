package za.co.synthesis.finsurvlocal.types;

//TODO: Complete (client-friendly) java doc for the code below...

public enum ArtefactType {
    FORM_JS("producer/api/rules/{channelName}/download/js", "form.js"),
    EVALUATION_RULES("producer/api/rules/{channelName}/download/evaluation", "evaluation.js"),
    VALIDATION_RULES("producer/api/rules/{channelName}/download/validation", "validation.js"),
    DOCUMENT_RULES("producer/api/rules/{channelName}/download/document", "document.js"),
    EXTERNAL_VALIDATION_CONFIG("producer/api/rules/{channelName}/download/externalValidations", "externalValidation.js"),
    LOOKUPS("producer/api/rules/{channelName}/download/lookups", "lookups.js");

   public String apiPath;
   public String filename;
   public String name;
   public String description;

    ArtefactType (String apiPath, String filename){
        this.apiPath = apiPath;
        this.filename = filename;
    }

    public static ArtefactType fromString(String n){
        if ("validation".equalsIgnoreCase(n.substring(0,10))){
            return ArtefactType.VALIDATION_RULES;
        } else if ("evaluation".equalsIgnoreCase(n.substring(0,10))){
            return ArtefactType.EVALUATION_RULES;
        } else if ("document".equalsIgnoreCase(n.substring(0,8))){
            return ArtefactType.DOCUMENT_RULES;
        } else if ("form".equalsIgnoreCase(n.substring(0,4))){
            return ArtefactType.FORM_JS;
        } else if ("external".equalsIgnoreCase(n.substring(0,8))){
            return ArtefactType.EXTERNAL_VALIDATION_CONFIG;
        }
        return null;
    }

    public String getApiPath() {
        return apiPath;
    }

    public String getFilename() {
        return filename;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
