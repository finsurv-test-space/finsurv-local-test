package za.co.synthesis.finsurvlocal.types;

import java.util.List;
import java.util.Map;

//TODO: Complete (client-friendly) java doc for the code below...

public class ArtefactResponse {
    public String channelName;
    public ArtefactType artefactType;
    public String responseBody;
    public Map<String, List<String>> responseHeaders;
    public int responseCode;

    @Override
    public String toString() {
        return "ArtefactResponse{" +
                "channelName='" + channelName + '\'' +
                ", artefactType=" + artefactType +
                ", responseBody='" + responseBody + '\'' +
                ", responseHeaders=" + responseHeaders +
                ", responseCode=" + responseCode +
                '}';
    }

    public ArtefactResponse(String channelName, ArtefactType artefactType) {
        this.channelName = channelName;
        this.artefactType = artefactType;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public ArtefactType getArtefactType() {
        return artefactType;
    }

    public void setArtefactType(ArtefactType artefactType) {
        this.artefactType = artefactType;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Map<String, List<String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
