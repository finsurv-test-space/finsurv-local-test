package za.co.synthesis.finsurvlocal.types;

import java.net.MalformedURLException;
import java.net.URL;

//TODO: Complete (client-friendly) java doc for the code below...

public class RdsConfiguration {
    private String protocol;
    private String server;
    private String port;
//    private String reportServicesURL;
    private String path;

    public RdsConfiguration(String protocol, String server, String port, String path) {
        this.protocol = protocol;
        this.server = server;
        this.port = port;
        this.path = path;
    }
    public RdsConfiguration(String url) {
        setConfiguration(url);
    }
    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl(){
        String reportServicesURL = protocol+"://"+server+":"+port+path;
        return reportServicesURL;
    }

    public void setConfiguration(String urlString) {
        try {
            URL url = new URL(urlString);
            this.port = Integer.toString(url.getPort());
            this.protocol = url.getProtocol();
            this.path = url.getPath();
            this.server = url.getHost();
        }catch (Exception exception){

        }
    }
}
