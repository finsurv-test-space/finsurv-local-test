package za.co.synthesis.finsurvlocal.types;

public interface IFinsurvRunnable extends Runnable{
    public enum FinsurvRunnableState{
        NEW,
        BUSY,
        COMPLETE,
        ERROR,
        IDLE;
    }


    public FinsurvRunnableState getStatus();
    public FinsurvRunnableState setBusyStatus();
    public FinsurvRunnableState setCompleteStatus();
    public FinsurvRunnableState setErrorStatus();
    public FinsurvRunnableState setStatus(FinsurvRunnableState newState);
    public FinsurvRunnableState setIdleStatus();

}
