package za.co.synthesis.finsurvlocal.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.validation.FinsurvEngineInstancePool;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.Rule;
import za.co.synthesis.rule.support.Util;

import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static za.co.synthesis.finsurvlocal.utils.JsonUtils.jsonStrToMap;

//TODO: Complete (client-friendly) java doc for the code below...
public class Document {
    public static Logger logger = LoggerFactory.getLogger(Document.class);

    public static int defaultValidatorInstancesPerPackage = 5;
    public static int defaultValidatorExpiryMinutesPerPackage = 60;
    public static Map<String, FinsurvEngineInstancePool<Validator>> packageValidators = new ConcurrentHashMap<String, FinsurvEngineInstancePool<Validator>>();

    /**
     *  This function retrieves a more specific extensive list of potential documents needed based on the bop category code entered.
     * @param channelName
     * @param category
     * @param flow
     * @return
     * @throws Exception
     */
    public static List<String> getPotentialDocs(String channelName,
                                                String category,
                                                String flow) throws Exception {
        return getPotentialDocuments(channelName, category, flow, "", CommonHelperFunctions.getArtefactLookups(channelName), CommonHelperFunctions.getArtefactDocumentRules(channelName));
    }

    /**
     * This function returns a more specific extensive list of potential documents needed based on the bop category code entered.
     * @param channelName
     * @param category
     * @param flow
     * @param section
     * @param lookups
     * @param documentRules
     * @return
     * @throws Exception
     */
    public static List<String> getPotentialDocuments(String channelName,
                                                     String category,
                                                     String flow,
                                                     String section,
                                                     String lookups,
                                                     String documentRules) throws Exception {
        List<String> results = new ArrayList<String>();

        ValidationEngine engine = new ValidationEngine();
        engine.setupLoadLookup(channelName,new StringReader(lookups));
        engine.setupLoadRule(channelName,new StringReader(documentRules));
        DefaultSupporting supporting = new DefaultSupporting();
        supporting.setGoLiveDate(Util.date("2013-08-19"));

        //  The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.

        supporting.setCurrentDate(LocalDate.now());

        engine.setupSupporting(supporting);
        engine.setIgnoreMissingCustomValidate(true);

        List<ResultEntry> documentResults = null;
        try {
            List<Rule> rules = engine.getPotentialDocuments(category, FlowType.fromString(flow));
            for(Rule rule :rules){
                //only add "unique" items...
                String item = rule.getCode()+", "+rule.getMessage();
                if (!results.contains(item)) {
                    results.add(item);
                }
            }
        }
        catch (Exception e) {
            logger.error("Cannot determine documents for transaction - "+e.getMessage());
        }

        return results;
    }



    public static List<String> getRequiredDocs(String bopData, String channelName) throws Exception {
        return getRequiredDocuments(bopData, CommonHelperFunctions.getArtefactLookups(channelName), channelName, CommonHelperFunctions.getArtefactDocumentRules(channelName));
    }



    public static synchronized Validator getDocumentValidator(String channelName, String lookups, String documentRules) throws Exception {
        List<String> results = new ArrayList<String>();

        HashMap<String, String> scopedDocInfo = new HashMap<String, String>();

        ValidationEngine engine = new ValidationEngine();
        engine.setupLoadLookup(channelName,new StringReader(lookups));
        engine.setupLoadRule(channelName,new StringReader(documentRules));
        DefaultSupporting supporting = new DefaultSupporting();
        supporting.setGoLiveDate(Util.date("2013-08-19"));

        //The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
        /*#if OLDDATE

                supporting.setCurrentDate(LocalDate.now());
#else*/
        supporting.setCurrentDate(LocalDate.now());
//#endif
        engine.setupSupporting(supporting);
        engine.setIgnoreMissingCustomValidate(true);

        ValidationResultsAndStats documentResults = null;
        try {
            Validator validator = engine.issueValidator();
            return validator;
        }
        catch (Exception e) {
            logger.error("Cannot determine documents for transaction - "+e.getMessage());
            throw e;
        }

    }

    public static String getDocumentValidationRules(String packageName){
        return CommonHelperFunctions.getArtefactDocumentRules(packageName);
    }

    public static synchronized Validator getValidatorForPackage(String packageName) {
        if (packageValidators.containsKey(packageName)){
            return packageValidators.get(packageName).getInstance();
        } else {
            ArrayList<Validator> initialInstances = new ArrayList<Validator>();
            for (int index = 0; index < defaultValidatorInstancesPerPackage; index ++) {
                try {
                    Validator validator = getDocumentValidator(packageName,
                            CommonHelperFunctions.getArtefactLookups(packageName),
                            getDocumentValidationRules(packageName));
                   initialInstances.add(validator);
                } catch (Exception e) {
                    return null;
                }
            }
            FinsurvEngineInstancePool<Validator> instancePool = new FinsurvEngineInstancePool<Validator>(packageName, defaultValidatorExpiryMinutesPerPackage, initialInstances);
            packageValidators.put(packageName, instancePool);
            return instancePool.getInstance();
        }
    }

    public static ValidationResultsAndStats validateBopJson(String bopData, String packageName) throws Exception {
        Validator validator = getValidatorForPackage(packageName);
        try
        {
            ValidationResultsAndStats results = validator.validateJson(bopData);
            return results;
        }catch (Exception e){
            throw e;
        }
    }

    /**
     * This function takes the bop data into account and returns the documents that is still outstanding
     *
     * @param bopData
     * @param lookups
     * @param channelName
     * @param documentRules
     * @return
     * @throws Exception
     */
    public static List<String> getRequiredDocuments(String bopData, String lookups, String channelName, String documentRules) throws Exception {
        List<String> results = new ArrayList<String>();

        HashMap<String, String> scopedDocInfo = new HashMap<String, String>();

        ValidationResultsAndStats documentResults = null;
        try {
            documentResults = validateBopJson(bopData,channelName);
            if (documentResults != null) {
                try {
                        Map<String, Object> bopObj = jsonStrToMap(bopData);
                        Map<String, Object> reportData =  (Map<String, Object>)(bopObj != null && bopObj.containsKey("Report")?bopObj.get("Report"):null);
                        for (ResultEntry entry : documentResults.getResults()) {
                            if (entry.getType() == ResultType.Document) {
                                Integer moneyIndex = entry.getMoneyInstance() >= 0 ? entry.getMoneyInstance()+1 : null;
                                Integer ieIndex = entry.getImportExportInstance() >= 0 ? entry.getImportExportInstance()+1 : null;
                                String scopeKey = getDocumentScopeKey((String)((reportData instanceof Map && reportData.containsKey("TrnReference"))?reportData.get("TrnReference"):"UNKNOWN-TRN"),
                                        entry.getScope().getName(), moneyIndex, ieIndex, entry.getCode());

                                String documentStr = "{'Key':'"+scopeKey+"', 'DocumentType':'"+entry.getCode()+"', 'Scope':'"+entry.getScope()+"', 'Description':'"+entry.getMessage()+"'}";
                                scopedDocInfo.put(scopeKey, documentStr);
                            }
                        }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            logger.error("Cannot determine documents for transaction - "+e.getMessage());
        }

        results.addAll(scopedDocInfo.values());
        return results;
    }

    public static String getDocumentScopeKey(String trnReference, String scope, Integer sequence, Integer subSequence, String type){
        scope = scope==null?"adhoc":scope;
        type = type==null?"document":type;
        String scopeKey = trnReference+"."+scope;
        scopeKey += ((scope.equals("money") || scope.equals("importexport"))
                ? ((sequence != null) ? "." + sequence : "")
                : "");
        scopeKey += ((scope.equals("importexport") && subSequence != null)
                ? "." + subSequence
                : "");
        scopeKey += "." + type;
        return scopeKey;
    }

    public static void clearCachedValidators(){
        packageValidators.clear();
    }
}
