package za.co.synthesis.finsurvlocal.document.types;

import za.co.synthesis.rule.core.Scope;

/**
 * Created by jake on 5/29/17.
 */
//TODO: Complete (client-friendly) java doc for the code below...
public class DocumentInfo extends DocumentData {
  private String description;
  private Boolean uploaded;

  public DocumentInfo(DocumentData documentData, Boolean uploaded) {
    this(documentData.getDocumentHandle(),
            documentData.getReportingSpace(),
            documentData.getTrnReference(),
            documentData.getScope(),
            documentData.getType(),
            documentData.getDescription(),
            documentData.getSequence(),
            documentData.getSubSequence(),
            uploaded);
    setAcknowledged(documentData.getAcknowledged());
    setAcknowledgedComment(documentData.getAcknowledgedComment());
    setFilename(documentData.getFileName());
    setFileType(documentData.getFileType());
  }

  public DocumentInfo(DocumentData documentData) {
    this(documentData.getDocumentHandle(),
            documentData.getReportingSpace(),
            documentData.getTrnReference(),
            documentData.getScope(),
            documentData.getType(),
            documentData.getDescription(),
            documentData.getSequence(),
            documentData.getSubSequence(),
            documentData.getFileName() != null && !documentData.getFileName().isEmpty());
    setAcknowledged(documentData.getAcknowledged());
    setAcknowledgedComment(documentData.getAcknowledgedComment());
    setFilename(documentData.getFileName());
    setFileType(documentData.getFileType());
  }

  public DocumentInfo(String documentHandle, String reportingSpace, String trnReference, Scope scope, String type, String description, Integer sequence, Integer subSequence, Boolean uploaded) {
    super(documentHandle, reportingSpace, trnReference, scope, type, description, sequence, subSequence);
    this.description = description;
    this.uploaded = uploaded;
  }

  //---------------------//
  //-----  GETTERS  -----//
  //---------------------//

  public String getDocumentHandle() {
    return super.getDocumentHandle();
  }

  public String getReportingSpace() {
    return super.getReportingSpace();
  }

  public String getTrnReference() {
    return super.getTrnReference();
  }

  public Scope getScope() {
    return super.getScope();
  }

  public String getType() {
    return super.getType();
  }

  public String getDescription() {
    return description;
  }

  public Integer getSequence() {
    return super.getSequence();
  }

  public Integer getSubSequence() {
    return super.getSubSequence();
  }

  public Boolean getUploaded() {
    return uploaded;
  }

  public Boolean getAcknowledged() {
    return super.getAcknowledged() == true;
  }

  public String getAcknowledgedComment() {
    return super.getAcknowledgedComment();
  }

  public String getFileName() {
    return super.getFileName();
  }

  public String getFileType() {
    return super.getFileType();
  }


  //---------------------//
  //-----  SETTERS  -----//
  //---------------------//

  public void setDocumentHandle(String documentHandle) {
    super.setDocumentHandle(documentHandle);
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setUploaded(Boolean uploaded) {
    this.uploaded = uploaded;
  }

  public void setAcknowledged(Boolean acknowledged) {
    super.setAcknowledged(acknowledged);
  }

  public void setAcknowledgedComment(String acknowledgedComment) {
    super.setAcknowledgedComment(acknowledgedComment);
  }

  public void setFilename(String fileName){
    super.setFilename(fileName);
  }

  public void setFileType(String fileType){
    super.setFileType(fileType);
  }


}
