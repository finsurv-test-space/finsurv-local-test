package za.co.synthesis.finsurvlocal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.document.Document;
import za.co.synthesis.finsurvlocal.evaluation.EvaluateRunnable;
import za.co.synthesis.finsurvlocal.report.GenerateBopReport;
import za.co.synthesis.finsurvlocal.report.ReportServicesApi;
import za.co.synthesis.finsurvlocal.evaluation.Evaluation;
import za.co.synthesis.finsurvlocal.task.ArtefactBackgroundWorker;
import za.co.synthesis.finsurvlocal.types.ArtefactResponse;
import za.co.synthesis.finsurvlocal.types.ArtefactType;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.types.RdsConfiguration;
import za.co.synthesis.finsurvlocal.utils.*;
import za.co.synthesis.finsurvlocal.validation.ValidateRunnable;
import za.co.synthesis.finsurvlocal.validation.Validation;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.rule.core.*;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
// The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
/*#if OLDDATE
import za.co.synthesis.rule.support.legacydate.LocalDate;
import za.co.synthesis.rule.support.legacydate.LocalDateTime;
import za.co.synthesis.rule.support.legacydate.format.DateTimeFormatter;
import za.co.synthesis.rule.support.legacydate.format.DateTimeParseException;
#else*/
//#endif

import static za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions.getArtefactContent;
import static za.co.synthesis.finsurvlocal.utils.HistoryUtils.*;
import static za.co.synthesis.finsurvlocal.utils.JsonUtils.mapToJsonStr;

/**
 * <h2>Local Library to provide Finsurv Functionality</h2>
 *
 * The FinsurvLocal library provides the relevant &quot;Evaluation&quot;, &quot;Validation&quot;, &quot;Document&quot; sighting and other such Finsurv Functionality.
 * In additiona to the above, there are helper functions and features provided to make any Finsurv implementation easier for the host system.
 * Many of the functions, features and datatypes have been provided with re-use in mind and can be leveraged for other purposes.
 *
 * Notably, the following items and related code can be re-used for other purposes:
 * <ul>
 *     <li>Freemarker Templating and associated helper functions</li>
 *     <li>JSON helper functions</li>
 *     <li>Thread pooling and associated helper functions</li>
 * </ul>
 *
 *
 * The library is specifically geared to enable integration to Synthesis' TXstream Finsurv Application Suite - in particular, it will directly integrate with the
 * Report Data Store application.
 */
public class FinsurvLocal {

    public static Logger logger = LoggerFactory.getLogger(FinsurvLocal.class);
    public static String artefactDirectory = "producer/api/rules";
    /* TODO: Convert the channels List to a Map - so we can store additional channel-specific parameters and objects.
        This will allow us to store such information as:
         - how many validator instances to create for the instance pool,
         - number of threads to support,
         - external validator objects etc etc etc.

         ..this could be facilitated by JSON files with the necessary configurations per channel... would require mechanisms to load config via function (setChannelParams), constructor... or read from and persist to disk (maybe)         */
    public static Map<String,Map<String, Object>> channels = new ConcurrentHashMap<String,Map<String, Object>>();
    public static String rdsURL;
    public static String username;
    public static String password;
    public static String jwt;
    public static String USER_AGENT = "FinsurvLocal(Java)";
    public static String utf8 = "UTF-8";
    public static String finsurvReportTemplateDirectory = null;
    public static String rootFinsurvReportTemplate = null;
    public static RdsConfiguration rdsConfiguration = null;
    public static volatile FinsurvLocal currentInstance;
    public static ArtefactBackgroundWorker artefactBackgroundWorker;

    /**
     * The maximum number of threads to allocate to the threadpool used for processing.
     * if set to <= 0, the system will calculate the maximum number of threads based on
     * the number of (logical) processor cores available, in conjunction with the
     * threadPoolCoresReducer and the threadPoolCoresMultiplier.
     * Formula derived as follows:
     * Max Threads = MAX(1, (cores - ABS(threadPoolCoresReducer))) * MAX(1, threadPoolCoresMultiplier)
     *
     * For example...:
     * Scenario #1:
     *      (threadPoolMaxSize = 0)
     *      cores = 12
     *      threadPoolCoresReducer = 2
     *      threadPoolCoresMultiplier = 1
     *  then, threadPoolMaxSize = (12 - 2) * 1
     *                          == 10 threads
     *
     * Scenario #2:
     *      (threadPoolMaxSize = 0)
     *      cores = 12
     *      threadPoolCoresReducer = 0
     *      threadPoolCoresMultiplier = 4
     *  then, threadPoolMaxSize = (12 - 0) * 4
     *                          == 48 threads
     *
     * Scenario #3:
     *      (threadPoolMaxSize = 0)
     *      cores = 12
     *      threadPoolCoresReducer = -20
     *      threadPoolCoresMultiplier = 0
     *  then, threadPoolMaxSize = MAX(1, (12 - ABS(-20))) * MAX(1, 0)
     *                          = MAX(1, (12 - 20)) * 1
     *                          = MAX(1, -8) * 1
     *                          = 1 * 1
     *                          == 1 thread
     *
     */
    public static volatile ExecutorService processingThreadPool;
    public static volatile int processingThreadPoolMaxSize = 0;
    public static volatile int processingThreadPoolCoresReducer = 0;
    public static volatile int processingThreadPoolCoresMultiplier = 1;
    public static volatile int processingThreadPoolShutdownAwaitMinutes = 1;


    public static volatile ExecutorService submissionThreadPool;
    public static volatile int submissionThreadPoolMaxSize = 0;
    public static volatile int submissionThreadPoolCoresReducer = 0;
    public static volatile int submissionThreadPoolCoresMultiplier = 20;
    public static volatile int submissionThreadPoolShutdownAwaitMinutes = 5;

    public static volatile ExecutorService taskThreadPool;
    public static volatile int taskThreadPoolMaxSize = 1;
    public static volatile int taskThreadPoolCoresReducer = 0;
    public static volatile int taskThreadPoolCoresMultiplier = 1;
    public static volatile int taskThreadPoolShutdownAwaitMinutes = 5;


/*
TODO: We should be able to specify either a directory to store artefacts into, or a memory mechanism (ie: Map) and other parameters such as how often to refresh etc etc.
    We also need to provide support for other auth mechanisms, connection types etc
 */
    /**
     * Constructor method to set all required string parameters for the FinsurvLocal Object
     * @param artefactDirectory This is the directory in which the channel-specific artefacts can be found.
     *                          Artefacts for each relevant, specific channel are expected to be kept within a sub-directory named with the channelName.
     *                          Artefacts expected in this sub-directory are as follows:
     *                          <ul>
     *                              <li><b>form.js</b>: <p>The self-contained JS rules engine and BOP Form (css, form partials, angular library and other dependencies)</p></li>
     *                              <li><b>lookups.js</b>: <p>JSON formatted lookup data.  This will contain any static and/or semi-static lookup data that is either used by the BOP form, rules engine or rules themselves.</p></li>
     *                              <li><b>evaluation.js</b>: <p>The Finsurv rules used for performing <i>Evaluations</i> on transactions - to determine the reportability of the transaction as well as any specific parameter values that should be used when reporting to the regulator.</p></li>
     *                              <li><b>validation.js</b>: <p>The Finsurv rules used for <i>Validation</i> of the BOP Report data - to determine completeness and correctness of the data provided in accordance with the regulatory specifications.</p></li>
     *                              <li><b>externalValidation.js</b>: <p>The External Validation Header Configuration file used by the rules engine(s) to register and make use of external services and databases for validation of specific fields and data in the BOP Report.  Typically, external validation calls would only be required for validating data that is maintained or stored in a centralised system or where a centralised system performs the necessary calculations to validate the BOP data provided.  Examples of such calls are: ARM, IVS, Transaction reversals, SDA and FIA allowances etc.</p></li>
     *                              <li><b>document.js</b>: <p>The rules used to determine the required excon documentation, based on the provided BOP Report data and the configured requirements for the specific channel.</p></li>
     *                          </ul>
     *                          The directory structure will typically conform to the same structure as the API path(s) on the TXstream Finsurv Report Data Store APIs for the artefacts mentioned above - <b>&quot;producer/api/rules/[channelName]/[artefactName]&quot;</b>.
     *
     * @param rdsURL The base-url for the TXstream Finsurv Report Data Store application.
     * @param username The username used when logging into TXstream Finsurv Report Data Store.
     * @param password The password used when logging into TXstream Finsurv Report Data Store.
     */
    public FinsurvLocal(String artefactDirectory, String rdsURL, String username, String password, String finsurvReportTemplateDirectory, String rootFinsurvReportTemplate){
        this.artefactDirectory = artefactDirectory;
        this.rdsURL = rdsURL;
        this.username = username;
        this.password = password;
        this.finsurvReportTemplateDirectory = finsurvReportTemplateDirectory;
        this.rootFinsurvReportTemplate = rootFinsurvReportTemplate;
        this.rdsConfiguration= new RdsConfiguration(rdsURL);
        currentInstance = this;
    }

    /**
     * Constructor method to set all required string parameters for the FinsurvLocal Object
     * @param artefactDirectory This is the directory in which the channel-specific artefacts can be found.
     *                          Artefacts for each relevant, specific channel are expected to be kept within a sub-directory named with the channelName.
     *                          Artefacts expected in this sub-directory are as follows:
     *                          <ul>
     *                              <li><b>form.js</b>: <p>The self-contained JS rules engine and BOP Form (css, form partials, angular library and other dependencies)</p></li>
     *                              <li><b>lookups.js</b>: <p>JSON formatted lookup data.  This will contain any static and/or semi-static lookup data that is either used by the BOP form, rules engine or rules themselves.</p></li>
     *                              <li><b>evaluation.js</b>: <p>The Finsurv rules used for performing <i>Evaluations</i> on transactions - to determine the reportability of the transaction as well as any specific parameter values that should be used when reporting to the regulator.</p></li>
     *                              <li><b>validation.js</b>: <p>The Finsurv rules used for <i>Validation</i> of the BOP Report data - to determine completeness and correctness of the data provided in accordance with the regulatory specifications.</p></li>
     *                              <li><b>externalValidation.js</b>: <p>The External Validation Header Configuration file used by the rules engine(s) to register and make use of external services and databases for validation of specific fields and data in the BOP Report.  Typically, external validation calls would only be required for validating data that is maintained or stored in a centralised system or where a centralised system performs the necessary calculations to validate the BOP data provided.  Examples of such calls are: ARM, IVS, Transaction reversals, SDA and FIA allowances etc.</p></li>
     *                              <li><b>document.js</b>: <p>The rules used to determine the required excon documentation, based on the provided BOP Report data and the configured requirements for the specific channel.</p></li>
     *                          </ul>
     *                          The directory structure will typically conform to the same structure as the API path(s) on the TXstream Finsurv Report Data Store APIs for the artefacts mentioned above - <b>&quot;producer/api/rules/[channelName]/[artefactName]&quot;</b>.
     *
     * @param rdsConfiguration base configuration object for the TXstream Finsurv Report Data Store instance
     * @param username The username used when logging into TXstream Finsurv Report Data Store.
     * @param password The password used when logging into TXstream Finsurv Report Data Store.
     */
    public FinsurvLocal(String artefactDirectory, RdsConfiguration rdsConfiguration, String username, String password, String finsurvReportTemplateDirectory, String rootFinsurvReportTemplate){
        this.artefactDirectory = artefactDirectory;
        this.rdsURL = rdsConfiguration.getUrl();
        this.username = username;
        this.password = password;
        this.finsurvReportTemplateDirectory = finsurvReportTemplateDirectory;
        this.rootFinsurvReportTemplate = rootFinsurvReportTemplate;
        currentInstance = this;
    }


    /**
     * Checks to see if an instance of Finsurv Local has been instantiated, if not, create and return a primed instance
     * @return a primed instance of Finsurv Local
     */
    public static FinsurvLocal getInstance(){
        synchronized(FinsurvLocal.class){
            if (currentInstance == null){
                currentInstance = new FinsurvLocal(artefactDirectory, rdsURL, username, password, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);
            }
        }
        return currentInstance;
    }

    /**
     * Configures the list of channelNames to be managed and supported by the FinsurvLocal instance.
     * The parameters for each channel will be NULL, and would either need to be configured separately or the defaults will be uniformly applied as needed.
     * @param channels a list containing the set of channelNames to be managed and/or maintained by the Finsurv Local instance.
     */
    public void setChannels(List<String> channels){
        ConcurrentHashMap<String, Map<String, Object>> newList = new ConcurrentHashMap<String, Map<String, Object>>();
       for (String channel : channels) {
           newList.put(channel, null);
       }
       this.channels = newList;
    }

    /**
     * Configures the list of channelNames to be managed and supported by the FinsurvLocal instance.
     * @param channels a list containing the set of channelNames to be managed and/or maintained by the Finsurv Local instance.
     */
    public void setChannels(Map<String, Map<String, Object>> channels){
        /*TODO: define the channel params schema (as a JSON schema).  Possible params:
            -   Validator instance count
            -   Validator instance TTL (minutes)
            -   Validator thread count
            -   Validator Ext objects (implement some interface?)
            - Evaluator...
            - Document...
            - artefact caching strategy/mechanism, refresh/TTL etc etc etc
         */
       ConcurrentHashMap<String, Map<String, Object>> newList = new ConcurrentHashMap<String, Map<String, Object>>();
       newList.putAll(channels);
       this.channels = newList;
    }

    /**
     * Configures the list of channelNames to be managed and supported by the FinsurvLocal instance.
     * @param channels an array containing the set of channelNames to be managed and/or maintained by the Finsurv Local instance.
     */
    public void addChannels(String[] channels){
        for (String channel: channels){
            this.channels.put(channel, new ConcurrentHashMap<String, Object>());
        }
    }

    /**
     * Configures (appends) the list of channelNames to be managed and supported by the FinsurvLocal instance.
     * @param channels a list containing the set of channelNames to be appended to the currently configured list to be managed and/or maintained by the Finsurv Local instance.
     */
    public void addChannels(List<String> channels){
        for (String channel: channels){
            this.channels.put(channel, new ConcurrentHashMap<String, Object>());
        }
    }

    /**
     * Configures (appends) the list (Map) of channels and associated params, to be managed and supported by the FinsurvLocal instance.
     * @param channels a map containing the set of channels and their associated parameters to be appended to the currently configured channels to be managed and/or maintained by the Finsurv Local instance.
     */
    public void addChannels(Map<String, Map<String, Object>> channels){
        this.channels.putAll(channels);
    }

    /**
     * Configures (appends) the channelName to be managed and supported by the FinsurvLocal instance.
     * @param channelName the channelName to be appended to the currently configured list to be managed and/or maintained by the Finsurv Local instance.
     */
    public void addChannel(String channelName){
        if (!this.channels.containsKey(channelName)) {
            this.channels.put(channelName, new ConcurrentHashMap<String, Object>());
        }
    }

    /**
     *  Returns the channel list of channels (channelNames) currently configured to be supported by the FinsurvLocal Object instance
     * @return List of channelNames
     */
    public Map<String, Map<String, Object>> getChannels(){
        return this.channels;
    }

    /**
     *  Returns the channel list of channels (channelNames) currently configured to be supported by the FinsurvLocal Object instance
     * @return List of channelNames
     */
    public List<String> getChannelNames(){
        List names = new ArrayList<String>();
        for (String name: this.channels.keySet().toArray(new String[0])){
            names.add(name);
        }
        return names;
    }

    /**
     * NOT IMPLEMENTED YET
     * @param path
     * @return
     */
    public List<String> loadChannelsFromPath(String path){
        /*TODO: COMPLETE THE  loadChannelsFromPath FUNCTION
            check if path exists
            see if it is directory
            go into and check subdirectory
            see if it contains eval and val rules
            if contains above, add to list to return
         */

        return null;
    }

    /**
     * This function will cycle through the full list of channelNames currently configured to be managed and/or maintained by the FinsurvLocal instance, and will sequentially download new copies of the specific channel artefacts for each.
     * Upon downloading, the artefacts are timestamped, compared and checksum-tested for integrity.
     * If acceptable, the artefacts will be copied over to replace the current copies of the chennalSpecific artefacts.
     * The original artefact copies will be kept in the timestamped sub-directories.
     */
    public void updateAllChannelsArtefacts(){
        for (String channel: channels.keySet()){
            updateChannelArtefacts(channel);
        }
    }

    /**
     * This function will refresh the artefacts for the given channelName.
     * If the channelName is not already in the list to be managed/maintained by the FinsurvLocal instance, it will be added automatically.
     * Upon downloading, the artefacts are timestamped, compared and checksum-tested for integrity.
     * If acceptable, the artefacts will be copied over to replace the current copies of the chennalSpecific artefacts.
     * The original artefact copies will be kept in the timestamped sub-directories.
     * @param channelName
     */
    public void updateChannelArtefacts(String channelName){
        String ps = File.separator;
        //Ensure the requested channelName is registered as a channel to be managed/maintained by this FinsurvLocal instance.
        addChannel(channelName);

        //  The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
       /*#if OLDDATE
         File dir = new File(artefactDirectory+ps+channelName+ps);
         File tempDir = new File(dir.getPath()+ ps + getTimeStamp()+ps);
#else*/
        File dir = Paths.get(artefactDirectory, channelName,ps).toFile();
        File tempDir = Paths.get(dir.getPath(),getTimeStamp(),ps).toFile();

//#endif

        if (tempDir.mkdirs()) {
            for (ArtefactType artefact : ArtefactType.values()) {
                try {
                    ArtefactResponse response = getArtefactResponse(channelName, artefact);
                    logger.info("Response from connection:\n"+response.toString());
                    String artefactContent = response.getResponseBody();
                    if (artefactContent instanceof String) {

                        // The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
                        /*#if OLDDATE
                            File targetFile = new File(dir.getAbsolutePath()+ps+artefact.filename+ps);
                            File tempFile = new File(tempDir.getAbsolutePath()+ps+artefact.filename+ps);
#else*/
                            File targetFile = Paths.get(dir.getAbsolutePath(), artefact.filename).toFile();
                            File tempFile = Paths.get(tempDir.getAbsolutePath(),artefact.filename).toFile();

//#endif

                        if (tempFile != null && (tempFile.canWrite() || tempFile.createNewFile())) {
                            FileWriter f = new FileWriter(tempFile, false);
                            f.write(artefactContent);
                            f.flush();
                            f.close();
                        }
                        if (targetFile == null) {
                            logger.error("Unable to acquire handle on file: "+targetFile.getAbsolutePath());
                        } else {
                            if (mustUpdateTargetFile(artefactContent, targetFile)) {
                                if (targetFile.canWrite() || targetFile.createNewFile() ) {
                                    FileWriter f = new FileWriter(targetFile, false);
                                    f.write(artefactContent);
                                    f.flush();
                                    f.close();
                                } else {
                                    logger.error("Unable to overwrite or create artefact file: "+targetFile.getAbsolutePath());
                                }
                            }
                        }
                    }
                } catch (Exception err) {
                    logger.error(err.getMessage());
                }
            }
        }
    }

    /**
     * Builds up a response which contains the body (content), the response code and the response headers from the artefact connection
     * @param channelName
     * @param artefact
     * @return
     * @throws Exception
     */
    private ArtefactResponse getArtefactResponse(String channelName, ArtefactType artefact) throws Exception{
        ArtefactResponse response = new ArtefactResponse(channelName, artefact);
        HttpURLConnection artefactConnection = getArtefactConnection(channelName, artefact);
        response.setResponseCode(artefactConnection.getResponseCode());
        response.setResponseHeaders(artefactConnection.getHeaderFields());
        String artefactContent = ConnectionUtil.getHttpConnectionBody(artefactConnection);//fetchArtefact(channelName, artefact);
        response.setResponseBody(artefactContent);
        return response;
    }

    /**
     * Create a ConnectionResponse object from the given connection HttpURLConnection object and channelName
     * @param channelName
     * @param connection
     * @return
     * @throws Exception
     */
    public ConnectionResponse getHttpConnectionResponse(String channelName, HttpURLConnection connection) throws Exception{
        return ConnectionUtil.getHttpConnectionResponse(channelName,connection);
    }


    /**
     * Create a ConnectionResponse object from the given connection HttpsURLConnection object and channelName
     * @param channelName
     * @param connection
     * @return
     * @throws Exception
     */
    public ConnectionResponse getHttpsConnectionResponse(String channelName, HttpsURLConnection connection) throws Exception{
        return ConnectionUtil.getHttpsConnectionResponse(channelName,connection);
    }


    /**
     * This function is used to return a String object by making the relevant connection as per the provided parameters and returning the retrieved bytes as a UTF-8 string.
     * @param httpRequestMethod
     * @param urlTemplate
     * @param payloadTemplate
     * @param headerTemplates
     * @param params
     * @param sslContext
     * @param keystoreTemplates
     * @param truststoreTemplates
     * @param reportData
     * @return
     * @throws Exception
     */
    public String getHttpsConnection(String httpRequestMethod,
                                     String urlTemplate,
                                     String payloadTemplate,
                                     Map<String, String>  headerTemplates,
                                     Map<String, Object> params,
                                     String sslContext,
                                     Map<String, Object> keystoreTemplates,
                                     Map<String, Object> truststoreTemplates,
                                     String reportData
                                    ) throws Exception{
        HttpsURLConnection conn = ConnectionUtil.getHttpsConnection(httpRequestMethod,urlTemplate,headerTemplates,payloadTemplate,params,sslContext,keystoreTemplates,truststoreTemplates);
        conn.setDoOutput(true);
        OutputStream out = conn.getOutputStream();
        out.write(reportData.getBytes(utf8));
        out.flush();
        out.close();

        //TODO: Figure out WHY we need the channelName as a param for this function call   ...?
        String response = getHttpsConnectionResponse((String)params.get("channelName"),conn).toString();
        logger.info("\n\nResponse: " + response);
        return response;
    }

    /**
     * This function returns the current timestamp as a string value using the (&quot;yyyyMMddhhmmss&quot;) SimpleDateFormat instance, an example: 20200910095206
     * @return
     */
    private String getTimeStamp(){
        final DateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
        return formatter.format(new Date());
    }

    /**
     * This function determines if the target file has been changed and should be replaced
     * @param replacementArtefactBody
     * @param targetFile
     * @return
     * @throws Exception
     */
    private boolean mustUpdateTargetFile(String replacementArtefactBody, File targetFile) {
        String targetFileBody = CommonHelperFunctions.getFileContent(targetFile.getPath());
        if (!targetFile.exists()) {
            System.out.println("When comparing files, target file did not exist. Creating new file " + targetFile);
            return true;
        }
        if ((targetFileBody == null || targetFileBody.isEmpty()) && (replacementArtefactBody != null || !replacementArtefactBody.isEmpty()) ) {
            return true;
        }
        else{
            Map<String, String> replacementChecksums = Checksum.checkForChecksumMatch(replacementArtefactBody, null, null);
            for (Map.Entry entry : replacementChecksums.entrySet()) {
                if (entry.getKey() instanceof String && !"MATCH".equalsIgnoreCase((String) entry.getKey())) {
                    String replacementFileChecksumAlgorithm = (String) entry.getKey();
                    String replacementFileChecksum = (String) entry.getValue();

                    Map<String, String> checksums = Checksum.checkForChecksumMatch(targetFileBody, replacementFileChecksumAlgorithm, replacementFileChecksum);
                    if (checksums.containsKey("MATCH") && "YES".equalsIgnoreCase(checksums.get("MATCH"))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Retrieve the response body from a given connection instance as a String object.
     * @param connection
     * @return
     * @throws Exception
     */
    private String getHttpConnectionBody(HttpURLConnection connection) throws Exception{
        return ConnectionUtil.getHttpConnectionBody(connection);
    }


    /**
     * This function returns a HttpURLConnection object that will be used to retrieve a specific artefact type for the given channel
     * @param channelName
     * @param artefactType
     * @return
     * @throws Exception
     */
    public HttpURLConnection getArtefactConnection(String channelName, ArtefactType artefactType) throws Exception{
//        HttpURLConnection connection = null;
        String url = FinsurvLocal.rdsURL+artefactType.apiPath;
        url = url.replaceAll("(?i)\\{channelName}", channelName);
        return getHttpConnection(url,"GET");
    }

    /**
     * Creates an HttpURLConnection instance using the given parameters
     * @param url
     * @param requestMethod
     * @return
     * @throws Exception
     */
    public HttpURLConnection getHttpConnection(String url, String requestMethod) throws Exception{
        return ConnectionUtil.getHttpConnection(url,requestMethod);
    }

    /**
     * Bulk submission/upload of BOP Reports to a TXstream Finsurv Report Data Store instance.
     * @param channelName A valid, registered channelName on the TXstream Report Data Store instance for performing validation and workflow operations.
     * @param reportData JSON formatted object of the expected schema and format as per the TXstream Finsurv Report Data API for bulk submission.
     */
    public void submitBulkReportsToRDS(String channelName, String reportData){
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        ReportServicesApi.submitBulkReportsToRDS(channelName,reportData);

    }


    /**
     * Submit a single JSObject instance (analogous to the JSON BOP schema definition for ~genv3) containing the relevant BOP Report data, to the configured TXstream Finsurv Report Data Store instance.
     * @param channelName
     * @param reportData
     */
    public void submitSingleReportToRDS(String channelName, JSObject reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    /**
     * Submit a single Map instance (analogous to the JSON BOP schema definition for ~genv3) containing the relevant BOP Report data, to the configured TXstream Finsurv Report Data Store instance.
     * @param channelName
     * @param reportData
     */
    public void submitSingleReportToRDS(String channelName, Map<String, Object> reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    /**
     * Submit a single Array instance (analogous to the JSON BOP schema definition for ~genv3, wrapped in []) containing the relevant BOP Report data, to the configured TXstream Finsurv Report Data Store instance.
     * @param channelName
     * @param reportData
     */
    public void submitSingleReportToRDS(String channelName, List<Object> reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    /**
     * Submit a single JSArray instance (analogous to the JSON BOP schema definition for ~genv3, wrapped in []) containing the relevant BOP Report data, to the configured TXstream Finsurv Report Data Store instance.
     * @param channelName
     * @param reportData
     */
    public void submitSingleReportToRDS(String channelName, JSArray reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    /**
     * Submit a single JSON formatted BOP Report to the configured TXstream Finsuv Report Data Store instance.
     * @param channelName
     * @param reportData
     */
    public void submitSingleReportToRDS(String channelName, String reportData){
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        ReportServicesApi.submitSingleReportToRDS(channelName,reportData);
    }

    /**
     * Submit a DR/CR BOP Report set for a transaction to the configured TXstream Finsurv Report Data Store.
     * The DR/CR set is a Map instance with keys "DR" and "CR" for the respective BOP Reports for the Debit (DR) and Credit (CR) sides of the transaction.
     * In certain scenarios, such as CFC-to-CFC transactions, both legs of the transaction are fully reportable to the regulator and <i>separate</i> reports must be compiled and submitted for each.
     * @param channelName
     * @param reportData
     */
    public void submitSingleDrCrReportToRDS(String channelName, Map<String,String> reportData){
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        for(Map.Entry<String,String> entry : reportData.entrySet()){
            ReportServicesApi.submitSingleReportToRDS(channelName,entry.getValue());
        }
    }

    /**
     * Fetch a single BOP report from the configured TXstream Finsurv Report Data Store, using the provided channelName and Transaction Reference (trnRef) provided
     * @param channelName
     * @param trnRef
     * @return
     */
    public String getSingleReportFromRDS(String channelName, String trnRef){
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        return ReportServicesApi.getSingleReportFromRDS(channelName,trnRef);
    }

    /**
     * This method has been replaced by getArtefactConnection and getArtefactConnectionBody
     * @param channelName
     * @param artefactType
     * @return an artefact
     * @throws Exception
     */
    @Deprecated
    private synchronized String fetchArtefact(String channelName, ArtefactType artefactType) throws Exception{
        String url = this.rdsURL+artefactType.apiPath;
        url = url.replaceAll("(?i)\\{channelName}", channelName);
        try {
            URL reportDataStoreURL = new URL(url);
//            System.out.println("**Calling URL: "+reportDataStoreURL+"\n\n");
            HttpURLConnection connection = (HttpURLConnection) reportDataStoreURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = connection.getResponseCode();
//            System.out.println("GET Response Code :: " + responseCode);
            CustomValidateResult result;
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                String responseStr = response.toString();
//                System.out.println(responseStr);
                return responseStr;
            } else {
                throw new Exception("GET request for artefact failed (HTTP Response Code: "+responseCode+")");
            }
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
        }
        return null;
    }


    //----------------------------------------------------------------------------------------------------------------//
    //  DOCUMENT VALIDATION FUNCTIONS
    //----------------------------------------------------------------------------------------------------------------//

    /**
     * This function retrieves an extensive list of potentially required documents, that may be required for Exchange Control (Excon) purposes - based purely on the bop category code and if necessary is filtered by the provided Flow.
     * @param channelName
     * @param category
     * @param flow
     * @return
     * @throws Exception
     */
    public List<String> getPotentialDocs(String channelName,
                                        String category,
                                        String flow) throws Exception {
        return Document.getPotentialDocuments(channelName, category, flow, "", CommonHelperFunctions.getArtefactLookups(channelName), CommonHelperFunctions.getArtefactDocumentRules(channelName));
    }

    /**
     * This function returns a (more specific) extensive list of potentially required documents, that may be required for Exchange Control (Excon) purposes - based on the bop category code and if necessary is filtered by the additionally provided parameters.
     * @param channelName
     * @param category
     * @param flow
     * @param section
     * @param lookups
     * @param documentRules
     * @return a list of potential documents required
     * @throws Exception
     */
    public List<String> getPotentialDocuments(String channelName,
                                                     String category,
                                                     String flow,
                                                     String section,
                                                     String lookups,
                                                     String documentRules) throws Exception {
      return Document.getPotentialDocuments(channelName,category,flow,section,lookups,documentRules);
    }


    /**
     * This function retrieves a (refined) specific list of documents required for Exchange Control (Excon) purposes - based the provided BOP Report data.
     * @param bopData
     * @param channelName
     * @return
     * @throws Exception
     */
    public List<String> getRequiredDocs(String bopData, String channelName) throws Exception {
        return Document.getRequiredDocuments(bopData, CommonHelperFunctions.getArtefactLookups(channelName), channelName, CommonHelperFunctions.getArtefactDocumentRules(channelName));
    }


    /**
     * This function retrieves a (refined) specific list of documents required for Exchange Control (Excon) purposes - based the provided BOP Report data.
     * @param bopData
     * @param lookups
     * @param channelName
     * @param documentRules
     * @return a list of the required documents
     * @throws Exception
     */
    public  List<String> getRequiredDocuments(String bopData, String lookups, String channelName, String documentRules) throws Exception {
       return Document.getRequiredDocuments(bopData,lookups,channelName,documentRules);
    }

    /**
     * Basic approach to parse the given JSON formatted BOP data (as would typically be provided by the TXstream Finsurv BOP Form) and return the data formatted as per the <i>genv3</i> JSON schema.
     * Essentially, this will normalize the older {"transaction":{...}, "customData":{...}} format to the newer {"Report":{...}, "Meta":{...}} format.
     * @param bopData
     * @return
     */
    public static String getGenv3BopJson(String bopData) {
        String reportJsonStr = "";
        if (bopData != null && bopData.trim().length() > 2){
            try {
                JSStructureParser structureParser = new JSStructureParser(bopData);
                JSObject jso = (JSObject) structureParser.parse();

                //fetch the "Report" and "Meta" data sections from the JSON Object
                JSObject reportData = null;
                JSObject metaData = null;
                if (jso.containsKey("Report") && jso.get("Report") instanceof JSObject) {
                    reportData = (JSObject) jso.get("Report");
                }
                if (jso.containsKey("Meta") && jso.get("Meta") instanceof JSObject) {
                    metaData = (JSObject) jso.get("Meta");
                }
                //the FINSURV form sometimes uses "transaction" instead of "Report"...
                if (reportData == null && jso.containsKey("transaction") && jso.get("transaction") instanceof JSObject) {
                    reportData = (JSObject) jso.get("transaction");
                }
                //the FINSURV form sometimes uses "customData" instead of "Meta"...
                if (metaData == null && jso.containsKey("customData") && jso.get("customData") instanceof JSObject) {
                    metaData = (JSObject) jso.get("customData");
                }
                JSObject reportJsonMap = new JSObject();
                reportJsonMap.put("Report", reportData);
                reportJsonMap.put("Meta", metaData);

                //TODO: This should also do a schema check to determine if it's using the older SARB schema and then upgrade it to the newer genv3 schema.
                reportJsonStr = mapToJsonStr(reportJsonMap);
            } catch (Exception err){
                //ignore for now
            }
        }
        return reportJsonStr;
    }


    /**
     * Generates a document scope key - as is typically used to uniquely identify a document object within the TXstream Finsurv Report Data Store.
     * The result is a compound key comprised of various specific fields.
     * @param trnReference
     * @param scope
     * @param sequence
     * @param subSequence
     * @param type
     * @return
     */
    public String getDocumentScopeKey(String trnReference, String scope, Integer sequence, Integer subSequence, String type){
        return Document.getDocumentScopeKey(trnReference,scope,sequence,subSequence,type);
    }
    //----------------------------------------------------------------------------------------------------------------//


    //----------------------------------------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------------------------------------//

    /**
     * Retreive the configured Template directory containing the relevant Freemarker Templates for generating the BOP Report(s).
     * @return
     */
    public static String getFinsurvReportTemplateDirectory() {
        return finsurvReportTemplateDirectory;
    }

    /**
     * Set/configure the Template directory - containing the relevant Freemarker Templates for generating the BOP Report(s).
     */
    public static void setFinsurvReportTemplateDirectory(String finsurvReportTemplateDirectory) {
        FinsurvLocal.finsurvReportTemplateDirectory = finsurvReportTemplateDirectory;
    }

    /**
     * Return the name of the file containing the Root template for the Freemarker templates used to generate the BOP Report(s)
     * @return
     */
    public static String getRootFinsurvReportTemplate() {
        return rootFinsurvReportTemplate;
    }

    /**
     * Set the name of the file containing the Root template for the Freemarker templates used to generate the BOP Report(s)
     * @param rootFinsurvReportTemplate
     */
    public static void setRootFinsurvReportTemplate(String rootFinsurvReportTemplate) {
        FinsurvLocal.rootFinsurvReportTemplate = rootFinsurvReportTemplate;
    }

    /**
     * This method goes through each of the Evaluation Decisions and generates a BOP Report for each Reportable Evaluation Decision.
     * The freemarker template will create a trn reference based on the reportable side, i.e.
     * <ul>
     *      <li>If the DR side is reportable the TrnReference will be something like "XYZ_DR"</li>
     *      <li>If the CR side is reportable, the TrnReference will be something like "XYZ_CR"</li>
     *  </ul>
     * @param data
     * @return a list containing string representations of the generated BOP reports
     */
    public List<String> generateBopRecords(Map<String, Object> data) {
        assert (this.finsurvReportTemplateDirectory instanceof String && !this.finsurvReportTemplateDirectory.trim().isEmpty());
        assert (this.rootFinsurvReportTemplate instanceof String && !this.rootFinsurvReportTemplate.trim().isEmpty());

        ArrayList<String> reports = new ArrayList<String>();
//        Object eval = data.get("eval");
        Object eval = data.get("Evaluation");
        //data.eval.Evaluations[...]
        if (eval instanceof Map && ((Map)eval).containsKey("Evaluations")){
            JSArray evaluations = (JSArray)((Map) eval).get("Evaluations");
            if (evaluations != null){
                String jsonStr = JsonUtils.mapToJsonStr(data);
                for (Object decision : evaluations){
                    try {
                        Map<String, Object> reportData = JsonUtils.jsonStrToMap(jsonStr);
//                        Map<String, Object> reportEval = (Map<String, Object>) reportData.get("eval");
                        Map<String, Object> reportEval = (Map<String, Object>) reportData.get("Evaluation");
                        List<Object> evl = new ArrayList<Object>();
                        evl.add(decision);
                        reportEval.put("Evaluations", evl);
                        String reportString = GenerateBopReport.composeTemplate(this.finsurvReportTemplateDirectory, this.rootFinsurvReportTemplate, reportData);
                        reports.add(reportString);
                    } catch (Exception err){
                        //failed to generate report...
                        reports.add("");
                    }
                }
            }
        }
        return reports;
    }

    /**
     * Freemarker templates are used to create a BOP report based on the values provided in the data
     * @param data
     * @throws Exception
     * @return a String representation of a BOP report
     */
    @Deprecated
    public String composeTemplate(Map<String, Object> data) throws Exception {
        assert (this.finsurvReportTemplateDirectory instanceof String && !this.finsurvReportTemplateDirectory.trim().isEmpty());
        assert (this.rootFinsurvReportTemplate instanceof String && !this.rootFinsurvReportTemplate.trim().isEmpty());

        return GenerateBopReport.composeTemplate(this.finsurvReportTemplateDirectory, this.rootFinsurvReportTemplate, data);
    }

    /**
     * Freemarker templates are used to create a BOP report based on the values provided in the data
     * @param data
     * @throws Exception
     * @return a String representation of a BOP report
     */
    public Map<String,String> composeDrCrTemplate(Map<String, Object> data) throws Exception {
        assert (this.finsurvReportTemplateDirectory instanceof String && !this.finsurvReportTemplateDirectory.trim().isEmpty());
        assert (this.rootFinsurvReportTemplate instanceof String && !this.rootFinsurvReportTemplate.trim().isEmpty());

        return GenerateBopReport.composeDrCrTemplate(this.finsurvReportTemplateDirectory, this.rootFinsurvReportTemplate, data);
    }

    /**
     * Freemarker templates are used to create a BOP report based on the values provided in the params
     * As this is a bulk function - multiple threads are utilized to generate the reports in parallel
     * <i>Please see the <b>bulkGenerateDrCrBopReport</b> function which replaces this function</i>
     * @param params
     * @throws Exception
     */
    @Deprecated
    public void bulkGenerateBopReport( Map<String, Map<String, Object>> params) throws Exception{
        bulkGenerateBopReport(params, null);
    }

    /**
     * Freemarker templates are used to create a BOP report based on the values provided in the params
     * As this is a bulk function - multiple threads are utilized to generate the reports in parallel
     * <i>Please see the <b>bulkGenerateDrCrBopReport</b> function which replaces this function</i>
     * @param params
     * @param threadPoolSize
     * @throws Exception
     */
    @Deprecated
    public void bulkGenerateBopReport( Map<String, Map<String, Object>> params, Integer threadPoolSize) throws Exception{
        assert (this.finsurvReportTemplateDirectory instanceof String && !this.finsurvReportTemplateDirectory.trim().isEmpty());
        assert (this.rootFinsurvReportTemplate instanceof String && !this.rootFinsurvReportTemplate.trim().isEmpty());

        GenerateBopReport.bulkGenerateBopReport(params,threadPoolSize,finsurvReportTemplateDirectory,rootFinsurvReportTemplate);
    }


    /**
     * Freemarker templates are used to create a Finsurv/BOP Report DR/CR set based on the values provided in the params (including Evaluation Decisions generated during the Evaluation step).
     * As this is a bulk function - multiple threads are utilized to generate the reports in parallel
     * @param params
     * @throws Exception
     */
    public void bulkGenerateDrCrBopReport( Map<String, Map<String, Object>> params) throws Exception{
        bulkGenerateDrCrBopReport(params, null);
    }

    /**
     * Freemarker templates are used to create a Finsurv/BOP Report DR/CR set based on the values provided in the params (including Evaluation Decisions generated during the Evaluation step).
     * As this is a bulk function - multiple threads are utilized to generate the reports in parallel
     * @param params
     * @param threadPoolSize
     * @throws Exception
     */
    public void bulkGenerateDrCrBopReport( Map<String, Map<String, Object>> params, Integer threadPoolSize) throws Exception{
        assert (this.finsurvReportTemplateDirectory instanceof String && !this.finsurvReportTemplateDirectory.trim().isEmpty());
        assert (this.rootFinsurvReportTemplate instanceof String && !this.rootFinsurvReportTemplate.trim().isEmpty());

        GenerateBopReport.bulkGenerateDrCrBopReport(params,threadPoolSize,finsurvReportTemplateDirectory,rootFinsurvReportTemplate);
    }

    /**
     * This method utilizes threading to submit a bulk payload of individual reports in parallel, to the configured TXstream Finsurv Report Data Store instance.
     * This method is preferred where the Report volumes/sizes are high, but the network latencies and overheads are low.
     * @param channelName
     * @param data
     * @throws Exception
     */
    public void bulkSubmitSingleBopReport(String channelName, Map<String, Map<String, Object>> data) throws Exception{
        bulkSubmitSingleBopReport(channelName, data,null);
    }

    /**
     * This method utilizes threading to submit a bulk payload of individual reports in parallel, to the configured TXstream Finsurv Report Data Store instance.
     * This method is preferred where the Report volumes/sizes are high, but the network latencies and overheads are low.
     * Deprecated and replaced by bulkSubmitDrCrBopReport as this method does not allow transactions where both the DR and CR side might be reportable
     * @param channelName
     * @param data
     * @param threadPoolSize
     * @throws Exception
     */
    @Deprecated
    public void bulkSubmitSingleBopReport(String channelName, Map<String, Map<String, Object>> data, Integer threadPoolSize) throws Exception{
        ReportServicesApi.bulkSubmitSingleBopReport(channelName,data,threadPoolSize);
    }

    /**
     * This method utilizes a dedicated submission thread pool to submit a bulk payload of individual reports in parallel, to the configured TXstream Finsurv Report Data Store instance.
     * This method allows for transactions where both the DR and CR sides are reportable and as a result both needs to be submitted to the regulators.
     * This method is preferred where the Report volumes/sizes are high, but the network latencies and overheads are low.
     * @param channelName
     * @param data
     * @param threadPoolSize
     * @throws Exception
     */
    public void bulkSubmitDrCrBopReport(String channelName, Map<String, Map<String, Object>> data, Integer threadPoolSize) throws Exception{
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        ReportServicesApi.bulkSubmitDrCrBopReport(channelName,data,threadPoolSize);
    }

    /**
     * Cycles through a Map of Report data, where each Report is referenced by the TrnReference as the Map key, and submits the payload as a bulk submission to the configured TXstream Finsurv Report Data Store instance.
     * This method extracts all BOP Reports provided in the Map instance and concatenates them into a single JSON string instance for submission.
     * This function is preferred where the bulk volume is not necessarily high (fewer reports) but the network latency and overheads have a high impact.
     * <i>Please see <b>bulkSubmitDrCrBopReport</b> as a replacement for this function</i>
     * @param channelName
     * @param data
     * @throws Exception
     */
    @Deprecated
    public void bulkSubmitBopReport(String channelName,  Map<String, Map<String, Object>> data) throws Exception {
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        ReportServicesApi.bulkSubmitBopReport(channelName,data);
    }

    /**
     * Cycles through a Map of Report data, where each Report is referenced by the TrnReference as the Map key, and submits the payload as a bulk submission to the configured TXstream Finsurv Report Data Store instance.
     * This method extracts all BOP Reports provided in the Map instance and concatenates them into a single JSON string instance for submission.
     * This function is preferred where the bulk volume is not necessarily high (fewer reports) but the network latency and overheads have a high impact.
     * @param channelName
     * @param data
     * @throws Exception
     */
    public void bulkSubmitDrCrBopReport(String channelName,  Map<String, Map<String, Object>> data) throws Exception {
        ReportServicesApi.setPassword(password);
        ReportServicesApi.setUsername(username);
        ReportServicesApi.bulkSubmitDrCrBopReport(channelName,data);
    }

    /**
     * This function will cycle through the provided Map of transaction and demographic data and perform Evaluations against each item.
     * The Evaluation Decisions for each are then persisted into the data object for use by other functions - such as the BOP Report Generation functions - and for auditing purposes.
     * @param channelName
     * @param data
     * @param boolSideFiltered
     * @param boolOnerousFiltered
     * @throws Exception
     */
    public void bulkEvaluateTransactionData ( String channelName,
                                          Map<String, Map<String, Object>> data,
                                           boolean boolSideFiltered,
                                          boolean boolOnerousFiltered) throws Exception {
        //TODO: perhaps do a schema check of the data?  Do we need to create a schema for the evaluation data?  ...is this even relevant?
        Evaluation.bulkEvaluateTransactionData(channelName,data,boolSideFiltered,boolOnerousFiltered);
    }

    /**
     * Takes in validation results for a given validation against a BOP Report object and logs any <b>Error</b> or <b>Warning</b> items found in the results. If any Errors are encountered in the validation result set, then the validation is deemed unsuccessful.  Warnings are allowed as part of a successful validation response, but should be noted for compliance reasons and should also be raised to the appropriate user of system as they may culminate in failures at the Regulator or compliance findings at a later date.
     * @param validationResults
     * @return a true or false value that states whether validations were successful
     */
    public boolean isValidationSuccessful(List<ResultEntry> validationResults){
       return Validation.isValidationSuccessful(validationResults);
    }

    /**
     * Performs Evaluation on the given transaction scenario and writes the results back into the provided transaction data Map.
     * @param channelName
     * @param tranData
     * @param boolSideFiltered
     * @param boolOnerousFiltered
     * @param trnReference
     */
    public void evaluateBopReport(String channelName,
                                         Map<String, Object> tranData,
                                         boolean boolSideFiltered,
                                         boolean boolOnerousFiltered,
                                         String trnReference){
        Evaluation.evaluateBopReport(channelName,tranData,boolSideFiltered,boolOnerousFiltered,trnReference);
    }

    /**
     * Validates the given DR/CR BOP Report Map items and adds the Validation results to the relevant DR/CR Map entry in the provided params Map.
     * @param channelName
     * @param trnReference
     * @param composedBopReports
     * @param params
     */
    public void validateDrCrBopReport(String channelName,
                                         String trnReference,
                                         Map<String,String> composedBopReports,
                                         Map<String, Object> params){
        Validation.validateDrCrBopReport(channelName,trnReference,composedBopReports,params, this.rdsConfiguration);
        System.out.println("Debit-leg Report Validation:");
        Validation.isValidationSuccessful((List<ResultEntry>) params.get("ValidationResultDR"));
        System.out.println("\r\n\r\nCredit-leg Report Validation:");
        Validation.isValidationSuccessful((List<ResultEntry>) params.get("ValidationResultCR"));
        System.out.println("\r\n\r\n");
    }

    /**
     * Validates the given DR/CR BOP Report Map items and adds the Validation results to the relevant DR/CR Map entry in the provided params Map.
     * NOTE: <i>It is advised to make use of the DR/CR oriented functions for all Finsurv scenarios to ensure that full reporting and compliance is maintained.  Failure to do so could result in unreported Debit and/or Credit transaction legs for specific scenarios</i>
     * @param channelName
     * @param trnReference
     * @param composedBopReport
     * @param params
     */
    public void validateBopReport(String channelName,
                                         String trnReference,
                                         String composedBopReport,
                                         Map<String, Object> params){
        Validation.validateBopReport(channelName,trnReference,composedBopReport,params, this.rdsConfiguration);
        Validation.isValidationSuccessful((List<ResultEntry>) params.get("ValidationResult"));
    }

    /**
     * Validation is done on bulk BOP reports. Threadpool size is being initialized to null.
     * TODO: for these validation functions, they are expecting a "Composed" DR/CR BOP Report... that should be documented and there should be checks and exceptions to facilitate.
     * @param channelName
     * @param data
     * @return
     * @throws Exception
     */
    @Deprecated
    public Map<String, Map<String, Object>> bulkValidateTransactionData(
            String channelName,
            Map<String, Map<String, Object>>  data
            ) throws Exception {
        return bulkValidateTransactionData(channelName,data,null, this.rdsConfiguration);
    }

    /**
     * Validation is done on bulk BOP reports.
     * @param channelName
     * @param data
     * @param threadPoolSize
     * @return
     * @throws Exception
     */
    @Deprecated
    public Map<String, Map<String, Object>> bulkValidateTransactionData(
            String channelName,
            Map<String, Map<String, Object>>  data,
            Integer threadPoolSize, RdsConfiguration rdsConfiguration) throws Exception {
        return Validation.bulkValidateTransactionData(channelName,data,threadPoolSize,rdsConfiguration);
    }

    /**
     * Bulk BOP Report Validations using a multi-threaded approach.
     * @param channelName
     * @param data
     * @return
     * @throws Exception
     */
    public Map<String, Map<String, Object>> bulkValidateDrCrTransactionData(
            String channelName,
            Map<String, Map<String, Object>>  data
    ) throws Exception {
        return bulkValidateDrCrTransactionData(channelName,data,this.rdsConfiguration);
    }

    /**
     * Bulk BOP Report Validations using a multi-threaded approach.
     * @param channelName
     * @param data
     * @return
     * @throws Exception
     */
    public Map<String, Map<String, Object>> bulkValidateDrCrTransactionData(
            String channelName,
            Map<String, Map<String, Object>>  data,
            RdsConfiguration rdsConfiguration) throws Exception {
        return Validation.bulkValidateDrCrTransactionData(channelName,data,rdsConfiguration);
    }

    /**
     * This clears the evaluator, validator and document caches (from memory)
     * TODO: Clear the artefact caches on disk?
     * TODO: Clear the artefact caches from memory (still to be implemented)
     */
    public static void clearCaches(){
        Evaluation.clearCachedEvaluators();
        Validation.clearCachedValidators();
        Document.clearCachedValidators();
    }

    /**
     * Retrieves the currently configured RDSConfiguration object which provides the instance configuration for the relevant TXstream Finsurv Report Data Store instance.
     * @return
     */
    public RdsConfiguration getRdsConfig(){
        return this.rdsConfiguration;
    }

    /**
     * Performs a diff between different JSON Report Objects using the getSmartJsonDiffActionMap function.
     * @param json1
     * @param json2
     * @return
     */
    public Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<Object>>> getReportDiff(String json1, String json2){
        try{
            return getSmartJsonDiffActionMap(json1,json2);
        }catch (Exception e){

        }
        return null;
    }


    /**
     * Performs a diff between different JSON Report Objects using the getJsonDiffText function.
     * @param json1
     * @param json2
     * @return
     */
    public String getReportDiffString(String json1, String json2){
        try{
            return getJsonDiffText(json1,json2);
        }catch (Exception e){

        }
        return null;
    }

    /**
     * Performs a diff between different (JSON) Report Objects in Map representational states, using the getJsonDiffText function.
     * @param json1
     * @param json2
     * @return
     */
    public String getReportDiffString(Map<String, Object> json1, Map<String, Object> json2){
        try{
            return getJsonDiffText(json1,json2);
        }catch (Exception e){

        }
        return null;
    }

    /**
     * Returns the differences detected between the two provided (JSON) Report objects in Map Representational states.
     * @param json1
     * @param json2
     * @return
     */
    public Map<ReportDataSection, Map<DiffActionType, List<String>>> getReportDifferenceMap(Map<String, Object> json1, Map<String, Object> json2){
        try{
            return getJsonDifferences(json1,json2);
        }catch (Exception e){

        }
        return null;
    }

    /**
     * Returns the differences detected between the two provided JSON Report objects.
     * @param json1
     * @param json2
     * @return
     */
    public Map<ReportDataSection, Map<DiffActionType, List<String>>> getReportDifferenceMap(String json1, String json2){
        try{
            return getJsonDifferences(json1,json2);
        }catch (Exception e){

        }
        return null;
    }



    // -----------------------------------------------------------------------------------------------------
    // PROCESSING THREAD POOL METHODS
    // -----------------------------------------------------------------------------------------------------
    /**
     * Takes in a thread pool size and returns a fixed thread pool executor that reuses a fixed number of threads.
     * The size of the pool depends on the value passed in, if the value is null, we try and use the processingThreadPoolMaxSize.
     * If the processingThreadPoolMaxSize is smaller than one, we determine the size of the thread pool heuristically.
     * @param threadPoolSize the maximum number of threads that will be reused in the processing pool
     * @return an executor that will be used to run processing intensive tasks
     */
    public static ExecutorService getProcessingThreadPoolExecutor(Integer threadPoolSize){
        if (!(threadPoolSize instanceof Integer && threadPoolSize.intValue() > 0)){
            if (processingThreadPoolMaxSize >= 1){
                threadPoolSize = new Integer(processingThreadPoolMaxSize);
            } else {
                int cores = Runtime.getRuntime().availableProcessors();
                threadPoolSize  = new Integer(Math.max(1, (cores - Math.abs(processingThreadPoolCoresReducer))) * Math.max(1, processingThreadPoolCoresMultiplier));
            }
        }
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    /**
     * Overloaded method of getProcessingThreadPoolExecutor, this returns an executor service on which processing tasks can be executed
     * @return an executor that will be used to run processing intensive tasks
     */
    public ExecutorService getProcessingThreadPoolInstance() {
        if (FinsurvLocal.processingThreadPool == null){
            FinsurvLocal.processingThreadPool = FinsurvLocal.getProcessingThreadPoolExecutor(null);
        }
        return FinsurvLocal.processingThreadPool;
    }

    /**
     * Kill existing processing thread pool and create a new thread pool
     */
    public void refreshProcessingThreadPool() {
        setProcessingThreadPoolSize(processingThreadPoolMaxSize);
    }

    /**
     * Set the maximum size of reusable threads for the processing thread pool.
     * Assign a new thread pool instance and await the termination of the existing thread pool.
     * @param size represents the max number of threads that will be reused in the processing thread pool
     */
    public void setProcessingThreadPoolSize(Integer size) {
        processingThreadPoolMaxSize = (size instanceof Integer? size.intValue() : 0);
        ExecutorService oldThreadPool = processingThreadPool;
        processingThreadPool = getProcessingThreadPoolExecutor(size);
        try {
            oldThreadPool.shutdown();
            oldThreadPool.awaitTermination(processingThreadPoolShutdownAwaitMinutes, TimeUnit.MINUTES);
        } catch(Exception err){
            System.out.println("WARNING: Interrupt exception encountered on old thread pool after creation of new thread pool.");
            err.printStackTrace();
        }
    }

    /**
     * Executes tasks on the processing thread pool
     * @param worker
     */
    public void executeWithProcessingThreadPool(IFinsurvRunnable worker){
        ExecutorService executor = FinsurvLocal.getInstance().getProcessingThreadPoolInstance();
        executor.execute(worker);
    }

    /**
     * Shutdown processing thread pool and await termination for a specific number of minutes, specified by processingThreadPoolShutdownAwaitMinutes
     */
    public void shutdownProcessingThreadPool(){
        try {
            processingThreadPool.shutdown();
            processingThreadPool.awaitTermination(processingThreadPoolShutdownAwaitMinutes, TimeUnit.MINUTES);
        } catch(Exception err){
            System.out.println("WARNING: Interrupt exception encountered on old thread pool after creation of new thread pool.");
            err.printStackTrace();
        }
    }

    /**
     * Create a thread safe runnable validator and validate the bop report that has been generated from tranData
     * @param channelName
     * @param entryKey
     * @param params
     * @return
     */
    public IFinsurvRunnable getRunnableValidator(String channelName, String entryKey, Map<String, Object> params){
        return new ValidateRunnable(channelName, entryKey, (String) params.get("Composed"), params, rdsConfiguration);
    }

    /**
     * Create a thread safe runnable evaluator and evaluate the bop report extracted from tranData
     * @param channelName
     * @param tranData
     * @param boolSideFiltered
     * @param boolOnerousFiltered
     * @param trnReference
     * @return
     */
    public IFinsurvRunnable getRunnableEvaluator(String channelName,
                                                 Map<String, Object> tranData,
                                                 boolean boolSideFiltered,
                                                 boolean boolOnerousFiltered,
                                                 String trnReference){
        return new EvaluateRunnable(channelName,tranData,boolSideFiltered,boolOnerousFiltered,trnReference);
    }

    /**
     * Retrieve the max number of reusable threads for processing pool
     * @return
     */
    public int getProcessingThreadPoolMaxSize() {
        return processingThreadPoolMaxSize;
    }

    /**
     * Set the max number of reusable threads for processing pool
     * @param threadPoolMaxSize
     */
    public void setProcessingThreadPoolMaxSize(int threadPoolMaxSize) {
        FinsurvLocal.processingThreadPoolMaxSize = threadPoolMaxSize;
    }

    /**
     * Returns the processing thread pool cores reducer value, which can be used to determine the thread pool size heuristically
     * @return
     */
    public int getProcessingThreadPoolCoresReducer() {
        return processingThreadPoolCoresReducer;
    }

    /**
     * Set the processing thread pool cores reducer - this value is used to determine the thread pool size heuristically
     * @param threadPoolCoresReducer
     */
    public void setProcessingThreadPoolCoresReducer(int threadPoolCoresReducer) {
        FinsurvLocal.processingThreadPoolCoresReducer = threadPoolCoresReducer;
    }

    /**
     * Returns the processing thread pool cores multiplier value, which can be used to determine the thread pool size heuristically
     * @return
     */
    public int getProcessingThreadPoolCoresMultiplier() {
        return processingThreadPoolCoresMultiplier;
    }


    /**
     * Set the processing thread pool cores multiplier - this value is used to determine the thread pool size heuristically
     * @param threadPoolCoresMultiplier
     */
    public void setProcessingThreadPoolCoresMultiplier(int threadPoolCoresMultiplier) {
        FinsurvLocal.processingThreadPoolCoresMultiplier = threadPoolCoresMultiplier;
    }

    /**
     * Retrieves the value of the processing thread pool shutdown await minutes
     * @return the amount of minutes that the processing thread pool executor must wait before it can shutdown completely
     */
    public int getProcessingThreadPoolShutdownAwaitMinutes() {
        return processingThreadPoolShutdownAwaitMinutes;
    }

    /**
     * Sets the value of the processing thread pool shutdown await minutes
     * @param threadPoolShutdownAwaitMinutes the amount of minutes that the processing thread pool executor must wait before it can shutdown completely
     */
    public void setProcessingThreadPoolShutdownAwaitMinutes(int threadPoolShutdownAwaitMinutes) {
        FinsurvLocal.processingThreadPoolShutdownAwaitMinutes = threadPoolShutdownAwaitMinutes;
    }


    //----------------------------------------------------------------------------------------------------
    // SUBMISSION THREAD POOL METHODS
    //----------------------------------------------------------------------------------------------------


    /**
     * Takes in a thread pool size and returns a fixed thread pool executor that reuses a fixed number of threads.
     * @param threadPoolSize this number determines the number of threads that will be reused
     * @return an executor that will be used to run tasks
     */
    public static ExecutorService getSubmissionThreadPoolExecutor(Integer threadPoolSize){
        if (!(threadPoolSize instanceof Integer && threadPoolSize.intValue() > 0)){
            if (submissionThreadPoolMaxSize >= 1){
                threadPoolSize = new Integer(submissionThreadPoolMaxSize);
            } else {
                int cores = Runtime.getRuntime().availableProcessors();
                threadPoolSize  = new Integer(Math.max(1, (cores - Math.abs(submissionThreadPoolCoresReducer))) * Math.max(1, submissionThreadPoolCoresMultiplier));
            }
        }
        return Executors.newFixedThreadPool(threadPoolSize);
    }

    /**
     * Checks to see if the submission thread pool has been instantiated, if so, return the existing one.
     * If the thread pool does not exist yet, return a new submission thread pool
     * @return an executor that will be used to submit reports
     */
    public ExecutorService getSubmissionThreadPoolInstance() {
        if (FinsurvLocal.submissionThreadPool == null){
            FinsurvLocal.submissionThreadPool = FinsurvLocal.getSubmissionThreadPoolExecutor(null);
        }
        return FinsurvLocal.submissionThreadPool;
    }

    /**
     * Kill the existing submission thread pool and instantiate a new submission thread pool.
     */
    public void refreshSubmissionThreadPool() {
        setSubmissionThreadPoolSize(submissionThreadPoolMaxSize);
    }

    /**
     * Set the maximum size of reusable threads for the submission thread pool.
     * Assign a new thread pool instance and await the termination of the existing thread pool.
     * @param size represents the max number of threads that will be reused in the submission pool
     */
    public void setSubmissionThreadPoolSize(Integer size) {
        submissionThreadPoolMaxSize = (size instanceof Integer? size.intValue() : 0);
        ExecutorService oldThreadPool = submissionThreadPool;
        submissionThreadPool = getSubmissionThreadPoolExecutor(size);
        try {
            oldThreadPool.shutdown();
            oldThreadPool.awaitTermination(submissionThreadPoolShutdownAwaitMinutes, TimeUnit.MINUTES);
        } catch(Exception err){
            System.out.println("WARNING: Interrupt exception encountered on old submission thread pool after creation of new submission thread pool.");
            err.printStackTrace();
        }
    }

    /**
     * Submits a report to Report Data Store with the help of a worker.
     * @param worker
     */
    public void executeWithSubmissionThreadPool(IFinsurvRunnable worker){
        ExecutorService executor = FinsurvLocal.getInstance().getSubmissionThreadPoolInstance();
        executor.execute(worker);
    }

    /**
     * Shuts down the submission thread pool and awaits termination
     */
    public void shutdownSubmissionThreadPool(){
        try {
            submissionThreadPool.shutdown();
            submissionThreadPool.awaitTermination(submissionThreadPoolShutdownAwaitMinutes, TimeUnit.MINUTES);
        } catch(Exception err){
            System.out.println("WARNING: Interrupt exception encountered on old submission thread pool after creation of new submission thread pool.");
            err.printStackTrace();
        }
    }

    /**
     * Retrieve the maximum number of reusable threads for submission pool
     * @return maximum number of reusable threads for submission pool
     */
    public int getSubmissionThreadPoolMaxSize() {
        return submissionThreadPoolMaxSize;
    }

    /**
     * Sets maximum number of reusable threads for submission pool
     * @param submissionThreadPoolMaxSize
     */
    public void setSubmissionThreadPoolMaxSize(int submissionThreadPoolMaxSize) {
        FinsurvLocal.submissionThreadPoolMaxSize = submissionThreadPoolMaxSize;
    }

    /**
     * Returns the submission thread pool cores reducer value, which can be used to determine the thread pool size heuristically
     * @return
     */
    public int getSubmissionThreadPoolCoresReducer() {
        return submissionThreadPoolCoresReducer;
    }

    /**
     * Sets the task thread pool cores reducer, this value is used in the getSubmissionExecutor method to determine the thread pool size heuristically
     * @param submissionThreadPoolCoresReducer
     */
    public void setSubmissionThreadPoolCoresReducer(int submissionThreadPoolCoresReducer) {
        FinsurvLocal.submissionThreadPoolCoresReducer = submissionThreadPoolCoresReducer;
    }

    /**
     * Returns the submission thread pool cores multiplier value, which can be used to determine the thread pool size heuristically
     * @return
     */
    public int getSubmissionThreadPoolCoresMultiplier() {
        return submissionThreadPoolCoresMultiplier;
    }

    /**
     *  Sets the task thread pool cores reducer, this value is used in the getSubmissionExecutor method to determine the thread pool size heuristically
     * @param submissionThreadPoolCoresMultiplier
     */
    public void setSubmissionThreadPoolCoresMultiplier(int submissionThreadPoolCoresMultiplier) {
        FinsurvLocal.submissionThreadPoolCoresMultiplier = submissionThreadPoolCoresMultiplier;
    }

    /**
     * Retrieve the shutdown await minutes for the submission thread pool
     * @return
     */
    public int getSubmissionThreadPoolShutdownAwaitMinutes() {
        return submissionThreadPoolShutdownAwaitMinutes;
    }

    /**
     * Set the shutdown await minutes for the submission thread pool
     * @param submissionThreadPoolShutdownAwaitMinutes
     */
    public void setSubmissionThreadPoolShutdownAwaitMinutes(int submissionThreadPoolShutdownAwaitMinutes) {
        FinsurvLocal.submissionThreadPoolShutdownAwaitMinutes = submissionThreadPoolShutdownAwaitMinutes;
    }

    //----------------------------------------------------------------------------------------------------

    /**
     * Takes in a thread pool size and returns a fixed thread pool executor that reuses a fixed number of threads.
     * The size of the pool depends on the value passed in, if the value is null, we try and use the taskThreadPoolMaxSize.
     * If the taskThreadPoolMaxSize is smaller than one, we determine the size of the thread pool heuristically.
     * @param threadPoolSize the maximum number of threads that will be reused in the processing pool
     * @return an executor that will be used to run tasks
     */
    public static ExecutorService getTaskExecutor(Integer threadPoolSize){
        if (!(threadPoolSize instanceof Integer && threadPoolSize.intValue() > 0)){
            if (taskThreadPoolMaxSize >= 1){
                threadPoolSize = new Integer(taskThreadPoolMaxSize);
            } else {
                int cores = Runtime.getRuntime().availableProcessors();
                threadPoolSize  = new Integer(Math.max(1, (cores - Math.abs(taskThreadPoolCoresReducer))) * Math.max(1, taskThreadPoolCoresMultiplier));
            }
        }
        return Executors.newFixedThreadPool(threadPoolSize);
    }


    /**
     * Overloaded method for getTaskExecutor.
     * Initializes the thread pool size to null.
     * @return an executor service on which tasks can be executed
     */
    public ExecutorService getTaskThreadPoolInstance() {
        if (FinsurvLocal.taskThreadPool == null){
            FinsurvLocal.taskThreadPool = FinsurvLocal.getTaskExecutor(null);
        }
        return FinsurvLocal.taskThreadPool;
    }

    /**
     * Refresh the task thread pool by referencing setTaskThreadPoolSize, which kills of the old thread pool and instantiates a new thread pool
     */
    public void refreshTaskThreadPool() {
        setTaskThreadPoolSize(taskThreadPoolMaxSize);
    }

     /**
     * Set the maximum size of reusable threads for the task thread pool.
     * Assign a new thread pool instance and await the termination of the existing task thread pool.
     * @param size represents the max number of threads that will be reused in the task thread pool
     */
    public void setTaskThreadPoolSize(Integer size) {
        taskThreadPoolMaxSize = (size instanceof Integer? size.intValue() : 0);
        ExecutorService oldThreadPool = taskThreadPool;
        taskThreadPool = getProcessingThreadPoolExecutor(size);
        try {
            oldThreadPool.shutdown();
            oldThreadPool.awaitTermination(taskThreadPoolShutdownAwaitMinutes, TimeUnit.MINUTES);
        } catch(Exception err){
            System.out.println("WARNING: Interrupt exception encountered on old thread pool after creation of new thread pool.");
            err.printStackTrace();
        }
    }

    /**
     * Executes a task in the task thread pool.
     * @param worker
     */
    public void executeWithTaskThreadPool(IFinsurvRunnable worker){
        ExecutorService executor = FinsurvLocal.getInstance().getTaskThreadPoolInstance();
        executor.execute(worker);
    }

    /**
     * Shuts down and awaits the termination of the task thread pool. The termination period is specified in the taskThreadPoolShutdownAwaitMinutes
     */
    public void shutdownTaskThreadPool(){
        try {
            taskThreadPool.shutdown();
            taskThreadPool.awaitTermination(taskThreadPoolShutdownAwaitMinutes, TimeUnit.MINUTES);
        } catch(Exception err){
            System.out.println("WARNING: Interrupt exception encountered on old thread pool after creation of new thread pool.");
            err.printStackTrace();
        }
    }

    /**
     * This method creates a background task which will run in the task thread pool.
     * The task is responsible for updating artefacts and refreshing validators and evaluators based on an expiry time that is specified.
     */
    public synchronized void runBackgroundArtefactWorker(){
        getTaskThreadPoolInstance();

        if(artefactBackgroundWorker != null){
            artefactBackgroundWorker.stop();
        }

        artefactBackgroundWorker = new ArtefactBackgroundWorker();
        executeWithTaskThreadPool(artefactBackgroundWorker);
    }

    /**
     * Retrieve the task thread pool max size
     * @return maximum number of reusable threads for task pool
     */
    public int getTaskThreadPoolMaxSize() {
        return taskThreadPoolMaxSize;
    }

    /**
     * Sets maximum number of reusable threads for submission pool
     * @param taskThreadPoolMaxSize
     */
    public void setTaskThreadPoolMaxSize(int taskThreadPoolMaxSize) {
        FinsurvLocal.taskThreadPoolMaxSize = taskThreadPoolMaxSize;
    }

    /**
     * Returns the task thread pool cores reducer value, which can be used to determine the thread pool size heuristically
     * @return taskThreadPoolCoresReducer
     */
    public int getTaskThreadPoolCoresReducer() {
        return taskThreadPoolCoresReducer;
    }

    /**
     *  Sets the task thread pool cores reducer, this value is used in the getTaskExecutor method to determine the thread pool size heuristically
     * @param taskThreadPoolCoresReducer
     */
    public void setTaskThreadPoolCoresReducer(int taskThreadPoolCoresReducer) {
        FinsurvLocal.taskThreadPoolCoresReducer = taskThreadPoolCoresReducer;
    }

    /**
     * Returns the task thread pool cores multiplier value, which can be used to determine the thread pool size heuristically
     * @return taskThreadPoolCoresMultiplier
     */
    public int getTaskThreadPoolCoresMultiplier() {
        return taskThreadPoolCoresMultiplier;
    }

    /**
     * Sets the task thread pool cores multiplier, this value is used in the getTaskExecutor method to determine the thread pool size heuristically
     * @param taskThreadPoolCoresMultiplier
     */
    public void setTaskThreadPoolCoresMultiplier(int taskThreadPoolCoresMultiplier) {
        FinsurvLocal.taskThreadPoolCoresMultiplier = taskThreadPoolCoresMultiplier;
    }

    /**
     * Retrieves the value of the task thread pool shutdown await minutes
     * @return the amount of minutes that the task thread pool executor must wait before it can shutdown completely
     */
    public int getTaskThreadPoolShutdownAwaitMinutes() {
        return taskThreadPoolShutdownAwaitMinutes;
    }

    /**
     * Sets the value of the task thread pool shutdown await minutes
     * @param taskThreadPoolShutdownAwaitMinutes the amount of minutes that the task thread pool executor must wait before it can shutdown completely
     */
    public void setTaskThreadPoolShutdownAwaitMinutes(int taskThreadPoolShutdownAwaitMinutes) {
        FinsurvLocal.taskThreadPoolShutdownAwaitMinutes = taskThreadPoolShutdownAwaitMinutes;
    }

    /**
     * This background task is continuously checking to see if the channels are ready for processing.
     * If they are not ready, sleep for 500 ms and check again.
     */
    public synchronized void waitTillReadyForProcessing(){
       if(channels.size()>0) {
           if (
                   artefactBackgroundWorker == null ||
                           artefactBackgroundWorker.getStatus().equals(IFinsurvRunnable.FinsurvRunnableState.ERROR) ||
                           artefactBackgroundWorker.getStatus().equals(IFinsurvRunnable.FinsurvRunnableState.COMPLETE)
           ) {
               System.out.println("Starting up background worker");
               runBackgroundArtefactWorker();
           }
//           while (!checkIfAllChannelsHaveArtefactsReadyForProcessing()) {
//               try {
//                   System.out.println("Still waiting on artefacts to be in a ready state");
//                   Thread.sleep(50);
//               } catch (InterruptedException e) {
//                   System.err.println("Exception while awaiting readiness to process: " + e.getMessage());
//                   e.printStackTrace();
//               }
//           }
       }
    }

    /**
     * Determines if all channels are ready for processing. Each channel name is sent to isChannelReady, which checks the artefacts for that specific channel name.
     * If the artefacts have string values, we can infer that the channel is ready for processing.
     * @return
     */
    public synchronized boolean checkIfAllChannelsHaveArtefactsReadyForProcessing(){
        boolean isReady = true;
        for (String channelName: FinsurvLocal.currentInstance.getChannelNames()){
            if (!isChannelArtefactsReady(channelName)){
                isReady = false;
                break;
            }
        }
        return isReady;
    }

    /**
     * This functions fetches the content of the various artefacts for the given channel and determines if the content resolves to strings.
     * If the content is null, we can infer that the artefacts are not ready yet.
     * @param channelName
     * @return a true / false value indicating whether the artefacts are initialized or not
     */
    public synchronized boolean isChannelArtefactsReady(String channelName){
        try {
            return (
                    getArtefactContent(channelName, ArtefactType.EVALUATION_RULES) instanceof String &&
                            getArtefactContent(channelName, ArtefactType.VALIDATION_RULES) instanceof String &&
                            getArtefactContent(channelName, ArtefactType.DOCUMENT_RULES) instanceof String &&
                            getArtefactContent(channelName, ArtefactType.EXTERNAL_VALIDATION_CONFIG) instanceof String &&
                            getArtefactContent(channelName, ArtefactType.LOOKUPS) instanceof String &&
                            getArtefactContent(channelName, ArtefactType.FORM_JS) instanceof String
            );
        } catch(Exception error){
            return false;
        }
    }

}
