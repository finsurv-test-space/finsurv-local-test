package za.co.synthesis.finsurvlocal.evaluation;

import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;

import java.util.Map;

//TODO: Complete (client-friendly) java doc for the code below...
public class EvaluateRunnable implements IFinsurvRunnable {

    private String channelName;
    private boolean boolSideFiltered;
    private boolean boolOnerousFiltered;
    private volatile Map<String,Object> tranData;
    private String trnReference;

    /**
     * In order to make the process of Evaluation, Generation, Validation and submission thread safe, we built in a status.
     * This status is being checked continuously and if this is still in a busy state, the process waits a certain amount of time and checks again.
     * Once the thread is in a complete state, the process can move on to generation of a bop report.
     */
    private FinsurvRunnableState status = FinsurvRunnableState.NEW;

    public EvaluateRunnable(String channelName,
                            Map<String, Object> tranData,
                            boolean boolSideFiltered,
                            boolean boolOnerousFiltered,
                            String trnReference) {
        this.channelName = channelName;
        this.tranData = tranData;
        this.boolOnerousFiltered = boolOnerousFiltered;
        this.boolSideFiltered = boolSideFiltered;
        this.trnReference = trnReference;
    }

    @Override
    public void run() {
        this.setBusyStatus();
        Evaluation.evaluateBopReport(channelName,tranData,boolSideFiltered,boolOnerousFiltered,trnReference);
        this.setCompleteStatus();
    }

    @Override
    public synchronized FinsurvRunnableState getStatus() {
        return this.status;
    }

    @Override
    public synchronized FinsurvRunnableState setBusyStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.BUSY;
        return lastState;
    }

    @Override
    public synchronized FinsurvRunnableState setCompleteStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.COMPLETE;
        return lastState;
    }

    @Override
    public FinsurvRunnableState setErrorStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.ERROR;
        return lastState;
    }

    @Override
    public synchronized FinsurvRunnableState setStatus(FinsurvRunnableState newState) {
        FinsurvRunnableState lastState = this.status;
        this.status = newState;
        return lastState;
    }

    @Override
    public FinsurvRunnableState setIdleStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.IDLE;
        return lastState;
    }
}
