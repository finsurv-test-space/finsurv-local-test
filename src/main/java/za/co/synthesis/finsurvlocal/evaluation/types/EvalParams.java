package za.co.synthesis.finsurvlocal.evaluation.types;

import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.ResidenceStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//TODO: Complete (client-friendly) java doc for the code below...
public class EvalParams {
    public volatile String drBIC = null;
    public volatile ResidenceStatus drResStatus = null;
    public volatile BankAccountType drBankAccType = null;
    public volatile String drCurr = null;
    public volatile String strField72 = null;
    public volatile Map<String, Object> drOptionalParams = new ConcurrentHashMap<String, Object>();

    public volatile String crBIC = null;
    public volatile ResidenceStatus crResStatus = null;
    public volatile BankAccountType crBankAccType = null;
    public volatile String crCurr = null;
    public volatile Map<String, Object> crOptionalParams = new ConcurrentHashMap<String, Object>();
}
