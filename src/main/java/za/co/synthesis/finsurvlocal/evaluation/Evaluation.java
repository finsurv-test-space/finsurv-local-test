package za.co.synthesis.finsurvlocal.evaluation;

import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.evaluation.types.EvalParams;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;
import za.co.synthesis.finsurvlocal.validation.FinsurvEngineInstancePool;
import za.co.synthesis.finsurvlocal.validation.Validation;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.rule.core.*;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//TODO: Complete (client-friendly) java doc for the code below...

public class Evaluation {

    public enum EvaluatorInstanceParameter{
        INSTANCE_POOL_MIN("minInstances"),
        INSTANCE_POOL_MAX("maxInstances"),
        TIME_TO_LIVE("ttl");

        private String value;
        EvaluatorInstanceParameter(String value){ this.value=value;}
        public String toString(){return this.value;}

        public static EvaluatorInstanceParameter fromString(String in){
            if(in != null){
                for(EvaluatorInstanceParameter eventType : EvaluatorInstanceParameter.values()){
                    if (in.equalsIgnoreCase(eventType.value)) {
                        return eventType;
                    }
                }
            }
            return null;
        }
    }

    /**
     * A statically defined instance of all the Java Rules Engine Evaluator instances for each Channel(AKA: Package, containing channel-specific artefacts)...
     */
    public static volatile Map<String, FinsurvEngineInstancePool<Evaluator>> packageEvaluators = new ConcurrentHashMap<String, FinsurvEngineInstancePool<Evaluator>>();

    public static volatile int defaultEvaluatorInstancesPerPackage = 12;
    public static volatile int defaultEvaluatorExpiryMinutesPerPackage = 20;
    /**
     * Create an instance of the Java Rules Engine Evaluator, primed with the evaluation rules Stubs
     *
     * @param channelName       Used to determine the set of artefacts to load and or server-side configurations to reference.
     * @param evaluationRules   Used to determine if a transaction is reportable or not
     * @return an evaluator
     * @throws Exception
     */
    public static Evaluator getEvaluator(String channelName, String evaluationRules) throws Exception {
        Evaluator evaluator = null;
        EvaluationEngine evaluationEngine = new EvaluationEngine();
        try {
            //Load the context, rules and assumptions to use for evaluation...
            //ALL CONTAINED IN ONE ARTEFACT NOW, BY DEFAULT - where these were previously supplied as separate artefacts
            evaluationEngine.setupLoadContext(channelName, new StringReader(evaluationRules));
            evaluationEngine.setupLoadRule(channelName, new StringReader(evaluationRules));
            evaluationEngine.setupLoadAssumptions(channelName, new StringReader(evaluationRules));

            //get an evaluator instance...
            evaluator = evaluationEngine.issueEvaluator();
        } catch (Exception e) {
            System.out.println("Exception on creating Evaluator instance: " + e.getMessage());
            e.printStackTrace();
        }
        return evaluator;
    }

    /**
     *  Example function to get or create an instance of an evaluator for a given channel/package name. This works by
     *  fetching the relevant artefacts specific to the channel/package name provided and passing the contents thereof to
     *  the new evaluator instance - namely:
     * <ul>
     *      <li>Evaluation Rules</li>
     * </ul>
     * @param packageName the channel/package name for which a Evaluator instance must be returned (if one already
     *      *                    exists) OR created - with the relevant package artefacts.
     * @return
     */
    public static synchronized Evaluator getEvaluatorForPackage(String packageName) {
        if (packageEvaluators.containsKey(packageName)){
            return packageEvaluators.get(packageName).getInstance();
        } else {
            ArrayList<Evaluator> initialInstances = new ArrayList<Evaluator>();
            int instanceCount = getEvaluatorInstancePoolMin(packageName);
            for (int index = 0; index < instanceCount; index ++) {
                try {
                    Evaluator evaluator = Evaluation.getEvaluator(packageName,
                            getEvaluationRules(packageName));
                    initialInstances.add(evaluator);
                } catch (Exception e) {
                    return null;
                }
            }
            FinsurvEngineInstancePool<Evaluator> instancePool = new FinsurvEngineInstancePool<Evaluator>(packageName, getEvaluatorInstancePoolTtl(packageName),initialInstances);
            packageEvaluators.put(packageName, instancePool);
            return instancePool.getInstance();
        }
    }


    /**
     * Function to simultaneously get/create an Evaluator instance for the given Channel/Package name, and return
     *   Unknown,
     *   Illegal,
     *   DoNotReport or
     *   ReportToRegulator
     *   as the chosen decision
     *
     * Evaluation is used to determine whether or not to report a transaction to the Regulator, and what/how to report,
     * and which side (DR, CR or both) for the transaction - if so required.
     *
     * @param channelName The channel/package from which the relevant rules, lookups and external validation configurations should be applied.
     * @param params The transaction parameters (I.E. Back account type of Dr and CR, ResStatus etc)
     * @param boolSideFiltered If set to true, this will Filter out results that are not relevant to the reportable side(s) and ONLY return those that apply as necessary (for payments this is usually DR,
     *      *                  and for Receipts, this is usually CR)
     * @param boolOnerousFiltered If set to true, the results will be filtered to only the single most onerous result for each/either side of the transaction. (IE: if a transaction could be either BOPCUS
     *      *                  reportable or "NON Reportable", the BOPCUS result will be returned as it requires a more complete BOP data set and over-reporting is usually more accptable by the regulator
     *      *                  as opposed to under-reporting.
     * @return an IEvaluationScenarioDecision
     * @throws Exception
     */
    public static IEvaluationScenarioDecision evaluateTransactionData(
            String channelName,
            EvalParams params,
            boolean boolSideFiltered,
            boolean boolOnerousFiltered) throws Exception {
        IEvaluationScenarioDecision evalResults = null;
        Evaluator evaluator = getEvaluatorForPackage(channelName);

        try {
            if(evaluator!= null) {
                synchronized(evaluator) {
                    evalResults = evaluator.consolidatedEvaluation(
                            params.drBIC, params.drResStatus, params.drBankAccType, params.drCurr, params.strField72, params.drOptionalParams,
                            params.crBIC, params.crResStatus, params.crBankAccType, params.crCurr, params.crOptionalParams,
                            boolSideFiltered,
                            boolOnerousFiltered);
                }
            }
        }catch(Exception e){
            System.out.println("Evaluator for package "+channelName+ " is null");
        }
        printEvaluationResults(evalResults);
        return evalResults;
    }

    public static void bulkEvaluateTransactionData(
            String channelName,
            Map<String, Map<String, Object>> data,
            boolean boolSideFiltered,
            boolean boolOnerousFiltered) throws Exception {

        ArrayList<IFinsurvRunnable> workers = new ArrayList<IFinsurvRunnable>();
        for (Map.Entry<String, Map<String,Object>> params: data.entrySet()){
            workers.add(evaluateTransactionData(channelName, params.getValue(), boolSideFiltered, boolOnerousFiltered, params.getKey()));
        }
        boolean isComplete = false;
        while (!isComplete){
            isComplete = true;
            for (IFinsurvRunnable worker : workers){
                if (worker.getStatus() != IFinsurvRunnable.FinsurvRunnableState.COMPLETE){
                    isComplete = false;
                    break;
                }
            }
            if (!isComplete){
                Thread.sleep(50);
            }
        }
//        System.out.println("\n\n******* Bulk Evaluation call complete ********");
//        for (String trnReference: data.keySet()){s
//            System.out.println("\t\t-\t"+trnReference);
//        }
//        System.out.println("\n");
    }

    public static IFinsurvRunnable evaluateTransactionData(
            String channelName,
            Map<String, Object> data,
            boolean boolSideFiltered,
            boolean boolOnerousFiltered,
            String trnReference) throws Exception {

            IFinsurvRunnable worker = new EvaluateRunnable(channelName, data, boolSideFiltered, boolOnerousFiltered, trnReference);
            FinsurvLocal.getInstance().executeWithProcessingThreadPool(worker);
            return worker;
    }

    public static synchronized void printEvaluationResults(IEvaluationScenarioDecision evalResults){
        if (evalResults != null) {
//            System.out.println("\nEval Decision Scenario: "+evalResults.getScenario());
//            System.out.println("\nEval Decision Information: "+evalResults.getInformation());
            List<IEvaluationDecision> decisions = evalResults.getDecisions();
            for (IEvaluationDecision decision : decisions){
//                System.out.println( "\t...Decision: "+decision.getDecision());
                //TODO: Provide more detail about the decision - Res/NonRes vs DR/CR... Acc Types... Category Code restrictions.... etc.
                //At this point, the **CHOSEN** Decision can then be used to augment/pre-fill the BOP Report with the relevant information...
            }
        }
    }

    public static String getEvaluationRules(String packageName){
        return CommonHelperFunctions.getArtefactEvaluationRules(packageName);
    }

    public static EvalParams getEvaluationParameters(String evalJsonParams) throws Exception {
        JSStructureParser structureParser = new JSStructureParser(evalJsonParams);
        JSObject jso = (JSObject)structureParser.parse();
        return getEvaluationParameters(jso);
    }

    public static EvalParams getEvaluationParameters(Map<String, Object> params) throws Exception {
        EvalParams evalParams = new EvalParams();

        //load the function param values from the provided json object - mapping code:
        if (params != null && params.containsKey("drBIC") && params.get("drBIC") instanceof String) {
            evalParams.drBIC = ((String) params.get("drBIC"));
        }
        if (params != null && params.containsKey("drCurrency") && params.get("drCurrency") instanceof String) {
            evalParams.drCurr = ((String) params.get("drCurrency"));
        }
        if (params != null && params.containsKey("drResidenceStatus") && params.get("drResidenceStatus") instanceof String && !((String)params.get("drResidenceStatus")).isEmpty()) {
            try {
                evalParams.drResStatus = ResidenceStatus.valueOf((String) params.get("drResidenceStatus"));
            } catch (Exception err){
                System.err.println("Invalid ResidenceStatus provided for drResStatus: \""+((String) params.get("drResidenceStatus"))+"\"");
                evalParams.drResStatus = ResidenceStatus.Unknown;
            }
        }
        if (params != null && params.containsKey("drBankAccType") && params.get("drBankAccType") instanceof String && !((String)params.get("drBankAccType")).isEmpty()) {
            try{
                evalParams.drBankAccType = BankAccountType.valueOf((String)params.get("drBankAccType"));
            } catch(Exception err){
                System.err.println("Invalid BankAccountType provided for drBankAccType: \""+((String) params.get("drBankAccType"))+"\"");
                evalParams.drBankAccType = BankAccountType.Unknown;
            }
        }
        if (params != null && params.containsKey("field72") && params.get("field72") instanceof String) {
            evalParams.strField72 = ((String)params.get("field72"));
        }
        if (params != null && params.containsKey("drOptionalParams") && params.get("drOptionalParams") instanceof Map) {
            evalParams.drOptionalParams = (Map<String, Object>) params.get("drOptionalParams");
        }
        if (params != null && params.containsKey("crBIC") && params.get("crBIC") instanceof String) {
            evalParams.crBIC = ((String) params.get("crBIC"));
        }
        if (params != null && params.containsKey("crCurrency") && params.get("crCurrency") instanceof String) {
            evalParams.crCurr = ((String) params.get("crCurrency"));
        }
        if (params != null && params.containsKey("crResidenceStatus") && params.get("crResidenceStatus") instanceof String && !((String)params.get("crResidenceStatus")).isEmpty()) {
            try {
                evalParams.crResStatus = ResidenceStatus.valueOf((String) params.get("crResidenceStatus"));
            } catch (Exception err){
                System.err.println("Invalid ResidenceStatus provided for crResStatus: \""+((String) params.get("crResidenceStatus"))+"\"");
                evalParams.crResStatus = ResidenceStatus.Unknown;
            }
        }
        if (params != null && params.containsKey("crBankAccType") && params.get("crBankAccType") instanceof String && !((String)params.get("crBankAccType")).isEmpty()) {
            try {
                evalParams.crBankAccType = BankAccountType.valueOf((String) params.get("crBankAccType"));
            } catch (Exception err){
                System.err.println("Invalid BankAccountType provided for crBankAccType: \""+((String) params.get("crBankAccType"))+"\"");
                evalParams.crBankAccType = BankAccountType.Unknown;
            }
        }
        if (params != null && params.containsKey("crOptionalParams") && params.get("crOptionalParams") instanceof Map) {
            evalParams.crOptionalParams = (Map<String, Object>) params.get("crOptionalParams");
        }
        return evalParams;
    }

    public static void clearCachedEvaluators(){
        packageEvaluators.clear();
    }

    public static Map<String, Map<String, Object>> bulkValidateTransactionData(
            String channelName,
            Map<String, Map<String, Object>> data) throws Exception {
        return Validation.bulkValidateTransactionData(channelName, data, null);
    }

    public static Map<String, Object> evaluateBopReport(String channelName,
                                         Map<String, Object> tranData,
                                         boolean boolSideFiltered,
                                         boolean boolOnerousFiltered,
                                         String trnReference){
        try{
            if (tranData.containsKey("TrnReference")){
//                System.out.println("TrnReference present in result data: " + trnReference);
            }
            EvalParams params = getEvaluationParameters(tranData);
            IEvaluationScenarioDecision evalResult = evaluateTransactionData(
                    channelName,
                    params,
                    boolSideFiltered,
                    boolOnerousFiltered);
            tranData.put("Evaluation", JsonUtils.evaluationScenarioDecisionToMap(evalResult));
        } catch(Exception err){
            //DO SOMETHING IF PARSE FAILS...
            System.err.println("Unable to parse evaluation scenario decision ["+trnReference+"]");
            err.printStackTrace();
        }
        return tranData;
    }

    public static int getEvaluatorInstancePoolMin(String channelName){
        if (FinsurvLocal.channels != null && FinsurvLocal.channels.size() > 0 && FinsurvLocal.channels.containsKey(channelName)){
            Map<String, Object> channelConfig = FinsurvLocal.channels.get(channelName);
            if (channelConfig.containsKey(EvaluatorInstanceParameter.INSTANCE_POOL_MIN) &&
                    channelConfig.get(EvaluatorInstanceParameter.INSTANCE_POOL_MIN) != null &&
                    channelConfig.get(EvaluatorInstanceParameter.INSTANCE_POOL_MIN) instanceof Integer){
                return ((Integer)channelConfig.get(EvaluatorInstanceParameter.INSTANCE_POOL_MIN)).intValue();
            }
        }
        return defaultEvaluatorInstancesPerPackage;
    }

    public static int getEvaluatorInstancePoolMax(String channelName){
        if (FinsurvLocal.channels != null && FinsurvLocal.channels.size() > 0 && FinsurvLocal.channels.containsKey(channelName)){
            Map<String, Object> channelConfig = FinsurvLocal.channels.get(channelName);
            if (channelConfig.containsKey(EvaluatorInstanceParameter.INSTANCE_POOL_MAX) &&
                    channelConfig.get(EvaluatorInstanceParameter.INSTANCE_POOL_MAX) != null &&
                    channelConfig.get(EvaluatorInstanceParameter.INSTANCE_POOL_MAX) instanceof Integer){
                return ((Integer)channelConfig.get(EvaluatorInstanceParameter.INSTANCE_POOL_MAX)).intValue();
            }
        }
        return defaultEvaluatorInstancesPerPackage;
    }

    public static int getEvaluatorInstancePoolTtl(String channelName){
        if (FinsurvLocal.channels != null && FinsurvLocal.channels.size() > 0 && FinsurvLocal.channels.containsKey(channelName)){
            Map<String, Object> channelConfig = FinsurvLocal.channels.get(channelName);
            if (channelConfig.containsKey(EvaluatorInstanceParameter.TIME_TO_LIVE) &&
                    channelConfig.get(EvaluatorInstanceParameter.TIME_TO_LIVE) != null &&
                    channelConfig.get(EvaluatorInstanceParameter.TIME_TO_LIVE) instanceof Integer){
                return ((Integer)channelConfig.get(EvaluatorInstanceParameter.TIME_TO_LIVE)).intValue();
            }
        }
        return defaultEvaluatorExpiryMinutesPerPackage;
    }

    public static void refreshExpiredInstances(){
        for(Map.Entry<String, FinsurvEngineInstancePool<Evaluator>> packageEvaluator : packageEvaluators.entrySet()){
            FinsurvEngineInstancePool<Evaluator> instancePool = packageEvaluator.getValue();
            String packageName = packageEvaluator.getKey();
            while (instancePool.checkForExpiredInstances()){
                Evaluator evaluator = null;
                try {
                    evaluator = Evaluation.getEvaluator(packageName,
                            getEvaluationRules(packageName));
                    instancePool.setInstance(evaluator);
                }catch (Exception e){
                    System.err.println("Unable to create new Evaluator instances for " + packageName +": "+e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }
}
