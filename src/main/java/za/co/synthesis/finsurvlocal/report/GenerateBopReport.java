package za.co.synthesis.finsurvlocal.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.utils.FreemarkerUtil;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


//TODO: I Believe this should be deprecated - to be checked!
@Deprecated
public class GenerateBopReport {
    public static Logger logger = LoggerFactory.getLogger(GenerateBopReport.class);


    public static Map<String, String> composeDrCrTemplate(String templateDirectory, String rootTemplate, Map<String, Object> data) throws Exception {
        HashMap<String, String> result = new HashMap<String,String>();
        try {
            Map<String, Object> evalMap = (Map<String, Object>) data.get("Evaluation");
            String scenario = (String)evalMap.get("Scenario");

            /*
                This logic works under the following assumptions:
                1) A Bank receiving payment would ONLY be required to report on the CREDIT (CR) leg of the payment
                    ...hence the DR side can be ignored for generation of a BOP Report
                2) A Bank making payment would ONLY be required to report on the DEBIT (DR) leg of the payment
                    ...hence the CR side can be ignored for generation of a BOP Report
                3) All other scenarios are potentially reportable for BOTH legs (DR & CR)
                    ...hence both CR and DR sides are considered for generation of a BOP Report
             */
            if (!"CustomerReceipt".equalsIgnoreCase(scenario) && !"BankReceipt".equalsIgnoreCase(scenario)) {
                //create new, editable, deep-cloned copies of the original evaluation data
                data.put("EvaluationDr", JsonUtils.jsonStrToMap(JsonUtils.mapToJsonStr((Map<String, Object>) data.get("Evaluation"))));
            }
            if (!"CustomerPayment".equalsIgnoreCase(scenario) && !"BankPayment".equalsIgnoreCase(scenario)) {
                //create new, editable, deep-cloned copies of the original evaluation data
                data.put("EvaluationCr", JsonUtils.jsonStrToMap(JsonUtils.mapToJsonStr((Map<String, Object>) data.get("Evaluation"))));
            }

            Object DrEvals = data.get("EvaluationDr");
            Object CrEvals = data.get("EvaluationCr");

            //Flag each leg that is to be reported...
            boolean reportDRSide = false;
            boolean reportCRSide = false;

            //Prep data for DR side...
            ArrayList<Map<String, Object>> newDrEvals = new ArrayList<Map<String, Object>>();
            if (DrEvals instanceof Map){
                List<Map<String,Object>> decisions = (List<Map<String,Object>>)((Map<String, Object>)DrEvals).get("Evaluations");
                for (Map<String,Object> decision : decisions){
                    //remove(ignore) all CR eval from the data for the DR side...
                    if ("DR".equalsIgnoreCase((String)decision.get("ReportingSide"))){
                        newDrEvals.add(decision);
                        if (!reportDRSide && decision.get("Reportable") instanceof String && ((String)decision.get("Decision")).equalsIgnoreCase("ReportToRegulator")){
                            reportDRSide = true;
                        }
                    }
                }
                ((Map<String, Object>)DrEvals).put("Evaluations",newDrEvals);
            }

            //Prep data for CR side...
            ArrayList<Map<String, Object>> newCrEvals = new ArrayList<Map<String, Object>>();
            if (CrEvals instanceof Map){
                List<Map<String,Object>> decisions = (List<Map<String,Object>>)((Map<String, Object>)CrEvals).get("Evaluations");
                for (Map<String,Object> decision : decisions){
                    //remove(ignore) all DR eval from the data for the CR side...
                    if ("CR".equalsIgnoreCase((String)decision.get("ReportingSide"))){
                        newCrEvals.add(decision);
                        if (!reportCRSide && decision.get("Reportable") instanceof String && ((String)decision.get("Decision")).equalsIgnoreCase("ReportToRegulator")){
                            reportCRSide = true;
                        }
                    }
                }
                ((Map<String, Object>)CrEvals).put("Evaluations",newCrEvals);
            }

            if (reportDRSide && reportCRSide){
                data.put("ReportForBothSides", true);
            }

            //Compose for DR side...
            if (DrEvals instanceof Map){
                data.put("ComposeFor", "DR");
                result.put("DR", FreemarkerUtil.composeTemplateFile(templateDirectory, rootTemplate, data, logger));
            }

            //Compose for CR side...
            if (CrEvals instanceof Map){
                data.put("ComposeFor", "CR");
                result.put("CR", FreemarkerUtil.composeTemplateFile(templateDirectory, rootTemplate, data, logger));
            }

            data.remove("ComposeFor");
            data.put("Composed", result);

        }catch(Exception e){
            logger.error("Cannot compose template");
        }
        return result;
    }


    @Deprecated
    public static String composeTemplate(String templateDirectory, String rootTemplate, Map<String, Object> data) throws Exception {
        String result = null;
        try {
            result = FreemarkerUtil.composeTemplateFile(templateDirectory, rootTemplate, data, logger);
            data.put("Composed", result);
        }catch(Exception e){
            logger.error("Cannot compose template");
        }
        return result;
    }

    /**
     * Freemarker templates are used to create a BOP report based on the values provided in the params
     * This is the bulk version - threads are utilized to generate this reports in parallel
     * @param params
     * @param threadPoolSize
     * @throws Exception
     */
    @Deprecated
    public static void bulkGenerateBopReport( Map<String, Map<String, Object>> params, Integer threadPoolSize,String finsurvReportTemplateDirectory, String rootFinsurvReportTemplate) throws Exception{
        //number of threads = maximum( number of CPU cores, sqrt(number of items to process))
        //for generation of data, or evaluation, or document validations, stick to the standard number of threads (x1)
        //for validation od BOP reports, multiply the thread count by factor of ... (~1.5x ... 2x) to account for external validation calls
        if(!(threadPoolSize instanceof Integer)){
            int recordCount = params.size();
            int cores = Runtime.getRuntime().availableProcessors();
            int maxThreads = 100;
            threadPoolSize = new Integer((int) Math.min(Math.max(cores, Math.sqrt(recordCount) ),maxThreads));
        }

        @Deprecated
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        for (Map.Entry<String, Map<String, Object>> entry: params.entrySet()){
            Runnable worker = new GenerateBopReportRunnable(entry.getKey(), (Map<String, Object>) entry.getValue(), finsurvReportTemplateDirectory, rootFinsurvReportTemplate);
            executor.execute(worker);
        }
        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
        // Wait until all threads are finish
        executor.awaitTermination(10, TimeUnit.MINUTES);
    }

    /**
     * Freemarker templates are used to create a BOP report based on the values provided in the params
     * This is the bulk version - threads are utilized to generate this reports in parallel
     * @param params
     * @param threadPoolSize
     * @throws Exception
     */
    public static void bulkGenerateDrCrBopReport( Map<String, Map<String, Object>> params, Integer threadPoolSize,String finsurvReportTemplateDirectory, String rootFinsurvReportTemplate) throws Exception{

        ArrayList<IFinsurvRunnable> workers = new ArrayList<IFinsurvRunnable>();
        for (Map.Entry<String, Map<String,Object>> entry: params.entrySet()){
            workers.add(generateDrCrBopReportRunnable(entry.getKey(), (Map<String, Object>) entry.getValue(), finsurvReportTemplateDirectory, rootFinsurvReportTemplate));
        }
        boolean isComplete = false;
        while (!isComplete){
            isComplete = true;
            for (IFinsurvRunnable worker : workers){
                if (worker.getStatus() != IFinsurvRunnable.FinsurvRunnableState.COMPLETE){
                    isComplete = false;
                    break;
                }
            }
            if (!isComplete){
                Thread.sleep(50);
            }
        }
//        System.out.println("Bulk Report Generation call complete");
//        for (String trnReference: params.keySet()){
//            System.out.println("\t\t-\t"+trnReference);
//        }
        System.out.println("\n");
    }

    private static IFinsurvRunnable generateDrCrBopReportRunnable(String trnReference, Map<String, Object> params, String finsurvReportTemplateDirectory, String rootFinsurvReportTemplate) {
        IFinsurvRunnable worker = new GenerateDrCrBopReportRunnable(trnReference, params, finsurvReportTemplateDirectory, rootFinsurvReportTemplate);
        FinsurvLocal.getInstance().executeWithProcessingThreadPool(worker);
        return worker;
    }

}
