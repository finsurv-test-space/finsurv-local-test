package za.co.synthesis.finsurvlocal.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;

import java.time.LocalDateTime;
import java.util.Map;

//TODO: Complete (client-friendly) java doc for the code below...

public class GenerateDrCrBopReportRunnable implements IFinsurvRunnable {

    public String templateDirectory = null;
    public String rootTemplate = null;
    Map<String, Object> params;
    String trnReference;
    private volatile FinsurvRunnableState status = FinsurvRunnableState.NEW;


    private static final Logger logger = LoggerFactory.getLogger(GenerateDrCrBopReportRunnable.class);

    public GenerateDrCrBopReportRunnable(String trnReference, Map<String, Object> params, String templateDirectory, String rootTemplate) {
        this.params = params;
        this.templateDirectory = templateDirectory;
        this.rootTemplate = rootTemplate;
        this.trnReference = trnReference;
    }
    @Override
    public void run() {
        setBusyStatus();
        try {
            GenerateBopReport.composeDrCrTemplate(templateDirectory, rootTemplate, params);
            logger.info("TimeStamp for trnRef "  + trnReference + " after template has been composed: " + LocalDateTime.now());
            setCompleteStatus();
        } catch (Exception e) {
            e.printStackTrace();
            setErrorStatus();
        }
    }

    @Override
    public FinsurvRunnableState getStatus() {
        return status;
    }

    @Override
    public FinsurvRunnableState setBusyStatus() {
        FinsurvRunnableState prevStatus = status;
        status = FinsurvRunnableState.BUSY;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setCompleteStatus() {
        FinsurvRunnableState prevStatus = status;
        status = FinsurvRunnableState.COMPLETE;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setErrorStatus() {
        FinsurvRunnableState prevStatus = status;
        status = FinsurvRunnableState.ERROR;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setStatus(FinsurvRunnableState newState) {
        FinsurvRunnableState prevStatus = status;
        status = newState;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setIdleStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.IDLE;
        return lastState;
    }

}
