package za.co.synthesis.finsurvlocal.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.FinsurvLocal;

import java.time.LocalDateTime;
import java.util.Map;

//TODO: I believe this should be deprecated - to be checked!
@Deprecated
public class GenerateBopReportRunnable implements Runnable{

    public String templateDirectory = null;
    public String rootTemplate = null;
    Map<String, Object> params;
    String trnReference;


    private static final Logger logger = LoggerFactory.getLogger(GenerateBopReportRunnable.class);

    public GenerateBopReportRunnable(String trnReference, Map<String, Object> params, String templateDirectory, String rootTemplate) {
        this.params = params;
        this.templateDirectory = templateDirectory;
        this.rootTemplate = rootTemplate;
        this.trnReference = trnReference;
    }
    @Override
    public void run() {
        try {
            GenerateBopReport.composeTemplate(templateDirectory, rootTemplate, params);
            logger.info("TimeStamp for trnRef "  + trnReference + " after template has been composed: " + LocalDateTime.now());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
