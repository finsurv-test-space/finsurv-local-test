package za.co.synthesis.finsurvlocal.report.types;

public class JsonConstant {
    public static String MonetaryAmount = "MonetaryAmount";
    public static String ImportExport = "ImportExport";
    public static String UniqueObjectInstanceID = "#UOIID";
    public static String SequenceNumber = "SequenceNumber";
}

