package za.co.synthesis.finsurvlocal.report.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

//TODO: Complete (client-friendly) java doc for the code below...

/**
 * Copied from RDS
 */
@JsonPropertyOrder({
        "Meta"
        , "Report"
})
public class ReportData {
  private Map<String, Object> meta;
  private Map<String, Object> report;

  public ReportData() {
    meta = new HashMap<String, Object>();
    report = new HashMap<String, Object>();
  }

  public ReportData(Map<String, Object> meta, Map<String, Object> report) {
    this.meta = (meta != null) ? meta : new HashMap<String, Object>();
    this.report = (report != null) ? report : new HashMap<String, Object>();
  }

  @JsonProperty("Meta")
  public Map<String, Object> getMeta() {
    return meta;
  }

  @JsonProperty("Report")
  public Map<String, Object> getReport() {
    return report;
  }

  @JsonIgnore
  public String getTrnReference() {
    return (String)getReport().get("TrnReference");
  }

}
