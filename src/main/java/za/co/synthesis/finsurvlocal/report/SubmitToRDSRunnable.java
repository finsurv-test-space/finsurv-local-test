package za.co.synthesis.finsurvlocal.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;

import java.time.LocalDateTime;

//TODO: Complete (client-friendly) java doc for the code below...

public class SubmitToRDSRunnable implements IFinsurvRunnable {
    private String trnReference;
    String bopReport;
    String channelName;
    boolean performUpload = true;
    private Logger logger = LoggerFactory.getLogger(SubmitToRDSRunnable.class);
    private volatile FinsurvRunnableState status = FinsurvRunnableState.NEW;

    public SubmitToRDSRunnable( String bopReport, String channelName, String trnReference) {
        this.bopReport = bopReport;
        this.channelName = channelName;
        this.trnReference = trnReference;
    }
    @Override
    public void run() {
        if (performUpload){
            //Upload the BOP report to RDS
            this.setBusyStatus();
            ReportServicesApi.submitSingleReportToRDS(channelName,bopReport);
//            System.out.println("TimeStamp for trnRef " + trnReference+" after data has been submitted: " + LocalDateTime.now());
            this.setCompleteStatus();
        }
        else{
            //Simulate an upload of a BOP report to RDS
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public FinsurvRunnableState getStatus() {
        return status;
    }

    @Override
    public FinsurvRunnableState setBusyStatus() {
        FinsurvRunnableState prevStatus = status;
        status = FinsurvRunnableState.BUSY;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setCompleteStatus() {
        FinsurvRunnableState prevStatus = status;
        status = FinsurvRunnableState.COMPLETE;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setErrorStatus() {
        FinsurvRunnableState prevStatus = status;
        status = FinsurvRunnableState.ERROR;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setStatus(FinsurvRunnableState newState) {
        FinsurvRunnableState prevStatus = status;
        status = newState;
        return prevStatus;
    }

    @Override
    public FinsurvRunnableState setIdleStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.IDLE;
        return lastState;
    }
}
