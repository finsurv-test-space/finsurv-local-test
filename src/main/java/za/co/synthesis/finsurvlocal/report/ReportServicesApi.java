package za.co.synthesis.finsurvlocal.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.utils.ConnectionUtil;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static za.co.synthesis.finsurvlocal.utils.ConnectionUtil.getHttpConnectionResponse;
import static za.co.synthesis.finsurvlocal.utils.JsonUtils.jsonStrToMap;
import static za.co.synthesis.finsurvlocal.utils.JsonUtils.mapToJsonStr;

//TODO: Complete (client-friendly) java doc for the code below...
public class ReportServicesApi {
    public static Logger logger = LoggerFactory.getLogger(ReportServicesApi.class);

    public static String utf8 = "UTF-8";
    public static String username;
    public static String password;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        ReportServicesApi.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        ReportServicesApi.password = password;
    }

    public static void submitSingleReportToRDS(String channelName, JSObject reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    public static void submitSingleReportToRDS(String channelName, Map<String, Object> reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    public static void submitSingleReportToRDS(String channelName, List<Object> reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    public static void submitSingleReportToRDS(String channelName, JSArray reportData){
        submitSingleReportToRDS(channelName, mapToJsonStr(reportData));
    }

    public static void submitSingleReportToRDS(String channelName, String reportData){
        HttpURLConnection connection = null;
        String url = FinsurvLocal.rdsURL+"producer/api/report/data";
        try {
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String> headers = new HashMap<String, String>();

            params.put("channelName", channelName);
            headers.put("Content-Type", "application/json; charset=UTF-8");
            headers.put("Accept", "application/json");

            // The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
            /*#if OLDDATE
                String encoded = DatatypeConverter.printBase64Binary((username+":"+password).getBytes(utf8));
#else*/
            String encoded = Base64.getEncoder().encodeToString((username+":"+password).getBytes(utf8));  //Java 8
//#endif

            headers.put("Authorization", "Basic "+encoded);
            url = CommonHelperFunctions.addParamsToHttpConnection(url, params);

            connection = ConnectionUtil.getHttpConnection(url,"POST");

            connection = CommonHelperFunctions.addHeadersToHttpConnection(connection, headers);

            connection.setDoOutput(true);
            OutputStream out = connection.getOutputStream();
            out.write(reportData.getBytes(utf8));
            out.flush();
            out.close();

            String response = getHttpConnectionResponse(channelName,connection).toString();
            logger.info("\n\nResponse: " + response);
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
        }

    }

    public static void submitBulkReportsToRDS(String channelName, String reportData){
        HttpURLConnection connection = null;
        String schema = "genv3";
        String url = FinsurvLocal.rdsURL+"producer/api/upload/reports/async/data/obj/"+schema;
        try {
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String> headers = new HashMap<String, String>();

            params.put("channelName", channelName);
            headers.put("Content-Type", "application/json; charset=UTF-8");
            headers.put("Accept", "application/json");

            // The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
            /*#if OLDDATE
                String encoded = DatatypeConverter.printBase64Binary((username+":"+password).getBytes(utf8));
#else*/
            String encoded = Base64.getEncoder().encodeToString((username+":"+password).getBytes(utf8));  //Java 8
//#endif

            headers.put("Authorization", "Basic "+encoded);
            url = CommonHelperFunctions.addParamsToHttpConnection(url, params);

            connection = ConnectionUtil.getHttpConnection(url,"POST");

            connection = CommonHelperFunctions.addHeadersToHttpConnection(connection, headers);
            connection.setDoOutput(true);
            OutputStream out = connection.getOutputStream();
            out.write(reportData.getBytes(utf8));
            out.flush();
            out.close();

            String response = getHttpConnectionResponse(channelName,connection).toString();
            logger.info("\n\nResponse: " + response);
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
        }

    }

    @Deprecated
    public static void bulkSubmitSingleBopReport(String channelName, Map<String, Map<String, Object>> data, Integer threadPoolSize) throws Exception{
        /*
            THOUGHTS:
            ---------
            1)  The submit SHOULD have it's OWN threadpool - as per the below.
                ...the reason being that the submission is not very CPU intensive - it's basically firing off a report, and waiting for response from server.
            2)  How do we decide on the size of the threadpool?
                ...just allocate one thread for each report?
                ...perhaps a divider - IE: 1/2 or 1/4 * the number of reports.
                ...or do we leave this as a parameter?
         */
        if(!(threadPoolSize instanceof Integer)) {
            threadPoolSize = new Integer((int) data.size());
        }
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        for(Map.Entry<String, Map<String, Object>> entry : data.entrySet()){
            Runnable worker = new SubmitToRDSRunnable((String) entry.getValue().get("Composed"),channelName,entry.getKey());
            executor.execute(worker);
        }
        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
        // Wait until all threads are finish
        executor.awaitTermination(10, TimeUnit.MINUTES);
    }

    public static void bulkSubmitDrCrBopReport(String channelName, Map<String, Map<String, Object>> data, Integer threadPoolSize) throws Exception{
        /*
            THOUGHTS:
            ---------
            1)  The submit SHOULD have it's OWN threadpool - as per the below.
                ...the reason being that the submission is not very CPU intensive - it's basically firing off a report, and waiting for response from server.
            2)  How do we decide on the size of the threadpool?
                ...just allocate one thread for each report?
                ...perhaps a divider - IE: 1/2 or 1/4 * the number of reports.
                ...or do we leave this as a parameter?
         */

        ArrayList<IFinsurvRunnable> workers = new ArrayList<IFinsurvRunnable>();
        for (Map.Entry<String, Map<String,Object>> entry: data.entrySet()){
            /*
            TODO: We could add network optimisation by providing a batching configuration that allows the function to batch a certain number of reports together and allocate to a single thread for submission.
                alternatively, or in addition, we could also allow a size limit to be specified for the payload to be submitted on each thread.
             */
            String drReport = (String)((HashMap) entry.getValue().get("Composed")).get("DR");
            String crReport = (String)((HashMap) entry.getValue().get("Composed")).get("CR");
            if(drReport != null){
                workers.add(submitDrCrBopReportRunnable(drReport,channelName,entry.getKey()));
            }
            if(crReport != null){
                workers.add(submitDrCrBopReportRunnable(crReport,channelName,entry.getKey()));
            }
        }

        boolean isComplete = false;
        while (!isComplete){
            isComplete = true;
            for (IFinsurvRunnable worker : workers){
                if (worker.getStatus() != IFinsurvRunnable.FinsurvRunnableState.COMPLETE){
                    isComplete = false;
                    break;
                }
            }
            if (!isComplete){
                Thread.sleep(50);
            }
        }
//        System.out.println("Bulk Report Submission call complete");

        System.out.println("\n");
    }

    private static IFinsurvRunnable submitDrCrBopReportRunnable(String bopReport, String channelName, String trnRef){
        IFinsurvRunnable worker = new SubmitToRDSRunnable(bopReport,channelName,trnRef);
        FinsurvLocal.getInstance().executeWithSubmissionThreadPool(worker);
        return worker;
    }

    @Deprecated
    public static void bulkSubmitBopReport(String channelName,  Map<String, Map<String, Object>> data) throws Exception {
        List<Map<String,Object>> reports = new ArrayList<Map<String,Object>>();
        for(Map.Entry<String, Map<String, Object>> entry : data.entrySet()){
            //The composed report is already in string format... we would need to parse it into a map if we want to make use of the JSON writer...
            Map<String, Object> report = jsonStrToMap((String) entry.getValue().get("Composed"));
            reports.add(report);
        }

        Map<String, Object> reportsMap = new HashMap<String, Object>();
        reportsMap.put("Reports",reports);
        String jsonStr = JsonUtils.mapToJsonStr(reportsMap);
        submitBulkReportsToRDS(channelName, jsonStr);
//        System.out.println("I am done with the reports");
    }

    public static void bulkSubmitDrCrBopReport(String channelName,  Map<String, Map<String, Object>> data) throws Exception {
        List<Map<String,Object>> reports = new ArrayList<Map<String,Object>>();
        for(Map.Entry<String, Map<String, Object>> entry : data.entrySet()){
            //The composed report is already in string format... we would need to parse it into a map if we want to make use of the JSON writer...
            if (entry.getValue().get("Composed") instanceof String) {
                Map<String, Object> report = jsonStrToMap((String) entry.getValue().get("Composed"));
                reports.add(report);
            } else if (entry.getValue().get("Composed") instanceof Map) {
                Map<String, String> composed = (Map<String, String>) entry.getValue().get("Composed");
                for (Map.Entry<String, String> composedEntry : composed.entrySet()) {
                    Map<String, Object> report = jsonStrToMap(composedEntry.getValue());
                    reports.add(report);
                }
            }
        }

        Map<String, Object> reportsMap = new HashMap<String, Object>();
        reportsMap.put("Reports",reports);
        String jsonStr = JsonUtils.mapToJsonStr(reportsMap);
        submitBulkReportsToRDS(channelName, jsonStr);
//        System.out.println("I am done with the reports");
    }

    public static String getSingleReportFromRDS(String channelName, String trnRef){
        HttpURLConnection connection = null;
        String url = FinsurvLocal.rdsURL+"producer/api/report/data";
        try {
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String> headers = new HashMap<String, String>();

            params.put("trnReference", trnRef);
            params.put("channelName", channelName);

            //  The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
             /*#if OLDDATE

                String encoded = DatatypeConverter.printBase64Binary((username+":"+password).getBytes(utf8));
#else*/
            String encoded = Base64.getEncoder().encodeToString((username+":"+password).getBytes(utf8));  //Java 8
//#endif
            headers.put("Authorization", "Basic "+encoded);
            url = CommonHelperFunctions.addParamsToHttpConnection(url, params);

            connection = ConnectionUtil.getHttpConnection(url,"GET");

            connection = CommonHelperFunctions.addHeadersToHttpConnection(connection, headers);

            String response =  ConnectionUtil.getHttpConnectionResponse(channelName,connection).toString();
            logger.info("\n\nResponse: " + response);
            return response;
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
            return null;
        }

    }

    public static String getReportHistoryFromRDS(String channelName, String trnRef){
        HttpURLConnection connection = null;
        String url = FinsurvLocal.rdsURL+"producer/api/report/historyTest";
        try {
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String> headers = new HashMap<String, String>();

            params.put("trnReference", trnRef);
            params.put("channelName", channelName);

            //  The preprocessor plugin uses the java 6 compatible libraries when building the build16.gradle file. The default build.gradle uses java 8 compatible libraries.
             /*#if OLDDATE

                String encoded = DatatypeConverter.printBase64Binary((username+":"+password).getBytes(utf8));
#else*/
            String encoded = Base64.getEncoder().encodeToString((username+":"+password).getBytes(utf8));  //Java 8
//#endif
            headers.put("Authorization", "Basic "+encoded);
            url = CommonHelperFunctions.addParamsToHttpConnection(url, params);

            connection = ConnectionUtil.getHttpConnection(url,"GET");

            connection = CommonHelperFunctions.addHeadersToHttpConnection(connection, headers);

            String response =  ConnectionUtil.getHttpConnectionResponse(channelName,connection).toString();
            logger.info("\n\nResponse: " + response);
            return response;
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
            return null;
        }

    }
}
