package za.co.synthesis.finsurvlocal.task;

import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.evaluation.Evaluation;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.validation.Validation;

/**
 * This class refreshes the artefacts in the background in a thread safe manner
 */
public class ArtefactBackgroundWorker implements IFinsurvRunnable {

    private FinsurvRunnableState status = FinsurvRunnableState.NEW;
    private long interval = 60*1000;//30*60*1000; //30min
    private boolean enabled = true;
    private boolean killed = false;



    @Override
    public synchronized IFinsurvRunnable.FinsurvRunnableState getStatus() {
        return this.status;
    }

    @Override
    public synchronized FinsurvRunnableState setBusyStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.BUSY;
        return lastState;
    }

    @Override
    public synchronized FinsurvRunnableState setCompleteStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.COMPLETE;
        return lastState;
    }

    @Override
    public FinsurvRunnableState setErrorStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.ERROR;
        return lastState;
    }

    @Override
    public synchronized FinsurvRunnableState setStatus(FinsurvRunnableState newState) {
        FinsurvRunnableState lastState = this.status;
        this.status = newState;
        return lastState;
    }

    @Override
    public FinsurvRunnableState setIdleStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.IDLE;
        return lastState;
    }

    public long setSleepHours(int hours){
        return setSleepMinutes(hours * 60);
    }

    public long setSleepMinutes(int minutes){
        return setSleepSeconds(minutes * 60);
    }

    public long setSleepSeconds(int seconds){
        return setSleepMilliseconds(seconds * 1000);
    }

    public long setSleepMilliseconds(long milliseconds){
        return interval = milliseconds;
    }

    public void stop(){
        killed = true;
    }

    public void disable(){
        enabled = false;
    }

    public void enable(){
        enabled = true;
    }

    @Override
    public void run() {
        this.setBusyStatus();
        while (!killed){
            if (enabled){
                // TODO: This needs to be split into two workers, one to update artefacts and one to update the instances
                //  The channel update artefact worker would not run in EE's case or would not reach RDS and may fail
                FinsurvLocal.getInstance().updateAllChannelsArtefacts();
                // TODO: The refresh expired instances still needs to run (it will refresh based on whatever is in the directory so that it will pick up manual changes)
                Validation.refreshExpiredInstances();
                Evaluation.refreshExpiredInstances();
            }
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                System.err.println("Unable to sleep the background task thread - Error: "+e.getMessage());
                e.printStackTrace();
                killed = true; // terminate?
            }
        }
        this.setCompleteStatus();
    }
}
