package za.co.synthesis.finsurvlocal.validation;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Vector;

public class FinsurvEngineInstancePool<T> {
    private final String packageName;
    /**
     * Number of minutes for an instance to live. Defaulted to 60 minutes (1 hour) before expiring and thus needing to be refreshed.
     */
    public int minutesToLive;
    public int minInstances; //MUST BE >= 1
    public int maxInstances; //MUST BE >= min_instances
    public int nextIndex = 0;
    public Boolean hasExpiredInstances = new Boolean(false);
    public Vector instances = new Vector<T>();
    public Vector instanceExpires = new Vector<LocalDateTime>();
    //public *** instanceMarshaller; //This is a background thread instance of a marshaller class implementation to monitor and refresh the instances as needed.  It *SHOULD* also check if *ALL* available threads are busy and add new instances as needed.

    public FinsurvEngineInstancePool(String packageName, int minutesToLive, Collection<T> initialInstances){
        this(packageName, 1, 1, minutesToLive, initialInstances);
    }

    public FinsurvEngineInstancePool(String packageName, int minInstances, int maxInstances, int minutesToLive, Collection<T> initialInstances){
        assert(initialInstances != null && initialInstances.size() > 0);
        this.instances.addAll(initialInstances);
        for (int index=0; index < instances.size(); index++){
            instanceExpires.add(LocalDateTime.now().plusMinutes(minutesToLive));
        }

        assert(minInstances > 0);
        minInstances = Math.max(minInstances,initialInstances.size());
        this.minInstances = minInstances;

        maxInstances = Math.max(maxInstances,initialInstances.size());
        assert(maxInstances >= minInstances);
        this.maxInstances = maxInstances;

        assert(minutesToLive >= 1);
        this.minutesToLive = minutesToLive;

        assert(packageName != null && !packageName.isEmpty());
        this.packageName = packageName;

    }


    public T getInstance() throws ArrayIndexOutOfBoundsException{
        T returnInstance = null;
        nextIndex = (instances.size() > 0) ? (nextIndex % instances.size()) : (0);//Ensure that the index never exceeds the size of the list.
        if (nextIndex >= 0 && nextIndex <instances.size()){
            returnInstance = (T) instances.get(nextIndex);
            if (((LocalDateTime)instanceExpires.get(nextIndex)).isBefore(LocalDateTime.now())){
                hasExpiredInstances = true;
            }
            nextIndex++;
            nextIndex = nextIndex % instances.size();
        } else {
            throw new ArrayIndexOutOfBoundsException("The Worker Pool has no valid instances available from which to procure an instance.  Please allow time for the Instance Marshaller to create ("+ minInstances +" - "+ maxInstances +") new instances for use.");
        }
        //System.out.println(" ** Returning instance "+nextIndex+" from the "+packageName+" "+returnInstance.getClass().getName()+" instance pool ** ");
        return returnInstance;
    };


    public int setInstance (T t){
        //if no instances, or if not yet within the bounds of the *min_instances* value, simply append the instance...
        if (instances.size() < minInstances /*|| instances.size() < max_instances*/){
            instances.add(t);
            instanceExpires.add(LocalDateTime.now().plusMinutes(minutesToLive));
            return instances.indexOf(t);
        }
        //Replace the first expired instance if any.  ...keep track of oldest expiry time too.
        int indexOfOldest = 0;
        for (int index = 0; index < instances.size(); index++){
            if (((LocalDateTime)instanceExpires.get(indexOfOldest)).isAfter((LocalDateTime)instanceExpires.get(index))){
                indexOfOldest = index;
            }
            if (((LocalDateTime)instanceExpires.get(index)).isBefore(LocalDateTime.now())){
                instances.set(index, t);
                instanceExpires.set(index, LocalDateTime.now().plusMinutes(minutesToLive));
                return index;
            }
        }
        //if no expired instances, replace the oldest instance - the oldest index was tracked from the previous for-loop...
        instances.set(indexOfOldest, t);
        instanceExpires.set(indexOfOldest, LocalDateTime.now().plusMinutes(minutesToLive));
        hasExpiredInstances = false;
        return indexOfOldest;
    }

    public synchronized boolean checkForExpiredInstances(){
        boolean hasExpired = false;
        for (int index = 0; index < instances.size(); index++){
            if (((LocalDateTime)instanceExpires.get(index)).isBefore(LocalDateTime.now())){
                hasExpired = true;
                break;
            }
        }
        hasExpiredInstances = hasExpired;
        return hasExpiredInstances;
    }
}
