package za.co.synthesis.finsurvlocal.validation.types;

import java.util.List;

public class Endpoint {
    private String name;
    private boolean failOnBusy;
    private boolean failOnError;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public List<EndpointParameter> getEndpointParameterList() {
        return endpointParameterList;
    }

    public void setEndpointParameterList(List<EndpointParameter> endpointParameterList) {
        this.endpointParameterList = endpointParameterList;
    }

    private String endpoint;
    private List<EndpointParameter> endpointParameterList;

    public Endpoint(String name, String endpoint, List<EndpointParameter> endpointParameter, boolean failOnBusy, boolean failOnError) {
        this.name = name;
        this.endpoint = endpoint;
        this.endpointParameterList = endpointParameter;
        this.failOnBusy = failOnBusy;
        this.failOnError = failOnError;
    }

    public boolean getFailOnBusy() {
        return failOnBusy;
    }

    public void setFailOnBusy(boolean failOnBusy) {
        this.failOnBusy = failOnBusy;
    }

    public boolean getFailOnError() {
        return failOnError;
    }

    public void setFailOnError(boolean failOnError) {
        this.failOnError = failOnError;
    }
}
