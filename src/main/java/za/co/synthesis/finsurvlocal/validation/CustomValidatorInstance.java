package za.co.synthesis.finsurvlocal.validation;

import za.co.synthesis.rule.core.ICustomValidate;


public class CustomValidatorInstance {
    public String validatorName;
    public ICustomValidate validatorInstance;
    public String[] params;

    public CustomValidatorInstance(String name, CustomRestCallValidate implementation, String[] params) {
        this.validatorName = name;
        this.validatorInstance = implementation;
        this.params = params;
    }

    public String getName() {
        return this.validatorName;
    }


    public ICustomValidate getImplementation() {
        return this.validatorInstance;
    }


    public String[] getParameters() {
        return params;
    }
}
