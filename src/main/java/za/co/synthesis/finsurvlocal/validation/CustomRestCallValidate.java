package za.co.synthesis.finsurvlocal.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.types.RdsConfiguration;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;
import za.co.synthesis.finsurvlocal.validation.types.Endpoint;
import za.co.synthesis.finsurvlocal.validation.types.EndpointParameter;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;


public class CustomRestCallValidate implements ICustomValidate {
    private static final Logger logger = LoggerFactory.getLogger(CommonHelperFunctions.class);
    private String protocol = "http";
    private String server = "localhost";
    private String port = "8084";
    private String reportServicesURL = protocol+"://"+server+":"+port+"/report-data-store/";
    private String extValidationPathUrl = "producer/api/validation/dynamic/";
    private String baseURL = reportServicesURL+extValidationPathUrl;//"http://localhost:8083/report-data-store/producer/api/validation/dynamic/";
    private String endpoint;
    private String channelName;
    private boolean failOnError;
    private List<EndpointParameter> urlParameters;
    /**
     * If a transport layer failure occurs (HTTP response != 200) then the validation return type is defaulted to this StatusType.
     * In terms of the validation rules,
     *  StatusType.Error => Soft Error/Warning (Validations still pass)
     *  StatusType.Fail => Hard Error (Validations will fail)
     */
    private StatusType transportFailureType = StatusType.Error;
    /**
     * If an invalid status is provided as part of the validation response (Status not in "Pass", "Fail", "Error") then the validation return type is defaulted to this StatusType.
     * In terms of the validation rules,
     *  StatusType.Error => Soft Error/Warning (Validations still pass)
     *  StatusType.Fail => Hard Error (Validations will fail)
     */
    private StatusType unknownValidationResponseTypeDefault = StatusType.Fail;
    private static final String USER_AGENT = "Mozilla/5.0";


    public CustomRestCallValidate(String channelName, Endpoint e, RdsConfiguration rdsConfiguration){
        this.endpoint = e.getEndpoint();
        this.urlParameters = e.getEndpointParameterList();
        this.channelName = channelName;
        this.failOnError = e.getFailOnError();
        this.transportFailureType = this.failOnError?StatusType.Fail : StatusType.Error;
        this.protocol = rdsConfiguration.getProtocol();
        this.server = rdsConfiguration.getServer();
        this.port = rdsConfiguration.getPort();
        this.reportServicesURL = rdsConfiguration.getUrl();
        this.baseURL = reportServicesURL+extValidationPathUrl;
    }

    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
        //TODO: Create the necessary HTTP GET request to the Report Services Instance...
         String url = this.baseURL + this.endpoint+"?"+"channelName="+channelName;
         logger.info("RUNNING: " + value+"\n");
        int countParameters = 0;
        for(EndpointParameter parameter : urlParameters){
            if (otherInputs[countParameters] instanceof String) {
                url = url + "&" + parameter.getName() + "=" + otherInputs[countParameters];
                logger.info("Parameter: " + parameter.getName() + "\nValue: " + otherInputs[countParameters] + "\n");
            } else {
                logger.info("<SKIPPED> Parameter: " + parameter.getName() + "\nValue: " + otherInputs[countParameters] + "\n");
            }
            countParameters++;
        }
        try {
            URL reportDataStoreURL = new URL(url);
//            logger.info("**Calling URL: "+reportDataStoreURL+"\n\n");
            HttpURLConnection connection = (HttpURLConnection) reportDataStoreURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = connection.getResponseCode();
//            logger.info("GET Response Code :: " + responseCode);
            CustomValidateResult result;
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                String responseStr = response.toString();
                logger.info(responseStr);
                result = new CustomValidateResult(getStatus(responseStr, unknownValidationResponseTypeDefault),getCode(responseStr, null),getMessage(responseStr,null));
            } else {
                result = new CustomValidateResult(transportFailureType,responseCode+"",connection.getResponseMessage());
                logger.info("GET request did not work");
            }
            return result;
        } catch (Exception e) {
            logger.error("Can not connect to " + url);
            return new CustomValidateResult(transportFailureType,"","Failed to connect to the API "+url);
        }
//        return null;
    }

    public static StatusType getStatus(String response, StatusType defaultStatus){
        try {
            Map<String, Object> jso = JsonUtils.jsonStrToMap(response);
            for (Map.Entry<String, Object> entry : jso.entrySet()) {
                if ("Status".equalsIgnoreCase(entry.getKey())) {
                    if (entry.getValue() instanceof String && !((String) entry.getValue()).trim().isEmpty()) {
                        String statusStr = ((String) entry.getValue()).trim();
                        if ("Pass".equalsIgnoreCase(statusStr)) {
                            return StatusType.Pass;
                        } else if ("Error".equalsIgnoreCase(statusStr)) {
                            return StatusType.Error;
                        } else if ("Fail".equalsIgnoreCase(statusStr)) {
                            return StatusType.Fail;
                        }
                    }
                }
            }
        } catch (Exception err){
            logger.error("Could not retrieve the status\n"+err.getMessage());
        }
        return defaultStatus;
    }


    public static String getMessage(String response, String defaultMessage){
        try {
            Map<String, Object> jso = JsonUtils.jsonStrToMap(response);
            for (Map.Entry<String, Object> entry : jso.entrySet()) {
                if ("Message".equalsIgnoreCase(entry.getKey())) {
                    if (entry.getValue() instanceof String && !((String) entry.getValue()).trim().isEmpty()) {
                        return ((String) entry.getValue()).trim();
                    }
                }
            }
        } catch (Exception err){
            logger.error("Could not retrieve the message\n"+err.getMessage());
        }
        return defaultMessage;
    }
    public static String getCode(String response, String defaultCode){
        try {
            Map<String, Object> jso = JsonUtils.jsonStrToMap(response);
            for (Map.Entry<String, Object> entry : jso.entrySet()) {
                if ("Code".equalsIgnoreCase(entry.getKey())) {
                    if (entry.getValue() instanceof String && !((String) entry.getValue()).trim().isEmpty()) {
                        return ((String) entry.getValue()).trim();
                    }
                    if (entry.getValue() instanceof Number) {
                        return ((Number) entry.getValue()).toString();
                    }
                    if (entry.getValue() != null) {
                        return entry.getValue().toString();  //this may just return the Memory address for the object.
                    }
                }
            }
        } catch (Exception err){  logger.error("Could not retrieve the code\n"+err.getMessage());}
        return defaultCode;
    }

}
