package za.co.synthesis.finsurvlocal.validation;

import za.co.synthesis.finsurvlocal.FinsurvLocal;
import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.types.RdsConfiguration;
import za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions;
import za.co.synthesis.finsurvlocal.utils.JsonUtils;
import za.co.synthesis.finsurvlocal.validation.types.Endpoint;
import za.co.synthesis.finsurvlocal.validation.types.EndpointParameter;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.Util;

import java.io.StringReader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

//TODO: allow for custom external validators to be passed/set for the entire class and then used (or implemented if not actually set) in the validation functions lower down.
public class Validation {

    public enum ValidatorInstanceParameter{
        INSTANCE_POOL_MIN("minInstances"),
        INSTANCE_POOL_MAX("maxInstances"),
        TIME_TO_LIVE("ttl");

        private String value;

        ValidatorInstanceParameter(String value) {
            this.value=value;
        }

        public String toString(){
            return this.value;
        }

        public static ValidatorInstanceParameter fromString(String in){
            if(in != null) {
                for(ValidatorInstanceParameter eventType : ValidatorInstanceParameter.values()) {
                    if (in.equalsIgnoreCase(eventType.value)) {
                        return eventType;
                    }
                }
            }
            return null;
        }


    }

    //TODO: we need to provide some mechanisms for managing the number of validators PER PACKAGE...  rather, a map of settings in general (min, max, ttl... etc) per package.
    public static int defaultValidatorInstancesPerPackage = 10;
    public static int defaultValidatorExpiryMinutesPerPackage = 10; //60;
    public static Map<String, FinsurvEngineInstancePool<Validator>> packageValidators = new ConcurrentHashMap<String, FinsurvEngineInstancePool<Validator>>();

    /**
     * Create an instance of the Java Rules Engine Validator, primed with the necessary Rules, Lookups and
     * External(Custom) Validation Stubs.
     *
     * @param channelName               Used to determine the set of artefacts to load and or server-side configurations
     *                                  to reference.
     * @param lookups                   This is the contents of the <i>psuedo-static data</i> file obtained from Report
     *                                  Data Store(or otherwise generated in a compatible JSON format), specific to the
     *                                  channel.
     * @param validationRules           This is the contents of the Validation rules file as obtained from Report Data
     *                                  Store, specific to the channel.
     * @param externalValidationConfigs This is the list of External Validation configurations to generate active stubs
     *                                  for enabling the external validation calls and configuring the required data
     *                                  params, for each respective validation (IE: External call to ARM or IVS) - also
     *                                  obtained from Report Data Store, specific to the channel.
     * @return a validator
     * @throws Exception
     */
    public static synchronized Validator getValidator(String channelName, String lookups, String validationRules, List<Endpoint> externalValidationConfigs, RdsConfiguration rdsConfiguration) throws Exception {
        ArrayList<CustomValidatorInstance> externalValidatorInstances = new ArrayList<CustomValidatorInstance>();

        //REGISTER external, custom validation stubs - these are used to enable specific validation calls to external systems via REST (or DB connections).
        for (Endpoint config : externalValidationConfigs) {

            CustomRestCallValidate implementation = new CustomRestCallValidate(channelName, config,rdsConfiguration);
            List<String> parameters = new ArrayList<String>();
            for (EndpointParameter param : config.getEndpointParameterList()) {
                parameters.add(param.getPath());
            }
            externalValidatorInstances.add(new CustomValidatorInstance(config.getName(), implementation, parameters.toArray(new String[0])));
        }

        return getValidatorWithCustomExtValidators(channelName, lookups, validationRules, externalValidatorInstances, rdsConfiguration);
    }

    /**
     * Create an instance of the Java Rules Engine Validator, primed with the necessary Rules, Lookups and
     * External(Custom) Validation Stubs.
     *
     * @param channelName               Used to determine the set of artefacts to load and or server-side configurations
     *                                  to reference.
     * @param lookups                   This is the contents of the <i>psuedo-static data</i> file obtained from Report
     *                                  Data Store(or otherwise generated in a compatible JSON format), specific to the
     *                                  channel.
     * @param validationRules           This is the contents of the Validation rules file as obtained from Report Data
     *                                  Store, specific to the channel.
     * @param externalValidatorInstances This is the list of External Validation Instances (active stubs)
     *                                  for enabling the external validation calls and configuring the required data
     *                                  params, for each respective validation (IE: External call to ARM or IVS) - also
     *                                  obtained from Report Data Store, specific to the channel.
     * @return a validator
     * @throws Exception
     */
    public static synchronized Validator getValidatorWithCustomExtValidators(String channelName, String lookups, String validationRules, List<CustomValidatorInstance> externalValidatorInstances, RdsConfiguration rdsConfiguration) throws Exception {
        ValidationEngine validationEngine = new ValidationEngine();

        //REGISTER external, custom validation stubs - these are used to enable specific validation calls to external systems via REST (or DB connections).
        for (CustomValidatorInstance instance : externalValidatorInstances) {
            validationEngine.registerCustomValidate(instance.getName(), instance.getImplementation(), instance.getParameters());
        }

        //REGISTER ICustomValidate implementation with the rules engine (to make external Validation calls to Report Services Instance).
        try {
            //Load the rules and lookups to use for validation...
            validationEngine.setupLoadLookup(channelName, new StringReader(lookups));
            validationEngine.setupLoadRule(channelName, new StringReader(validationRules));

            //load supporting context for validation...
            DefaultSupporting supporting = new DefaultSupporting();
            supporting.setGoLiveDate(Util.date("2013-08-19"));
            validationEngine.setupSupporting(supporting);
            validationEngine.setIgnoreMissingCustomValidate(false);

            //TODO: configure the validation caching engine for the validator instance.

            //get a validator instance...
            Validator validator = validationEngine.issueValidator();
            return validator;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Example function to get OR create an instance of a validator for a given channel/package name. This works by
     * fetching the relevant artefacts specific to the channel/package name provided and passing the contents thereof to
     * the new Validator instance - namely:
     * <ul>
     *      <li>Lookups</li>
     *      <li>Validation Rules</li>
     *      <li>External Validation Configurations</li>
     * </ul>
     *
     * @param packageName the channel/package name for which a Validation instance must be returned (if one already
     *                    exists) OR created - with the relevant package artefacts.
     * @return
     */
    public static synchronized Validator getValidatorForPackage(String packageName, RdsConfiguration rdsConfiguration) {
        FinsurvLocal.getInstance().addChannel(packageName);

        //TODO: Add threaded background package updater to trigger updates for items that are about to expire and remove those that have expired.
        //TODO: Provide alternative mechanism for storing artefacts (primarily) in  memory as opposed to on disk - allow for zero disk access etc.
        if (packageValidators.containsKey(packageName)){
            return packageValidators.get(packageName).getInstance();
        } else {
            ArrayList<Validator> initialInstances = new ArrayList<Validator>();
            int instanceCount = getValidatorInstancePoolMin(packageName);
            for (int index = 0; index < instanceCount; index ++){
                try {
                    Validator validator = Validation.getValidator(packageName,
                            CommonHelperFunctions.getArtefactLookups(packageName),
                            getValidationRules(packageName),
                            getExternalValidationConfigList(packageName),
                            rdsConfiguration);
                    initialInstances.add(validator);
                } catch (Exception e) {
                    return null;
                }
            }
            FinsurvEngineInstancePool<Validator> instancePool = new FinsurvEngineInstancePool<Validator>(packageName, getValidatorInstancePoolTtl(packageName), initialInstances);
            packageValidators.put(packageName, instancePool);
            return instancePool.getInstance();
        }
    }

    public static int getValidatorInstancePoolMin(String channelName){
        if (FinsurvLocal.channels != null && FinsurvLocal.channels.size() > 0 && FinsurvLocal.channels.containsKey(channelName)){
            Map<String, Object> channelConfig = FinsurvLocal.channels.get(channelName);
            if (channelConfig.containsKey(ValidatorInstanceParameter.INSTANCE_POOL_MIN) &&
                    channelConfig.get(ValidatorInstanceParameter.INSTANCE_POOL_MIN) != null &&
                    channelConfig.get(ValidatorInstanceParameter.INSTANCE_POOL_MIN) instanceof Integer){
                return ((Integer)channelConfig.get(ValidatorInstanceParameter.INSTANCE_POOL_MIN)).intValue();
            }
        }
        return defaultValidatorInstancesPerPackage;
    }

    public static int getValidatorInstancePoolMax(String channelName){
        if (FinsurvLocal.channels != null && FinsurvLocal.channels.size() > 0 && FinsurvLocal.channels.containsKey(channelName)){
            Map<String, Object> channelConfig = FinsurvLocal.channels.get(channelName);
            if (channelConfig.containsKey(ValidatorInstanceParameter.INSTANCE_POOL_MAX) &&
                    channelConfig.get(ValidatorInstanceParameter.INSTANCE_POOL_MAX) != null &&
                    channelConfig.get(ValidatorInstanceParameter.INSTANCE_POOL_MAX) instanceof Integer){
                return ((Integer)channelConfig.get(ValidatorInstanceParameter.INSTANCE_POOL_MAX)).intValue();
            }
        }
        return defaultValidatorInstancesPerPackage;
    }

    public static int getValidatorInstancePoolTtl(String channelName){
        if (FinsurvLocal.channels != null && FinsurvLocal.channels.size() > 0 && FinsurvLocal.channels.containsKey(channelName)){
            Map<String, Object> channelConfig = FinsurvLocal.channels.get(channelName);
            if (channelConfig.containsKey(ValidatorInstanceParameter.TIME_TO_LIVE) &&
                    channelConfig.get(ValidatorInstanceParameter.TIME_TO_LIVE) != null &&
                    channelConfig.get(ValidatorInstanceParameter.TIME_TO_LIVE) instanceof Integer){
                return ((Integer)channelConfig.get(ValidatorInstanceParameter.TIME_TO_LIVE)).intValue();
            }
        }
        return defaultValidatorExpiryMinutesPerPackage;
    }

    public static void refreshExpiredInstances(){
        for( Map.Entry<String,FinsurvEngineInstancePool<Validator>> packageValidator : packageValidators.entrySet()){
            FinsurvEngineInstancePool<Validator> instancePool = packageValidator.getValue();
            String packageName = packageValidator.getKey();
            while (instancePool.checkForExpiredInstances()){
                Validator validator = null;
                try {
                    validator = Validation.getValidator(packageName,
                            CommonHelperFunctions.getArtefactLookups(packageName),
                            getValidationRules(packageName),
                            getExternalValidationConfigList(packageName),
                            FinsurvLocal.getInstance().getRdsConfig()); //TODO: Should this be a parameter or is this ok???
                    instancePool.setInstance(validator);
                } catch (Exception e) {
                    System.err.println("Unable to create new Validator instances for "+packageName+": "+e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Makes use of the much simplified validate function in rules engine version 2.0.14 onwards The result of this
     * function may be passed into the provided example function to determine if the state of the BOP information is
     * complete/valid or not:
     * <u><b>isValidationSuccessful()</b></u> - see: za.co.synthesis.finsurvlocal.validation.Validation:isValidationSuccessful()
     *
     * @param validator An instance of a Java Rules Engine Validator, with preconfigured rules, lookups and external
     *                  validation configs.
     * @param bopData   The JSON formatted BOP Report (schema: genv3) to be validated by the Java Rules Engine Validator
     *                  instance.
     * @return a list of transaction validation results - if there are no "Error" results, the validation may be
     * considered a pass. "Warning" results provide insight into issues which <i>may</i> cause rejections when
     * submitting to the regulator or are know issues, but would otherwise be ignored by the regulator.
     * @throws Exception
     */
    public static List<ResultEntry> validateJsonBopData(Validator validator, String bopData) throws Exception {
        List<ResultEntry> validationResults = new ArrayList<ResultEntry>();
        if (validator != null && bopData != null) {
            try {
                //-------------------------------------------------------------------//

                //do a report validation...
                ValidationResultsAndStats results = validator.validateJson(bopData);
                validationResults = results.getResults();
                //-------------------------------------------------------------------//

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return validationResults;
    }


    /**
     * Function to simultaneously get/create a Validator instance for the given Channel/Package name, and return:
     * <ul>
     *     <li>a true result to indicate if the provided BOP JSON is valid (with/without "Warning" results)</li>
     *     <li>a false result to indicate if the provided BOP JSON is invalid/incomplete (contains "Error" results)</li>
     * </ul>
     *
     * @param bopData     BOP data provided in the ("genv3") JSON schema format
     * @param packageName The channel/package from which the relevant rules, lookups and external validation
     *                    configurations should be applied.
     * @return
     * @throws Exception
     */
    public static boolean isBopJsonValid(String bopData, String packageName, RdsConfiguration rdsConfiguration) throws Exception {
        Validator validator = Validation.getValidatorForPackage(packageName, rdsConfiguration);
        List<ResultEntry> results = Validation.validateJsonBopData(validator, bopData);
        return isValidationSuccessful(results);
    }


    /**
     *   Use "isValidationSuccessful" function to determine if the validation is successful or not (Boolean result)
     * @param bopData
     * @param packageName
     * @return
     * @throws Exception
     */
    public static List<ResultEntry> validateBopJson(String bopData, String packageName, RdsConfiguration rdsConfiguration) throws Exception {
        Validator validator = Validation.getValidatorForPackage(packageName,rdsConfiguration);
        return Validation.validateJsonBopData(validator, bopData);
    }

    /**
     * A Helper function to fetch and load the contents of the validation rules artefact for the given channel/package name
     * @param packageName the Name of the channel or package from which the artefact should be sourced (IE: sbZA, coreSARB, sbIBR1...)
     * @return String content of the text file
     */
    public static String getValidationRules(String packageName){
        return CommonHelperFunctions.getArtefactValidationRules(packageName);
    }

    /**
     * A Helper function to fetch and load the contents of the external validation configuration artefact for the given channel/package name
     * @param packageName the Name of the channel or package from which the artefact should be sourced (IE: sbZA, coreSARB, sbIBR1...)
     * @return String content of the text file
     */
    public static synchronized List<Endpoint> getExternalValidationConfigList(String packageName) {
        try {
            String fileContent = CommonHelperFunctions.getArtefactExternalValidationConfigs(packageName);
            List<JSObject> configs = JsonUtils.jsonStrToList(fileContent);
            List<Endpoint> endpoints = new ArrayList<Endpoint>();
            for(JSObject config : configs){
//                System.out.println(config);
                List<JSObject> parameters = (List)config.get("parameters");
                EndpointParameter parametersForEndpoint;
                List<EndpointParameter> listOfParameters = new ArrayList<EndpointParameter>();
                for(JSObject parameter : parameters){
                    parametersForEndpoint = new EndpointParameter((String)parameter.get("name"), (String)parameter.get("path"));
                    listOfParameters.add(parametersForEndpoint);
                }
                //TODO: The boolean cast below needs to be hardened so that it doesn't explode if an integer or something else is provided.
                boolean failOnBusy = (config.get("failOnBusy") instanceof String )?
                                            ("true".equalsIgnoreCase((String)config.get("failOnBusy"))) :
                                            (config.get("failOnBusy") == null ? false : (Boolean)config.get("failOnBusy"));
                boolean failOnError = (config.get("failOnError") instanceof String )?
                        ("true".equalsIgnoreCase((String)config.get("failOnError"))) :
                        (config.get("failOnError") == null ? false : (Boolean)config.get("failOnError"));
                Endpoint endpoint = new Endpoint((String)config.get("name"),(String)config.get("endpoint"),listOfParameters, failOnBusy, failOnError);
                endpoints.add(endpoint);
            }
            return endpoints;
        } catch (Exception e) {
            return new ArrayList<Endpoint>();
        }
    }

    /**
     * Helper function to determine if the validation was "successful" - with/without Warnings, but completely devoid of hard ERRORS.
     */
    public static boolean isValidationSuccessful(List<ResultEntry> validationResults){
        boolean success = true;
        //System.out.println("LibraEval() isValidationSuccessful() validationResults=="+validationResults);
        if(validationResults != null) {
            for (ResultEntry entry : validationResults) {
                if (entry.getType() == ResultType.Error) { //only errors are considered as failures.  Warnings neither denote a failure or success - they are simply warnings and are configurable per channel.
                    System.out.println("VALIDATION ERROR: " + entry.getCode() + " - " + entry.getName() + " :: " + entry.getMessage());
                    success = false;
                } else if (entry.getType() == ResultType.Warning) { //only errors are considered as failures.  Warnings neither denote a failure or success - they are simply warnings and are configurable per channel.
                    System.out.println("VALIDATION WARNING: " + entry.getCode() + " - " + entry.getName() + " :: " + entry.getMessage());
                }
            }
        }
        return success;
    }


    public static Map<String, Map<String, Object>> bulkValidateTransactionData(
            String channelName,
            List<String> transactionJsonStrings,
            RdsConfiguration rdsConfiguration) throws Exception {
        Map<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>(transactionJsonStrings.size(),0.5f);

        for(String transactionString: transactionJsonStrings){
            Map<String, Object> transaction = JsonUtils.jsonStrToMap(transactionString);
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("Composed",transaction);
            Map<String,Object> report = (Map<String, Object>) transaction.get("Report");
            data.put((String) report.get("TrnReference"),params);
        }
        return bulkValidateTransactionData(channelName, data, null,rdsConfiguration);
    }

    public static Map<String, Map<String, Object>> bulkValidateTransactionData(
            String channelName,
            Map<String, Map<String, Object>> data,
            RdsConfiguration rdsConfiguration) throws Exception {
        return bulkValidateTransactionData(channelName, data, null,rdsConfiguration);
    }

    @Deprecated
   public static Map<String, Map<String, Object>> bulkValidateTransactionData(
        String channelName,
        Map<String, Map<String, Object>>  data,
        Integer threadPoolSize,
        RdsConfiguration rdsConfiguration) throws Exception {
       //number of threads = maximum( number of CPU cores, sqrt(number of items to process))
       //for generation of data, or evaluation, or document validations, stick to the standard number of threads (x1)
       //for validation od BOP reports, multiply the thread count by factor of ... (~1.5x ... 2x) to account for external validation calls
       if (!(threadPoolSize instanceof Integer)) {
           int recordCount = data.size();
           int cores = Runtime.getRuntime().availableProcessors();
           int maxThreads = 100;
           threadPoolSize = new Integer((int) Math.min(Math.max(cores, Math.sqrt(recordCount)), maxThreads));
       }

//       ConcurrentHashMap<String, Map<String, Object>> data = new ConcurrentHashMap<String, Map<String, Object>>(data.size(), 0.5f);
       ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
       for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()) {
           Map<String, Object> params = entry.getValue();
           Runnable worker = new ValidateRunnable(channelName, entry.getKey(),(String) params.get("Composed"), params, rdsConfiguration);
           executor.execute(worker);
       }
        // Wait until all threads are finish
        executor.awaitTermination(10, TimeUnit.MINUTES);

        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
       return data;
   }

    /**
     * Clears the cached package validators
     */
    public static void clearCachedValidators(){
        packageValidators.clear();
    }

    public static void validateBopReport(String channelName,
                                         String trnReference,
                                         String composedBopReport,
                                         Map<String, Object> params,
                                         RdsConfiguration rdsConfiguration){
        List<ResultEntry> validationResult = null;
        try {
            validationResult = validateBopJson(composedBopReport, channelName,rdsConfiguration);
//            System.out.println("TimeStamp for trnRef " + trnReference+" after data has been validated: " + LocalDateTime.now());
            params.put("ValidationResult", validationResult);
        } catch (Exception e) {
            System.out.println("Exception caught doing full example for bulk loading.\n\n"+e.getMessage());
        }
    }

    public static void validateDrCrBopReport(String channelName,
                                         String trnReference,
                                         Map<String,String> composedBopReports,
                                         Map<String, Object> params,
                                         RdsConfiguration rdsConfiguration){
        List<ResultEntry> validationResult = null;
        for (Map.Entry<String, String> entry : composedBopReports.entrySet()) {
            try {
                validationResult = validateBopJson(entry.getValue(), channelName, rdsConfiguration);
//            System.out.println("TimeStamp for trnRef " + trnReference+" after data has been validated: " + LocalDateTime.now());
                params.put("ValidationResult"+entry.getKey(), validationResult);
            } catch (Exception e) {
                System.out.println("Exception caught during Dr/Cr report validation ("+trnReference +" - "+entry.getKey()+").\n\n" + e.getMessage());
            }
        }
    }


    public static IFinsurvRunnable validateTransactionData(
            String channelName,
            String trnReference,
            Map<String,String> composedBopReports,
            Map<String, Object> params,
            RdsConfiguration rdsConfiguration) {

        IFinsurvRunnable worker = new ValidateDrCrReportRunnable(channelName,trnReference,composedBopReports, params, rdsConfiguration);
        FinsurvLocal.getInstance().executeWithProcessingThreadPool(worker);
        return worker;
    }

    public static Map<String, Map<String, Object>> bulkValidateDrCrTransactionData(
            String channelName,
            Map<String, Map<String, Object>>  data,
            RdsConfiguration rdsConfiguration) throws Exception {
        ArrayList<IFinsurvRunnable> workers = new ArrayList<IFinsurvRunnable>();
        for (Map.Entry<String, Map<String,Object>> params: data.entrySet()){
            Map<String, Object> param = params.getValue();
            workers.add(validateTransactionData(channelName, params.getKey(),(Map<String,String>) param.get("Composed"), param, rdsConfiguration));
        }
        boolean isComplete = false;
        while (!isComplete){
            isComplete = true;
            for (IFinsurvRunnable worker : workers){
                if (worker.getStatus() != IFinsurvRunnable.FinsurvRunnableState.COMPLETE){
                    isComplete = false;
                    break;
                }
            }
            if (!isComplete){
                Thread.sleep(50);
            }
        }

//        System.out.println("Bulk Validation call complete for the following Transactions: ");
//        for (String trnReference: data.keySet()){
//            System.out.println("\t\t-\t"+trnReference);
//        }
        System.out.println("\n");
        return data;
    }

}