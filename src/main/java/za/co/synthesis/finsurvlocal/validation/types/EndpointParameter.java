package za.co.synthesis.finsurvlocal.validation.types;

public class EndpointParameter {
        private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private String path;

    public EndpointParameter(String name, String path) {
        this.name = name;
        this.path = path;
    }
}
