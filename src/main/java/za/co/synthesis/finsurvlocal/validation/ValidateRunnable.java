package za.co.synthesis.finsurvlocal.validation;

import za.co.synthesis.finsurvlocal.types.IFinsurvRunnable;
import za.co.synthesis.finsurvlocal.types.RdsConfiguration;

import java.util.Map;

public class ValidateRunnable implements IFinsurvRunnable {

    private Map<String, Object> params;
    private String channelName;
    private String composedBopReport;
    private String trnReference;
    private RdsConfiguration rdsConfiguration;
    private FinsurvRunnableState status = FinsurvRunnableState.NEW;

    public ValidateRunnable(String channelName, String trnReference, String composedBopReport, Map<String, Object> params, RdsConfiguration rdsConfiguration) {
        this.channelName = channelName;
        this.composedBopReport = composedBopReport;
        this.trnReference = trnReference;
        this.params = params;
        this.rdsConfiguration = rdsConfiguration;
    }


    @Override
    public void run() {
        this.setBusyStatus();
        Validation.validateBopReport(channelName,trnReference,composedBopReport,params, rdsConfiguration);
        this.setCompleteStatus();
    }


    @Override
    public synchronized FinsurvRunnableState getStatus() {
        return this.status;
    }

    @Override
    public synchronized FinsurvRunnableState setBusyStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.BUSY;
        return lastState;
    }

    @Override
    public synchronized FinsurvRunnableState setCompleteStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.COMPLETE;
        return lastState;
    }

    @Override
    public FinsurvRunnableState setErrorStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.ERROR;
        return lastState;
    }

    @Override
    public synchronized FinsurvRunnableState setStatus(FinsurvRunnableState newState) {
        FinsurvRunnableState lastState = this.status;
        this.status = newState;
        return lastState;
    }

    @Override
    public FinsurvRunnableState setIdleStatus() {
        FinsurvRunnableState lastState = this.status;
        this.status = FinsurvRunnableState.IDLE;
        return lastState;
    }
}
