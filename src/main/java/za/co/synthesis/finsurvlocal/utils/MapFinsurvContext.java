package za.co.synthesis.finsurvlocal.utils;


import za.co.synthesis.rule.core.IFinsurvContext;
import za.co.synthesis.rule.core.IFinsurvContextCache;
import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.rule.core.Scope;
import za.co.synthesis.rule.support.DisplayRuleType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class MapFinsurvContext implements IFinsurvContext {
  public static class NodeName {
    private String MonetaryAmount = "MonetaryAmount";
    private String ImportExport = "ImportExport";
    private String ResidentDot = "Resident.";
    private String NonResidentDot = "NonResident.";

    public String getMonetaryAmount() {
      return MonetaryAmount;
    }

    public void setMonetaryAmount(String monetaryAmount) {
      MonetaryAmount = monetaryAmount;
    }

    public String getImportExport() {
      return ImportExport;
    }

    public void setImportExport(String importExport) {
      ImportExport = importExport;
    }

    public String getResidentDot() {
      return ResidentDot;
    }

    public void setResidentDot(String residentDot) {
      ResidentDot = residentDot;
    }

    public String getNonResidentDot() {
      return NonResidentDot;
    }

    public void setNonResidentDot(String nonResidentDot) {
      NonResidentDot = nonResidentDot;
    }
  }

  private final Pattern dotPattern;
  private final NodeName nodeName;
  private final Map<String, Object> trx;
  private final List<Map<String, Object>> monetaryAmountList;
  private final List<List<Map<String, Object>>> importExportLists;
  private final IFinsurvContext.Undefined undefined = new IFinsurvContext.Undefined();

  public MapFinsurvContext(Map<String, Object> trn) {
    this.dotPattern = Pattern.compile("\\.");
    this.nodeName = new NodeName();
    this.trx = trn;
    List array;
    array = (List)trx.get(nodeName.getMonetaryAmount());
    if (array != null)
      this.monetaryAmountList = (List<Map<String, Object>>)array;
    else
      this.monetaryAmountList = new ArrayList<Map<String, Object>>();
    this.importExportLists = new ArrayList<List<Map<String, Object>>>();
    for (Object maObj : this.monetaryAmountList) {
      if (maObj instanceof Map) {
        Object ieList = ((Map)maObj).get(nodeName.getImportExport());
        this.importExportLists.add((List<Map<String, Object>>)ieList);
      }
    }
  }

  public Object getDataField(Scope scope, Object objLevel, final String field) {
    String[] fields = dotPattern.split(field);
    String fieldName = null;
    for (int i=0; i<fields.length; i++) {
      fieldName = fields[i];
      objLevel = ((Map)objLevel).get(fieldName);
      if (objLevel == null) {
        if (i >= fields.length-1)
          return null;
        else {
          if (scope == Scope.Transaction) {

            if (i < 2 && (field.startsWith(nodeName.getResidentDot()) || field.startsWith(nodeName.getNonResidentDot())))
              return undefined;
          }
          return null;
        }
      }
    }
    return objLevel;
  }

  private Object createFieldPath(Object objBase, Object objLevel, String[] fields, boolean clearPath){
    Object level1Obj = ((Map)objBase).get(fields[0]);
    if (level1Obj == null) {
      Map<String, Object> newJsObj = new HashMap<String, Object>();
      ((Map)objBase).put(fields[0], newJsObj);
      level1Obj = newJsObj;
    }
    objLevel = level1Obj;
    for (int j=1; j<fields.length-1; j++) {
      String fieldName = fields[j];
      if (objLevel instanceof Map) {
        Map jsObj = (Map) objLevel;
        if (clearPath) {
          jsObj.clear();
        }
        Map<String, Object> newJsObj = new HashMap<String, Object>();
        jsObj.put(fieldName, newJsObj);
        objLevel = newJsObj;
      }
    }
    return objLevel;
  }

  public void setFieldValue(Scope scope, Object objLevel, final String field, final String value) {
    Object objBase = objLevel;
    String[] fields = dotPattern.split(field);
    String fieldName;
    for (int i=0; i<fields.length-1; i++) {
      fieldName = fields[i];
      objLevel = ((Map)objLevel).get(fieldName);
      if (objLevel == null) {
        if (i >= fields.length-1)
          return;
        else {
          if (scope == Scope.Transaction) {
            if (i < 2 && (field.startsWith(nodeName.getResidentDot()) || field.startsWith(nodeName.getNonResidentDot()))) {
              objLevel = createFieldPath(objBase, objLevel, fields, true);
              /*
              Object level1Obj = ((JSObject)objBase).get(fields[0]);
              if (level1Obj == null) {
                JSObject newJsObj = new JSObject();
                ((JSObject)objBase).put(fields[0], newJsObj);
                level1Obj = newJsObj;
              }
              objLevel = level1Obj;
              for (int j=1; j<fields.length-1; j++) {
                fieldName = fields[j];
                if (objLevel instanceof JSObject) {
                  JSObject jsObj = (JSObject) objLevel;
                  jsObj.clear();
                  JSObject newJsObj = new JSObject();
                  jsObj.put(fieldName, newJsObj);
                  objLevel = newJsObj;
                }
              }
              */
              break;
            } else {
              objLevel = createFieldPath(objBase, objLevel, fields, false);
              break;
            }
          } else {
            objLevel = createFieldPath(objBase, objLevel, fields, false);
            break;
          }
          //return;
        }
      }
    }
    fieldName = fields.length > 1 ? fields[fields.length-1] : field;
    ((Map)objLevel).put(fieldName, value);
  }

  @Override
  public IFinsurvContextCache getFinsurvContextCache() {
    return null;
  }

  public boolean hasField(Object obj, final String field) {
    if (obj instanceof Map) {
      return ((Map)obj).get(field) != null;
    }
    return false;
  }

  public Object getTransactionField(final String field) {
    return getDataField(Scope.Transaction, trx, field);
  }

  public int getMoneySize() {
    return monetaryAmountList != null ? monetaryAmountList.size() : 0;
  }

  public Object getMoneyField(final int instance, final String field) {
    if (instance >= 0 && instance < monetaryAmountList.size())
      return getDataField(Scope.Money, monetaryAmountList.get(instance), field);
    else
      return undefined;
  }

  public int getImportExportSize(final int moneyInstance) {
    if (moneyInstance >= 0 && moneyInstance < monetaryAmountList.size()) {
      List list = importExportLists.get(moneyInstance);
      if (list != null)
        return list.size();
    }
    return 0;
  }

  public Object getImportExportField(final int moneyInstance, final int instance, final String field) {
    if (moneyInstance >= 0 && moneyInstance < monetaryAmountList.size()) {
      List list = importExportLists.get(moneyInstance);
      if (list != null)
        if (instance >= 0 && instance < list.size()) {
          return getDataField(Scope.ImportExport, list.get(instance), field);
        }
    }
    return null;
  }

  public void logTransactionEvent(final ResultType type, final String field, final String code, final String msg) {
  }

  public void logMoneyEvent(final ResultType type, final int instance, final String field, final String code, final String msg) {
  }

  public void logImportExportEvent(final ResultType type, final int moneyInstance, final int instance, final String field, final String code, final String msg) {
  }

  private String resolveValueFromList(final List<String> values) {
    String result = null;
    if (values != null && values.size() > 0)
      result = values.get(0);
    return result;
  }
  public void logTransactionDisplayEvent(final DisplayRuleType type, final String field, final List<String> values) {
    if (type == DisplayRuleType.SetValue || type == DisplayRuleType.SetField) {
      setFieldValue(Scope.Transaction, trx, field, resolveValueFromList(values));
    }
  }

  public void logMoneyDisplayEvent(final DisplayRuleType type, int instance, final String field, final List<String> values) {
    if (type == DisplayRuleType.SetValue) {
      if (instance >= 0 && instance < monetaryAmountList.size()) {
        Object money = monetaryAmountList.get(instance);
        setFieldValue(Scope.Money, money, field, resolveValueFromList(values));
      }
    }
  }

  public void logImportExportDisplayEvent(final DisplayRuleType type, int moneyInstance, int instance, final String field, final List<String> values) {
    if (type == DisplayRuleType.SetValue) {
      if (moneyInstance >= 0 && moneyInstance < monetaryAmountList.size()) {
        List list = importExportLists.get(moneyInstance);
        if (list != null) {
          if (instance >= 0 && instance < list.size()) {
            setFieldValue(Scope.ImportExport, list.get(instance), field, resolveValueFromList(values));
          }
        }
      }
    }
  }
}
