package za.co.synthesis.finsurvlocal.utils;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.ICustomValue;

import java.util.Map;

/**
 * Created by jake on 6/6/17.
 */
public class MapCustomValues implements ICustomValue {
  private Map<String, Object> customValues;

  public MapCustomValues(Map<String, Object> customValues) {
    this.customValues = customValues;
  }

  @Override
  public Object get(String field) {
    return customValues.get(field);
  }



  public static MapCustomValues getCustomValues(Map<String, Object> meta) {
    if (meta == null) {
      meta = new JSObject();
    }
    if (!meta.containsKey("DealerType"))
      meta.put("DealerType", "AD");

    return new MapCustomValues(meta);
  }
}



