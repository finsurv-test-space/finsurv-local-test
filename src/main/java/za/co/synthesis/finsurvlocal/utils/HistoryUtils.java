package za.co.synthesis.finsurvlocal.utils;

import za.co.synthesis.finsurvlocal.report.types.JsonConstant;
import za.co.synthesis.finsurvlocal.report.types.ReportData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.finsurvlocal.utils.JsonUtils.jsonStrToMap;

//TODO: Complete (client-friendly) java doc for the code below...
/**
 * A collection of functions used to perform comparative analysis and augmentation of JSON (BOP reports) which can then provide feedback around any changes needed or made to get from one version thereof to another.
 * Created by james on 2017/07/04.
 */
public class HistoryUtils {

    public static enum DiffActionType {
        Added("Added"),
        Removed("Removed"),
        Updated("Updated"),
        Moved("Moved"),
        None("None");
        private String action;
        DiffActionType(String action){
            this.action = action;
        }

        public String toString(){
            return action;
        }
    }

    public static enum ReportDataSection {
        Report("Report"),
        Meta("Meta"),
//        History("History"),
        Audit("Audit"),
        General("General"); //TODO: implement an audit section for each report? ...not used or implemented at present.
        private String section;
        ReportDataSection(String section){
            this.section = section;
        }

        public String toString(){
            return section;
        }
    }

    private static Map<String, Object> jsonTreeToPathMap(Map<String, Object> jsonTree){
        Map<String, Object> pathMap = new HashMap<String, Object>();
        JsonMapper.mapJsonObject(jsonTree, "", pathMap);
        return pathMap;
    }

    private static Map<DiffActionType, List<String>> getJsonDiffMap(Map<String, Object> fromVersion, Map<String, Object> toVersion){
        Map<DiffActionType, List<String>> diffMap = new HashMap<DiffActionType, List<String>>();
        for (Map.Entry<String, Object> entry : fromVersion.entrySet()){
            if (toVersion.containsKey(entry.getKey())) {
                Object toValue = toVersion.get(entry.getKey());
                //check for diff.
                if (entry.getValue()!=null && !entry.getValue().equals(toValue)){
                    if (!diffMap.containsKey(DiffActionType.Updated)){
                        diffMap.put(DiffActionType.Updated, new ArrayList<String>());
                    }
                    diffMap.get(DiffActionType.Updated).add(entry.getKey()+":\""+entry.getValue()+"\"->\""+toValue+"\"");
                }
            } else {
                if (!diffMap.containsKey(DiffActionType.Removed)){
                    diffMap.put(DiffActionType.Removed, new ArrayList<String>());
                }
                diffMap.get(DiffActionType.Removed).add(entry.getKey()+":\""+entry.getValue()+"\"");
            }
        }
        for (Map.Entry<String, Object> entry : toVersion.entrySet()){
            if (!fromVersion.containsKey(entry.getKey())) {
                if (!diffMap.containsKey(DiffActionType.Added)){
                    diffMap.put(DiffActionType.Added, new ArrayList<String>());
                }
                diffMap.get(DiffActionType.Added).add(entry.getKey()+":\""+entry.getValue()+"\"");
            }
        }
        return diffMap;
    }

    private static Map<DiffActionType, List<Object>> getJsonDiffActionMap(Map<String, Object> fromVersion, Map<String, Object> toVersion){
        Map<DiffActionType, List<Object>> diffMap = new HashMap<DiffActionType, List<Object>>();
        for (Map.Entry<String, Object> entry : fromVersion.entrySet()){
            if (toVersion.containsKey(entry.getKey())) {
                Object toValue = toVersion.get(entry.getKey());
                //check for diff.
                if (entry.getValue()!=null && !entry.getValue().equals(toValue)){
                    if (!diffMap.containsKey(DiffActionType.Updated)){
                        diffMap.put(DiffActionType.Updated, new ArrayList<Object>());
                    }
                    Map<String, Object> actionMap = new HashMap<String, Object>();
                    Map <String, Object> valueMap = new HashMap<String, Object>();
                    valueMap.put("From",entry.getValue());
                    valueMap.put("To",toValue);
                    actionMap.put(entry.getKey(), valueMap);
                    diffMap.get(DiffActionType.Updated).add(actionMap);
                }
            } else {
                if (!diffMap.containsKey(DiffActionType.Removed)){
                    diffMap.put(DiffActionType.Removed, new ArrayList<Object>());

                }
                Map<String, Object> actionMap = new HashMap<String, Object>();
                Map <String, Object> valueMap = new HashMap<String, Object>();
                valueMap.put("Value",entry.getValue());
                actionMap.put(entry.getKey(), valueMap);
                diffMap.get(DiffActionType.Removed).add(actionMap);
            }
        }
        for (Map.Entry<String, Object> entry : toVersion.entrySet()){
            if (!fromVersion.containsKey(entry.getKey())) {
                if (!diffMap.containsKey(DiffActionType.Added)){
                    diffMap.put(DiffActionType.Added, new ArrayList<Object>());
                }
                Map<String, Object> actionMap = new HashMap<String, Object>();
                Map <String, Object> valueMap = new HashMap<String, Object>();
                valueMap.put("Value",entry.getValue());
                actionMap.put(entry.getKey(), valueMap);
                diffMap.get(DiffActionType.Added).add(actionMap);
            }
        }
        return diffMap;
    }



    private static void getSmartSequenceDiffActionMap(Map<DiffActionType, List<Object>> diffMap,
                                                      String prefixPath,
                                                      Object fromSequence,
                                                      Object toSequence){
        List<Object> unmatchedFromList = new ArrayList<Object>();
        List<Object> unmatchedToList = new ArrayList<Object>();
        List<Object> markForRemoval = new ArrayList<Object>();
        List<Object> matchedFromList = new ArrayList<Object>();
        List<Object> matchedToList = new ArrayList<Object>();
        if ((fromSequence instanceof List) && (toSequence instanceof List)){
            unmatchedFromList.addAll((List)fromSequence);
            unmatchedToList.addAll((List)toSequence);
            //do Unique ID matches across the lists...
            for (Object fromObj : unmatchedFromList){
                if (fromObj instanceof Map) {
                    String sourceID = (String) ((Map) fromObj).get(JsonConstant.UniqueObjectInstanceID);
                    //we have a uniqueID to compare with...
                    if (sourceID != null && !sourceID.isEmpty()) {
                        for (Object toObj : unmatchedToList) {
                            if (toObj instanceof Map) {
                                String targetID = (String) ((Map) toObj).get(JsonConstant.UniqueObjectInstanceID);
                                if (sourceID.equals(targetID)) {
                                    matchedFromList.add(fromObj);
                                    markForRemoval.add(fromObj);
                                    matchedToList.add(toObj);
                                    markForRemoval.add(toObj);
                                }
                            }
                        }
                    }
                } else if (fromObj instanceof List) {
                    //  *****  IGNORE!! *****  //
                    //This implies lists of lists without context. NOT handling this as there is no current, known need for it.
                    // ...Do we need a smarter way of determining if a XD array has been altered and how...   Matrix operations?
                    // ...for now, just ignore.  we can't handle this complexity and don't expect it to occur in the normal data.
                } else {
                    //this is a list of objects (Assumedly strings or some such)
                    for (Object toObj : unmatchedToList) {
                        if (!((toObj instanceof List) || (toObj instanceof Map))) {
                            if (fromObj.equals(toObj)) {
                                matchedFromList.add(fromObj);
                                markForRemoval.add(fromObj);
                                matchedToList.add(toObj);
                                markForRemoval.add(toObj);
                            }
                        }
                    }
                }
            }

            for (Object rem : markForRemoval){
                unmatchedFromList.remove(rem);
                unmatchedToList.remove(rem);
            }

            //TODO: FUZZY-MATCHING OF UNMATCHED JSON BRANCHES (IMPORT EXPORTS AND MONETARY AMOUNTS)...
            //for any remaining items, try performing a distance calculation to determine similarity and hence matches...



            //perform the diffs for the items that are known to be the same item on either side...
            for (int cnt=0; cnt< matchedFromList.size(); cnt++) {
                Map fromObj = (Map<String, Object>)matchedFromList.get(cnt);
                Map toObj = (Map<String, Object>)matchedToList.get(cnt);
                int oldIndex = ((List) fromSequence).indexOf(fromObj) + 1;
                int currentIndex = ((List) toSequence).indexOf(toObj) + 1;
                String currentPath = (prefixPath!=null?prefixPath:"") + "["+oldIndex+"]";

                //look for sequence changes and add to the diff...
                if (oldIndex != currentIndex) {
                    if (!diffMap.containsKey(DiffActionType.Moved)) {
                        diffMap.put(DiffActionType.Moved, new ArrayList<Object>());
                    }
                    Map<String, Object> actionMap = new HashMap<String, Object>();
                    Map<String, Object> valueMap = new HashMap<String, Object>();
                    valueMap.put("From", "[" + oldIndex + "]");
                    valueMap.put("To", "[" + currentIndex + "]");
                    actionMap.put(currentPath, valueMap);
                    diffMap.get(DiffActionType.Moved).add(actionMap);
                }

//                currentPath = (prefixPath!=null?prefixPath+".":"") + "["+(oldIndex == currentIndex?currentIndex:oldIndex+"->"+currentIndex)+"]";
                currentPath = (prefixPath!=null?prefixPath:"") + "["+currentIndex+"]";
                if (fromObj instanceof Map && toObj instanceof Map)
                    getSmartJsonDiffActionMap(diffMap, currentPath, fromObj, toObj);
            }


            //Any remaining unmatched "FROM" items will be considered as "removed"...
            for (int cnt=0; cnt< unmatchedFromList.size(); cnt++) {
                Map fromObj = (Map<String, Object>)unmatchedFromList.get(cnt);
                int oldIndex = ((List) fromSequence).indexOf(fromObj) + 1;
                String currentPath = (prefixPath!=null?prefixPath:"") + "["+oldIndex+"]";

                if (!diffMap.containsKey(DiffActionType.Removed)) {
                    diffMap.put(DiffActionType.Removed, new ArrayList<Object>());
                }
                Map<String, Object> actionMap = new HashMap<String, Object>();
                Map<String, Object> valueMap = new HashMap<String, Object>();
                valueMap.put("From", "["+oldIndex+"]");
                actionMap.put(currentPath, valueMap);
                diffMap.get(DiffActionType.Removed).add(actionMap);
            }

            //Any remaining unmatched "TO" items will be considered as "added"...
            for (int cnt=0; cnt< unmatchedToList.size(); cnt++) {
                Map toObj = (Map<String, Object>)unmatchedToList.get(cnt);
                int currentIndex = ((List) toSequence).indexOf(toObj) + 1;
                String currentPath = (prefixPath!=null?prefixPath:"") + "["+currentIndex+"]";

                if (!diffMap.containsKey(DiffActionType.Added)) {
                    diffMap.put(DiffActionType.Added, new ArrayList<Object>());
                }
                Map<String, Object> actionMap = new HashMap<String, Object>();
                Map<String, Object> valueMap = new HashMap<String, Object>();
                valueMap.put("To", "["+currentIndex+"]");
                actionMap.put(currentPath, valueMap);
                diffMap.get(DiffActionType.Added).add(actionMap);
            }
        }
    }


    /**
     * This function expects two Json tree maps as parameters.
     * the diff will happen branch by branch against each property contained therein.
     * Map and List objects are considered as new branches and will spawn a recursive diff.
     * @param fromVersion
     * @param toVersion
     * @return
     */
    private static Map<DiffActionType, List<Object>> getSmartJsonDiffActionMap(Map<String, Object> fromVersion,
                                                                               Map<String, Object> toVersion) {
        Map<DiffActionType, List<Object>> diffMap = new HashMap<DiffActionType, List<Object>>();
        getSmartJsonDiffActionMap(diffMap, null, fromVersion, toVersion);
        return diffMap;
    }

    private static void getSmartJsonDiffActionMap(Map<DiffActionType, List<Object>> diffMap,
                                                  String prefixPath,
                                                  Map<String, Object> fromVersion,
                                                  Map<String, Object> toVersion){
        if (diffMap != null) {
            for (Map.Entry<String, Object> entry : fromVersion.entrySet()) {
                //handle the monetary amount and import-export sequences differently from the rest of the data...
//                if (entry.getKey().equalsIgnoreCase(JsonConstant.MonetaryAmount) ||
//                        entry.getKey().equalsIgnoreCase(JsonConstant.ImportExport)) {
                if (entry.getValue() instanceof List){
                    getSmartSequenceDiffActionMap(diffMap, (prefixPath!=null&&!prefixPath.isEmpty()?prefixPath+".":"") + entry.getKey(), entry.getValue(), toVersion.get(entry.getKey()));
                } else {
                    String newPath = (prefixPath!=null&&!prefixPath.isEmpty()?prefixPath+".":"") + entry.getKey();
                    if (toVersion.containsKey(entry.getKey())) {
                        if (entry.getValue() instanceof Map) {
                            //diff the maps
                            getSmartJsonDiffActionMap(diffMap,
                                    newPath,
                                    (Map)entry.getValue(),
                                    (Map)toVersion.get(entry.getKey()));
//                        } else if (entry.getValue() instanceof List) {
//                            //hmmm....   diff the lists?   ...is this any different from the money and import exports???
//                            getSmartSequenceDiffActionMap(diffMap,
//                                    newPath,
//                                    entry.getValue(),
//                                    toVersion.get(entry.getKey()));
                        } else {
                            Object toValue = toVersion.get(entry.getKey());
                            //check for diff.
                            if (entry.getValue() != null && !entry.getValue().equals(toValue)) {
                                if (!diffMap.containsKey(DiffActionType.Updated)) {
                                    diffMap.put(DiffActionType.Updated, new ArrayList<Object>());
                                }
                                Map<String, Object> actionMap = new HashMap<String, Object>();
                                Map<String, Object> valueMap = new HashMap<String, Object>();
                                valueMap.put("From", entry.getValue());
                                valueMap.put("To", toValue);
                                actionMap.put(newPath, valueMap);
                                diffMap.get(DiffActionType.Updated).add(actionMap);
                            }
                        }
                    } else {
                        if (!diffMap.containsKey(DiffActionType.Removed)) {
                            diffMap.put(DiffActionType.Removed, new ArrayList<Object>());
                        }
                        Map<String, Object> actionMap = new HashMap<String, Object>();
                        Map<String, Object> valueMap = new HashMap<String, Object>();
                        valueMap.put("Value", entry.getValue());
                        actionMap.put(newPath, valueMap);
                        diffMap.get(DiffActionType.Removed).add(actionMap);
                    }
                }
            }
            for (Map.Entry<String, Object> entry : toVersion.entrySet()) {
                if (!(entry.getKey().equalsIgnoreCase(JsonConstant.MonetaryAmount) ||
                        entry.getKey().equalsIgnoreCase(JsonConstant.ImportExport))) {
                    String newPath = (prefixPath!=null?prefixPath+".":"") + entry.getKey();
                    if (!fromVersion.containsKey(entry.getKey())) {
                        if (!diffMap.containsKey(DiffActionType.Added)) {
                            diffMap.put(DiffActionType.Added, new ArrayList<Object>());
                        }
                        Map<String, Object> actionMap = new HashMap<String, Object>();
                        Map<String, Object> valueMap = new HashMap<String, Object>();
                        valueMap.put("Value", entry.getValue());
                        actionMap.put(entry.getKey(), valueMap);
                        diffMap.get(DiffActionType.Added).add(actionMap);
                    }
                }
            }
        }
    }

//    public static Map<DiffActionType, List<String>> getJsonDiffMap(JSObject fromJSObject, JSObject toJSObject){
//        Map<String, String> fromVersion = new HashMap<>();
//        Map<String, String> toVersion = new HashMap<>();
//        JsonMapper mapper = new JsonMapper();
//        mapper.mapJsonSrcToTarget(fromJSObject, fromVersion);
//        mapper.mapJsonSrcToTarget(toJSObject, toVersion);
//        return getJsonDiffMap(fromVersion, toVersion);
//    }

    public static Map<ReportDataSection, Map<DiffActionType, List<String>>> getJsonDifferences(Map<String, Object> objectMap1, Map<String, Object> objectMap2) {
        ReportData fromReportData = new ReportData();
        ReportData toReportData = new ReportData();
        try {
            fromReportData.getMeta().putAll((Map) objectMap1.get("Meta"));
            fromReportData.getReport().putAll((Map) objectMap1.get("Report"));
            toReportData.getMeta().putAll((Map) objectMap2.get("Meta"));
            toReportData.getReport().putAll((Map) objectMap2.get("Report"));
        } catch (Exception error){
            //handle this better
        }
        return getJsonDiffMap(fromReportData, toReportData);
    }

    public static Map<ReportDataSection, Map<DiffActionType, List<String>>> getJsonDifferences(String json1, String json2) throws Exception {
        Map<String, Object> jsonMap1 = jsonStrToMap(json1);
        Map<String, Object> jsonMap2 = jsonStrToMap(json2);
        return getJsonDifferences(jsonMap1, jsonMap2);
    }

    public static Map<ReportDataSection, Map<DiffActionType, List<String>>> getJsonDiffMap(ReportData prevData, ReportData nextData){
        Map<ReportDataSection, Map<DiffActionType, List<String>>> diffMap = new HashMap<ReportDataSection, Map<DiffActionType, List<String>>>();
        //get diff of Meta info:
        diffMap.put(ReportDataSection.Meta,
                getJsonDiffMap(
                        jsonTreeToPathMap(prevData.getMeta()),
                        jsonTreeToPathMap(nextData.getMeta())
                )
        );
        //get diff of Report data:
        diffMap.put(ReportDataSection.Report,
                getJsonDiffMap(
                        jsonTreeToPathMap(prevData.getReport()),
                        jsonTreeToPathMap(nextData.getReport())
                )
        );
        return diffMap;
    }

    public static Map<ReportDataSection, Map<DiffActionType, List<Object>>> getJsonDiffActionMap(ReportData prevData, ReportData nextData){
        Map<ReportDataSection, Map<DiffActionType, List<Object>>> diffMap = new HashMap<ReportDataSection, Map<DiffActionType, List<Object>>>();
        //get diff of Meta info:
        diffMap.put(ReportDataSection.Meta,
                getJsonDiffActionMap(
                        jsonTreeToPathMap(prevData.getMeta()),
                        jsonTreeToPathMap(nextData.getMeta())
                )
        );
        //get diff of Report data:
        diffMap.put(ReportDataSection.Report,
                getJsonDiffActionMap(
                        jsonTreeToPathMap(prevData.getReport()),
                        jsonTreeToPathMap(nextData.getReport())
                )
        );
        return diffMap;
    }

    public static String getJsonDiffText(ReportData fromReportData, ReportData toReportData) {
        return getDiffMapText(getJsonDiffMap(fromReportData, toReportData));
    }

    public static String getJsonDiffText(String json1, String json2) {
        ReportData fromReportData = new ReportData();
        ReportData toReportData = new ReportData();
        try {
            Map<String, Object> jsonMap1 = jsonStrToMap(json1);
            fromReportData.getMeta().putAll((Map) jsonMap1.get("Meta"));
            fromReportData.getReport().putAll((Map) jsonMap1.get("Report"));
            Map<String, Object> jsonMap2 = jsonStrToMap(json2);
            toReportData.getMeta().putAll((Map) jsonMap2.get("Meta"));
            toReportData.getReport().putAll((Map) jsonMap2.get("Report"));
        } catch (Exception error){
            //handle this better
        }
        return getDiffMapText(getJsonDiffMap(fromReportData, toReportData));
    }

    public static String getJsonDiffText(Map<String, Object> json1, Map<String, Object> json2) {
        ReportData fromReportData = new ReportData();
        ReportData toReportData = new ReportData();
        try {
            fromReportData.getMeta().putAll((Map) json1.get("Meta"));
            fromReportData.getReport().putAll((Map) json1.get("Report"));
            toReportData.getMeta().putAll((Map) json2.get("Meta"));
            toReportData.getReport().putAll((Map) json2.get("Report"));
        } catch (Exception error){
            //handle this better
        }
        return getDiffMapText(getJsonDiffMap(fromReportData, toReportData));
    }



    public static String getDiffMapText(Map<ReportDataSection, Map<DiffActionType, List<String>>> diffMap){
        String diffText = "";
        for (ReportDataSection section : ReportDataSection.values()){
            Map<DiffActionType, List<String>> sectionDiff = diffMap.get(section);
            if (sectionDiff != null && sectionDiff.size()>0){
                diffText+=(diffText.length()>0?"\r\n":"")+section.name()+" Changes:\r\n";
                for (Map.Entry<DiffActionType, List<String>> entry : sectionDiff.entrySet()) {
                    diffText+= "\t"+entry.getKey().name()+":\r\n";
                    for (String diff : entry.getValue()){
                        diffText+= "\t- "+diff+"\r\n";
                    }
                }
            }
        }
        return diffText.length()>0?diffText:"No Content Changes";
    }

    //TODO: create an intelligent dif which will ID mismatched/misaligned items and then attempt to look for more specific changes - such as deletes, moves and minor edits.
    public static Map<ReportDataSection, Map<DiffActionType, List<Object>>> getSmartJsonDiffActionMap(ReportData prevData, ReportData nextData){
        Map<ReportDataSection, Map<DiffActionType, List<Object>>> diffMap = new HashMap<ReportDataSection, Map<DiffActionType, List<Object>>>();
        //get diff of Meta info:
        diffMap.put(ReportDataSection.Meta,
                getJsonDiffActionMap(
                        jsonTreeToPathMap(prevData.getMeta()),
                        jsonTreeToPathMap(nextData.getMeta())
                )
        );
        //get diff of Report data:
        diffMap.put(ReportDataSection.Report,
                getSmartJsonDiffActionMap(
                        prevData.getReport(),
                        nextData.getReport()
                )
        );
        return diffMap;
    }
    public static Map<ReportDataSection, Map<DiffActionType, List<Object>>> getSmartJsonDiffActionMap(String json1, String json2) throws Exception {
        Map<ReportDataSection, Map<DiffActionType, List<Object>>> diffMap = new HashMap<ReportDataSection, Map<DiffActionType, List<Object>>>();
        diffMap.put(ReportDataSection.General,
                getSmartJsonDiffActionMap(
                        jsonStrToMap(json1),
                        jsonStrToMap(json2)
                )
        );
        return diffMap;
    }

}
