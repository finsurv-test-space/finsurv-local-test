package za.co.synthesis.finsurvlocal.utils;

import za.co.synthesis.rule.support.Util;


//TODO: Complete (client-friendly) java doc for the code below...
/**
 * This class provides Finsurv-specific augmentation functions to the Freemarker DSL.
 */
public class FreemarkerLogic {
  public static boolean isNumeric(final String s) {
    return Util.isNumeric(s);
  }

  public static boolean isValidRSAID(final String id) {
    return Util.validRSAID(id);
  }

  public static boolean isValidVATNumber(final String str) {
    return Util.isValidVATNumber(str);
  }

  public static boolean isValidZATaxNumber(final String str) {
    return Util.isValidZATaxNumber(str);
  }

  public static boolean isValidCCN(final String str) {
    return Util.isValidCCN(str);
  }

  public static boolean isValidPostalCode(final String str) {
    return Util.isValidPostalCode(str);
  }

  public static boolean isValidUCR(final String str) {
    return Util.isValidUCR(str);
  }

  public static boolean isValidEmail(final String str) {
    return Util.isValidEmail(str);
  }

  public static boolean isValidECI(final String str, final String cardType) {
    return Util.isValidECI(str, cardType);
  }
}
