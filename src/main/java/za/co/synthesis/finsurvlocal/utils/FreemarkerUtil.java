package za.co.synthesis.finsurvlocal.utils;

import freemarker.core.Environment;
import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.xml.sax.InputSource;
import za.co.synthesis.javascript.JSStructureParser;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Pattern;

//TODO: Complete (client-friendly) java doc for the code below...
/**
 * TODO: add description for class
 * Created by jake on 9/15/17.
 */
public class FreemarkerUtil {
  public static String encodeUrl(String str) {
    try {
      return URLEncoder.encode(str, "UTF-8");
    } catch (UnsupportedEncodingException e) {
    }
    return str;
  }

  public static String decodeUrl(String encoded) {
    try {
      return URLDecoder.decode(encoded, "UTF-8");
    } catch (UnsupportedEncodingException e) {
    }
    return encoded;
  }

  public static String encodeBase64(String str) {
    byte[] encodedBytes = Base64.getEncoder().encode(str.getBytes());
    return new String(encodedBytes);
  }

  public static String escapeJava(String str) {
    return StringEscapeUtils.escapeJava(str);
  }


  public static String escapeSql(String str) {
    //return StringEscapeUtils.escapeSql(str);  //This was deprecated (in commons-lang3 already) and subsequently removed from commons-text.
    return str!=null?escapeJavaScript(str):"";
  }

  public static String escapeSql(Number num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeSql(Integer num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeSql(Float num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeSql(Long num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeJavaScript(String str) {
    return StringEscapeUtils.escapeEcmaScript(str);
  }

  public static String escapeCsv(String str) {
    return StringEscapeUtils.escapeCsv(str);
  }

  public static String escapeXml1_0(String str) {
    return StringEscapeUtils.escapeXml10(str);
  }

  public static String escapeXml1_1(String str) {
    return StringEscapeUtils.escapeXml11(str);
  }

  public static String unescapeJava(String str) {
    return StringEscapeUtils.unescapeJava(str);
  }

  public static String unescapeJavaScript(String str) {
    return StringEscapeUtils.unescapeEcmaScript(str);
  }

  public static String unescapeCsv(String str) {
    return StringEscapeUtils.unescapeCsv(str);
  }

  public static String unescapeXml(String str) {
    return StringEscapeUtils.unescapeXml(str);
  }

  public static Object jsonDeserialize(final String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      return parser.parse();
    }
    return null;
  }

  public static List<Map<String, Object>> sqlDeserialize(final List<Map<String, Object>> sqlResponse) throws Exception {
    return sqlResponse;
  }

  public static Map<String, Object> sqlFirstRowDeserialize(final List<Map<String, Object>> sqlResponse) throws Exception {
    if (sqlResponse != null && sqlResponse.size() > 0)
      return sqlResponse.get(0);
    return null;
  }

  public static NodeModel xmlDeserialize(final String str) throws Exception {
    if (str != null) {
      InputSource inputSource = new InputSource(new StringReader( str ));
      return NodeModel.parse(inputSource, true, true);
    }
    return null;
  }


  private final static Pattern dotPattern = Pattern.compile("\\.");

  /**
  Takes a <i>split</i>  (dot-notation) string[] and creates a Map-branch to assign the value provided.
   IE: ("x.y.z" -> "ABC") => ({"x","y","z"} -> "ABC")
   */
  private static void addValueToMap(String[] fields, String value, Map<String, Object> map) {
    if (fields.length == 1) {
      String key = fields[0];
      if (!map.containsKey(key)) {
        map.put(key, value);
      }
    }
    else
    if (fields.length > 1) {
      String key = fields[0];
      Map<String, Object> subMap = null;
      if (!map.containsKey(key)) {
        subMap = new HashMap<String, Object>();
        map.put(key, subMap);
      }
      else {
        Object obj = map.get(key);
        if (obj instanceof Map) {
          subMap = (Map)obj;
        }
      }
      if (subMap != null) {
        addValueToMap(Arrays.copyOfRange(fields, 1, fields.length), value, subMap);
      }
    }
  }

  /*
    TODO: this function currently assumes and caters for a single BOP report per transaction.
    It may very well be the case that both sides of the BOP transaction are reportable and a separate BOP report
    would then need to be generated for each of the DR and CR sides.  Case in point: CFC -> CFC
    To cater for this, the return type for the function should probably be a map<String, String> where the key is the
    (determined/augmented) transaction reference, and the value is the populated BOP data template.
    In a case where the transaction (with reference xyz123) is reportable for both DR and CR, the map should then have
    two entries - namely: xyz123_DR and xyz123_CR
   */
  public static String composeTemplateFile(String templateDirectory, String templateName, Map<String, Object> parameterValues, Logger log) throws Exception {
      Version version = new Version(2, 3, 30);
      //Configuration config = new Configuration(version);
// Create your Configuration instance, and specify if up to what FreeMarker
// version (here 2.3.29) do you want to apply the fixes that are not 100%
// backward-compatible. See the Configuration JavaDoc for details.
      Configuration config = new Configuration(Configuration.VERSION_2_3_30);

// Specify the source where the template files come from. Here I set a
// plain directory for it, but non-file-system sources are possible too:
      config.setDirectoryForTemplateLoading(new File(templateDirectory));

// From here we will set the settings recommended for new projects. These
// aren't the defaults for backward compatibilty.

// Set the preferred charset template files are stored in. UTF-8 is
// a good choice in most applications:
      config.setDefaultEncoding("UTF-8");

// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
      config.setLogTemplateExceptions(false);

// Wrap unchecked exceptions thrown during template processing into TemplateException-s:
      config.setWrapUncheckedExceptions(true);

// Do not fall back to higher scopes when reading a null loop variable:
      config.setFallbackOnNullLoopVariable(false);

      Map<String, Object> context = new HashMap<String, Object>();
      context.putAll(parameterValues);
      context.put("logic", new FreemarkerLogic());
      context.put("util", new FreemarkerUtil());
      try {
        //Template t = new Template(name, new StringReader(str), config);
        Template t = config.getTemplate(templateName);
        Writer out = new StringWriter();
        Environment env = t.createProcessingEnvironment(context, out);
        env.process();
        return out.toString();

      } catch (TemplateException  e) {
        log.error("Cannot compose '" + templateName + "'", e);
        throw new Exception("Cannot compose '" + templateName + "'", e);
      } catch (IOException e) {
        log.error("Cannot compose '" + templateName + "'", e);
        throw new Exception("Cannot compose '" + templateName + "'", e);
      }
  }


  public static String composeTemplateString(String templateStr, Map<String, Object> parameterValues, Logger log) throws Exception {
    Version version = new Version(2, 3, 30);
    //Configuration config = new Configuration(version);
// Create your Configuration instance, and specify if up to what FreeMarker
// version (here 2.3.29) do you want to apply the fixes that are not 100%
// backward-compatible. See the Configuration JavaDoc for details.
    Configuration config = new Configuration(Configuration.VERSION_2_3_30);

// Specify the source where the template files come from. Here I set a
// plain directory for it, but non-file-system sources are possible too:
    //config.setDirectoryForTemplateLoading(new File(templateDirectory));

// From here we will set the settings recommended for new projects. These
// aren't the defaults for backward compatibilty.

// Set the preferred charset template files are stored in. UTF-8 is
// a good choice in most applications:
    config.setDefaultEncoding("UTF-8");

// Don't log exceptions inside FreeMarker that it will throw at you anyway:
    config.setLogTemplateExceptions(false);

// Wrap unchecked exceptions thrown during template processing into TemplateException-s:
    config.setWrapUncheckedExceptions(true);

// Do not fall back to higher scopes when reading a null loop variable:
    config.setFallbackOnNullLoopVariable(false);

    Map<String, Object> context = new HashMap<String, Object>();
    context.putAll(parameterValues);
    context.put("logic", new FreemarkerLogic());
    context.put("util", new FreemarkerUtil());
    try {
      Template t = new Template("StringTemplate", new StringReader(templateStr), config);
      Writer out = new StringWriter();
      Environment env = t.createProcessingEnvironment(context, out);
      env.process();
      return out.toString();

    } catch (TemplateException  e) {
      log.error("Cannot compose the provided String Template: \n" + templateStr + "\n", e);
      throw new Exception("Cannot compose the provided String Template", e);
    } catch (IOException e) {
      log.error("Cannot compose the provided String Template: \n" + templateStr + "\n", e);
      throw new Exception("Cannot compose the provided String Template", e);
    }
  }


}
