package za.co.synthesis.finsurvlocal.utils;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.finsurvlocal.ConnectionResponse;
import za.co.synthesis.rule.core.CustomValidateResult;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Map;

import static za.co.synthesis.finsurvlocal.utils.FreemarkerUtil.composeTemplateString;

//TODO: Complete (client-friendly) java doc for the code below...
public class ConnectionUtil {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);

    public static final String urlTemplateKey = "urlTemplate";
    public static final String httpRequestMethodKey = "httpRequestMethod";
    public static final String headerTemplatesKey = "headerTemplates";
    public static final String payloadTemplateKey = "payloadTemplate";

    public static final String sslContextKey = "sslContext";

    public static final String keyStoreTemplateKey = "keyStoreTemplate";
    public static final String keyStoreTypeKey = "keyStoreType";
    public static final String keyStorePathKey = "keyStorePath";
    public static final String keyStorePassKey = "keyStorePass";

    public static final String trustStoreTemplateKey = "trustStoreTemplate";
    public static final String trustStoreTypeKey = "trustStoreType";
    public static final String trustStorePathKey = "trustStorePath";
    public static final String trustStorePassKey = "trustStorePass";

    public static final String defaultSSLContext = "TLS";
    public static final String allowedHttpRequestMethodsRegex = "^(?i)(:?(:?GET)|(:?PUT)|(:?POST)|(:?DELETE)|(:?OPTIONS))$";



    public static String getHttpConnectionBody(HttpURLConnection connection) throws Exception{
        if (connection instanceof  HttpURLConnection) {
            int responseCode = connection.getResponseCode();
//            System.out.println("GET Response Code :: " + responseCode);
            CustomValidateResult result;
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine+"\n");
                }
                in.close();

                // print result
                String responseStr = response.toString();
//                System.out.println(responseStr);
                return responseStr;
            } else {
                throw new Exception(connection.getRequestMethod()+" request failed (HTTP Response Code: " + responseCode + ")");
            }
        }
        return null;
    }

    public static String getHttpsConnectionBody(HttpsURLConnection connection) throws Exception{
        if (connection instanceof  HttpURLConnection) {
            int responseCode = connection.getResponseCode();
//            System.out.println("GET Response Code :: " + responseCode);
            CustomValidateResult result;
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                String responseStr = response.toString();
//                System.out.println(responseStr);
                return responseStr;
            } else {
                throw new Exception(connection.getRequestMethod()+" request failed (HTTP Response Code: " + responseCode + ")");
            }
        }
        return null;
    }

    public static HttpsURLConnection createConnectionFromTemplates(String connectionConfigTemplates, String templateToUse, Map<String, Object> params) throws Exception {
            Map<String, Object> mappedConfig = (JsonUtils.jsonStrToMap(connectionConfigTemplates));
            return createConnectionFromTemplates(mappedConfig, templateToUse, params);
    }

    public static HttpsURLConnection createConnectionFromTemplates(Map<String, Object> connectionConfigTemplates, String templateToUse, Map<String, Object> params) throws Exception {
        assert(connectionConfigTemplates != null);
        assert(connectionConfigTemplates.containsKey(templateToUse));
        assert(connectionConfigTemplates.get(templateToUse) instanceof Map);
        return createConnectionFromTemplate((Map<String, Object>) connectionConfigTemplates.get(templateToUse), params);
    }

    public static HttpsURLConnection createConnectionFromTemplate(Map<String, Object> connectionConfig, Map<String, Object> params) throws Exception {
        assert(connectionConfig != null);
        assert(connectionConfig.containsKey(urlTemplateKey));
        assert(connectionConfig.get(urlTemplateKey) instanceof String);
        assert(connectionConfig.containsKey(httpRequestMethodKey));
        assert(connectionConfig.get(httpRequestMethodKey) instanceof String);
        // Full registry of HTTP Methods: https://webconcepts.info/concepts/http-method/
        assert(((String)connectionConfig.get(httpRequestMethodKey)).matches(allowedHttpRequestMethodsRegex));

        String urlTemplate = null;
        String httpRequestMethod = null;
        String sslContext = null;
        String payloadTemplate = null;
        Map<String, Object> keyStoreTemplate = null;
        Map<String, Object> trustStoreTemplate = null;
        Map<String, String> headerTemplates = null;

        try {
            //if (connectionConfig.containsKey(urlTemplateKey) && connectionConfig.get(urlTemplateKey) instanceof String) {
            // ***** ALREADY ASSERTED ABOVE *****
                urlTemplate = (String) connectionConfig.get(urlTemplateKey);
            //}
            //if (connectionConfig.containsKey(httpRequestMethodKey) && connectionConfig.get(httpRequestMethodKey) instanceof String) {
            // ***** ALREADY ASSERTED ABOVE *****
                httpRequestMethod = (String) connectionConfig.get(httpRequestMethodKey);
            //}
            if (connectionConfig.containsKey(headerTemplatesKey) && connectionConfig.get(headerTemplatesKey) instanceof Map) {
                headerTemplates = (Map<String, String>) connectionConfig.get(headerTemplatesKey);
            }
            if (connectionConfig.containsKey(payloadTemplateKey) && connectionConfig.get(payloadTemplateKey) instanceof String) {
                payloadTemplate = (String) connectionConfig.get(payloadTemplateKey);
            }
            if (connectionConfig.containsKey(sslContextKey) && connectionConfig.get(sslContextKey) instanceof String) {
                sslContext = (String) connectionConfig.get(sslContextKey);
            }
            if (connectionConfig.containsKey(keyStoreTemplateKey) && connectionConfig.get(keyStoreTemplateKey) instanceof Map) {
                keyStoreTemplate = (Map<String, Object>) connectionConfig.get(keyStoreTemplateKey);
            }
            if (connectionConfig.containsKey(trustStoreTemplateKey) && connectionConfig.get(trustStoreTemplateKey) instanceof Map) {
                trustStoreTemplate = (Map<String, Object>) connectionConfig.get(trustStoreTemplateKey);
            }
            return getHttpsConnection(httpRequestMethod,urlTemplate, headerTemplates, payloadTemplate, params, sslContext, keyStoreTemplate, trustStoreTemplate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("nope");
        return null;
    }

    public static HttpsURLConnection createHttpGet(String urlTemplate, Map<String, String> headerTemplates, Map<String, Object> params) throws Exception {
        return getHttpsConnection("GET", urlTemplate, headerTemplates, null, params, null, null,null);
    }

    public static HttpsURLConnection getHttpsConnection(String httpRequestMethod, String urlTemplate, Map<String, String> headerTemplates, String payloadTemplate, Map<String, Object> params) throws Exception {
        return getHttpsConnection(httpRequestMethod, urlTemplate, headerTemplates, payloadTemplate, params,null, null, null);
    }

    public static HttpsURLConnection getHttpsConnection(String httpRequestMethod,
                                                      String urlTemplate,
                                                        Map<String, String>  headerTemplates,
                                                        String payloadTemplate,
                                                      Map<String, Object> params,
                                                      String sslContextType,
                                                      Map<String, Object> keystoreTemplates,
                                                      Map<String, Object> truststoreTemplates) throws Exception {
        assert (httpRequestMethod != null);
        assert (httpRequestMethod.matches(allowedHttpRequestMethodsRegex));
        assert (urlTemplate != null);
        try {
            KeyStore keystore = null;
            KeyStore truststore = null;
            if (keystoreTemplates != null)
                try {
                    keystore = KeyStore.getInstance((String) keystoreTemplates.get(keyStoreTypeKey));
                    FileInputStream identityKeyStoreFile = new FileInputStream(new File((String) keystoreTemplates.get(keyStorePathKey)));
                    keystore.load(identityKeyStoreFile, ((String) keystoreTemplates.get(keyStorePassKey)).toCharArray());
                } catch (Exception err) {
                    //throw something?
                }
            if (truststoreTemplates != null)
                try {
                    truststore = KeyStore.getInstance((String) truststoreTemplates.get(trustStoreTypeKey));
                    FileInputStream trustKeyStoreFile = new FileInputStream(new File((String) truststoreTemplates.get(trustStorePathKey)));
                    truststore.load(trustKeyStoreFile, ((String) truststoreTemplates.get(trustStorePassKey)).toCharArray());
                } catch (Exception err) {
                    //throw something?
                }

            KeyManagerFactory kmf = null;
            KeyManager[] kms = null;
            if (keystore != null) {
                kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                kmf.init(keystore, ((String) keystoreTemplates.get(keyStorePassKey)).toCharArray());
                kms = kmf.getKeyManagers();
            }

            TrustManagerFactory tmf = null;
            TrustManager[] tms = null;
            if (truststore != null) {
                tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(truststore);
                tms = tmf.getTrustManagers();
            }

            SSLContext sslContext = null;
            if (kms != null || tms != null) {
                if (sslContextType != null) {
                    sslContext = SSLContext.getInstance(sslContextType);
                } else {
                    sslContext = SSLContext.getInstance(defaultSSLContext);
                }
                sslContext.init(kms, tms, new SecureRandom());


            }

            if (params.size() > 0) {
                urlTemplate = composeTemplateString(urlTemplate, params, logger);
            }
            URL url = new URL(urlTemplate);
            HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
            if(sslContext != null) {
                urlConn.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            }
            urlConn.setRequestMethod(httpRequestMethod);
            if (headerTemplates.size() > 0) {
                urlConn = CommonHelperFunctions.addHeadersToHttpConnection(urlConn, headerTemplates);
            }
            if (payloadTemplate != null){
                OutputStream out = urlConn.getOutputStream();
                out.write(payloadTemplate.getBytes());
                out.flush();
            }
            return urlConn;
        } catch (Exception ex) {
            System.out.println("Error setting up HTTP connection");
        }

        throw new NotImplementedException("Error setting up HTTP connection");
    }

    public static HttpURLConnection getHttpConnection(String url, String requestMethod) throws Exception{
        HttpURLConnection connection = null;
        try {
            URL reportDataStoreURL = new URL(url);
//            System.out.println("**Calling URL: "+reportDataStoreURL+"\n\n");
            connection = (HttpURLConnection) reportDataStoreURL.openConnection();
            connection.setRequestMethod(requestMethod);
            return connection;
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
        }
        return null;
    }


    public static ConnectionResponse getHttpConnectionResponse(String channelName, HttpURLConnection connection) throws Exception{
        ConnectionResponse response = new ConnectionResponse(channelName);
        response.setResponseCode(connection.getResponseCode());
        response.setResponseHeaders(connection.getHeaderFields());
        String httpContent = ConnectionUtil.getHttpConnectionBody(connection);
        response.setResponseBody(httpContent);
        return response;
    }

    public static ConnectionResponse getHttpsConnectionResponse(String channelName, HttpsURLConnection connection) throws Exception{
        ConnectionResponse response = new ConnectionResponse(channelName);
        response.setResponseCode(connection.getResponseCode());
        response.setResponseHeaders(connection.getHeaderFields());
        String httpContent = ConnectionUtil.getHttpsConnectionBody(connection);
        response.setResponseBody(httpContent);
        return response;
    }
}
