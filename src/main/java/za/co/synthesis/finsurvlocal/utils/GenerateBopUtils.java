package za.co.synthesis.finsurvlocal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//TODO: Complete (client-friendly) java doc for the code below...
public class GenerateBopUtils {
    private static final Logger logger = LoggerFactory.getLogger(GenerateBopUtils.class);


    //------------------------------------------------------------------------------------------------------------------//
    public Map<String, String> staticParams = new HashMap<String, String>();

    //--------------------------------------------------------------//
    //  EVALUATION PARAMETERS
    public static String eval_domestic_value = "domesticValue";
    //DR Values...
    public static String eval_dr_country_code = "drCountryCode";
    public static String eval_dr_bic = "drCounterpartyInstitution_or_BIC";
    public static String eval_dr_acc_type = "drAccountType";
    public static String eval_dr_res_status = "drResidentStatus";
    public static String eval_dr_currency = "drSourceCurrency";
    public static String eval_dr_additional_params = "drAdditionalParams"; //additional params map
    public static String eval_dr_acc_holder_status = "AccountHolderStatus"; //stored in the drAdditionalParams map.
    public static String eval_dr_field72 = "field72";
    //CR Values...
    public static String eval_cr_country_code = "crCountryCode";
    public static String eval_cr_bic = "crCounterpartyInstitution_or_BIC";
    public static String eval_cr_acc_type = "crAccountType";
    public static String eval_cr_res_status = "crResidentStatus";
    public static String eval_cr_currency = "crDestinationCurrency";
    public static String eval_cr_additional_params = "crAdditionalParams"; //additional params map
    public static String eval_cr_acc_holder_status = "AccountHolderStatus"; //stored in the crAdditionalParams map.
    //--------------------------------------------------------------//

    //------------------------------------------------------------------------------------------------------------------//
    // TEMPLATE STRINGS

    public Map<String, String> stringTemplates;

    //------------------------------------------------------------------------------------------------------------------//

    public JSArray InstitutionalSectors = new JSArray();
    public LocalDateTime InstitutionalSectorsCache = null; //LocalDateTime.now()  //Last update time.

    //------------------------------------------------------------------------------------------------------------------//



    /**
     * Naive idea for generating a BOP wireframe from Evaluation Decision and accompanying DR/CR Demographic/KYC info...
     *
     * Ideally we would like to create some form of templating engine which we can use to generate the various parameters below.
     * The templates would be configured per system such that the system directly inputs the various parameters as maps and arrays
     * and the template then maps the relevant key:value pairs from the maps to the relevant template fields as needed.
     *
     * For now, we have assumed a specific format (analogous, as per the relevant section of the genv3 JSON schema)
     */
    public static String GenerateBOPReport(JSObject EvaluationDecisions, JSObject TransactionDetails, JSObject DRDemographics, JSObject CRDemographics, JSArray MonetaryDetails){
        String returnValue = "";
        //TODO: Complete the templating code here...
        return returnValue;
    }





    /**
     * this replaces params with wildcard (varaiable/template place-holders) with the respective values from the params map.
     * @param params
     * @return
     */
    public Map<String, Object> convertWildcardMap(Map<String, ?> params) {
        HashMap<String, Object> res = new HashMap<String, Object>();
        for (String key : params.keySet()){
            if (params.get(key) != null){
                if (params.get(key) instanceof String
                ) {
                    String val = (String)params.get(key);
                    val = val.replaceAll("\n", " ");
                    res.put(key,val);
                } else if (
                        params.get(key) instanceof Object  /*||
            params.get(key) instanceof Number ||
            params.get(key) instanceof List ||
            params.get(key) instanceof Map ||*/
                ) {
                    res.put(key,params.get(key));
                } else {
                    res.put(key,""+params.get(key));
                }
            }
        }
        return res;
    }

    /**
     * Builds and populates the main JSON BOP report template with the relevant data and sub-templates.
     */
    public String populateBOPTemplate(Map<String, ?> paramMap) {
        //Coerce the wildcarded Map<String, ?> paramMap into a Map<String, Object> instance so we can enrich it as needed...
        Map<String, Object> params = convertWildcardMap(paramMap);

        String templatePath = "templates/";
//    String[] bopTemplatePaths = {
//        "ReportTemplate.json",
//        "ResidentIndividualTemplate.json",
//        "ResidentEntityTemplate.json",
//        "ResidentExceptionTemplate.json"/*,
//        "NonResidentIndividualTemplate.json",
//        "NonResidentEntityTemplate.json",
//        "NonResidentExceptionTemplate.json",
//        "AddressTemplate.json",
//        "ContactTemplate.json"*/
//    };
        //Check if we are dealing with Entity, individual or Exception...
        String residentType = "Individual";
        String residentTemplate = "null";
        // TODO: Abstract all the hard-coded param keys and make configurable via an xml file.
        // TODO: We should be checking for a flag (from evaluation) that tells us to use "Exception" first...  then assume either Individual or Entity
        if (params != null &&
                ( /* If we are supplied with a surname, a name and we have no entity name specified ... then, we can assume this is an individual */
                        (params.get("ResExceptionName") == null || !(params.get("ResExceptionName") instanceof String) || ((String) params.get("ResExceptionName")).isEmpty()) &&
                                (params.get("osResidentSurname") != null && (params.get("osResidentSurname") instanceof String) && !((String) params.get("osResidentSurname")).isEmpty()) &&
                                (params.get("osResidentName") != null && (params.get("osResidentName") instanceof String) && !((String) params.get("osResidentName")).isEmpty()) &&
                                /*(params.get("osGender") != null && (params.get("osGender") instanceof String) && !((String) params.get("osGender")).isEmpty()) &&*/
                                (params.get("osResidentEntityName") == null || (!(params.get("osResidentEntityName") instanceof String)) || ((String) params.get("osResidentEntityName")).isEmpty())
                )
        ){
            residentType = "Individual";
        } else if (params.get("ResExceptionName") == null || !(params.get("ResExceptionName") instanceof String) || ((String) params.get("ResExceptionName")).isEmpty()){
            residentType = "Entity";
        } else {
            residentType = "Exception";
        }

        String nonResidentType = "Individual";
        String nonResidentTemplate = "null";
        if (params != null &&
                ( /* If we are supplied with a surname, a name and we have no entity name specified ... then, we can assume this is an individual */
                        (params.get("osSurname") != null && (params.get("osSurname") instanceof String) && !((String) params.get("osSurname")).isEmpty()) &&
                                (params.get("osName") != null && (params.get("osName") instanceof String) && !((String) params.get("osName")).isEmpty()) &&
                                (params.get("osEntityName") == null || (!(params.get("osEntityName") instanceof String)) || ((String) params.get("osEntityName")).isEmpty())
                )
        ){
            nonResidentType = "Individual";
            //enrich data as needed...
            params.put("osName_osSurname", params.get("osName").toString() +" "+ params.get("osSurname"));
        } else {
            nonResidentType = "Entity";
        }


        residentTemplate = loadTemplate(templatePath+"Resident"+residentType+"Template.json", params, null,null);
        nonResidentTemplate = loadTemplate(templatePath+"NonResident"+nonResidentType+"Template.json", params, null,null);


        HashMap<String, String> childTemplates = new HashMap<String, String>();
        childTemplates.put("ResidentTemplate", residentTemplate);
        childTemplates.put("NonResidentTemplate", nonResidentTemplate);

        //dont lose the calculated data...
        Map<String, Object> calcMap = new JSObject();
        calcMap.putAll(params);
        calcMap.remove("iMAL_API_Data");
        params.put("iMAL_Calculated_Data", calcMap);

        String result = loadTemplate(templatePath+"ReportTemplate.json", params, childTemplates);
        return result;
    }

    /**
     * Loads a template and it's sub-templates from the specified file and populates with data provided.
     * @param templateFile
     * @param params
     * @param childTemplateName
     * @param childTemplate
     * @return
     */
    public String loadTemplate(String templateFile, Map<String, ?> params, String childTemplateName, String childTemplate){
        return loadTemplate(templateFile, params, childTemplateName, childTemplate, true);
    }

    /**
     * Loads a template and it's sub-templates from the specified file and populates with data provided.
     * @param templateFile
     * @param params
     * @param childTemplateName
     * @param childTemplate
     * @param quoteStrings
     * @return
     */
    public String loadTemplate(String templateFile, Map<String, ?> params, String childTemplateName, String childTemplate, boolean quoteStrings){
        HashMap<String, String> childTemplates = null;
        if (childTemplateName instanceof String && !childTemplateName.isEmpty()) {
            childTemplates = new HashMap<String, String>();
            childTemplates.put(childTemplateName, childTemplate);
        }
        return loadTemplate(templateFile, params, childTemplates, quoteStrings);
    }

    public String loadTemplate(String templateFile, Map<String, ?> params, Map<String, String> childTemplates){
        return loadTemplate(templateFile, params, childTemplates, true);
    }

    public String loadTemplate(String templateFile, Map<String, ?> params, Map<String, String> childTemplates, boolean quoteStrings){
        String templateStr = "";

        ClassLoader classLoader = this.getClass().getClassLoader();
        URL fileResourceUrl = classLoader.getResource(templateFile);
        try {
            if (fileResourceUrl != null) {
                Path bytesPath = Paths.get(fileResourceUrl.toURI());
                if (bytesPath != null) {
                    byte[] bytes = Files.readAllBytes(bytesPath);
                    if (bytes != null) {
                        templateStr = new String(bytes);
                    }
                }
            }
        }
        catch (Exception e){
            logger.error("Exception while trying to load template file ("+templateFile+"):"+e.getMessage());
        }

        if (params instanceof Map) {
            for (Map.Entry<String, ?> entry : params.entrySet()) {
                String val = "null";
                if (entry.getValue() instanceof Object) {
                    Object objVal = entry.getValue();
                    if (objVal != null) {
                        if (objVal instanceof String[]) {
                            if (((String[]) objVal).length == 1) {
                                val = (quoteStrings ? "\"" : "") + (((String[]) objVal)[0]!=null?(((String[]) objVal)[0]).trim():((String[]) objVal)[0]) + (quoteStrings ? "\"" : "");
                            } else if (((String[]) objVal).length > 1) {
                                val = JsonUtils.listToJsonStr(Arrays.asList(entry.getValue()));
                            }
                        } else if (objVal instanceof List) {
                            val = JsonUtils.listToJsonStr((List) objVal);
                        } else if (objVal instanceof Map) {
                            val = JsonUtils.mapToJsonStr((Map) objVal);
                        } else if (objVal instanceof String) {
                            if (!"null".equalsIgnoreCase((String) objVal)) {
                                val = (quoteStrings ? "\"" : "") + ((String) objVal).trim() + (quoteStrings ? "\"" : "");
                            }
                        } else if (objVal instanceof Number) {
                            val = "" + objVal;
                        } else if (objVal instanceof Object) {
                            val = "" + objVal.toString();
                        }
                    }
                    templateStr = templateMerge(templateStr, entry.getKey(), (val!=null?val.trim():val));
                } else if (entry.getValue() != null) {
                    templateStr = templateMerge(templateStr, entry.getKey(), entry.getValue() + "");
                } else {
                    templateStr = templateMerge(templateStr, entry.getKey(), "null");
                }
            }
        }
        if (childTemplates != null) {
            for (Map.Entry<String, String> entry : childTemplates.entrySet()) {
                String childTemplateName = entry.getKey();
                String childTemplate = entry.getValue();
                templateStr = templateMerge(templateStr, childTemplateName, childTemplate);
            }
        }

        templateStr = templateStr.replaceAll("[" + Pattern.quote("$#")+"]_?" + Pattern.quote("{") + "[^}]*" + Pattern.quote("}"), "null"); //REMOVE ANY REMAINING (EMPTY) ${} PLACEHOLDERS -> null.
        return templateStr;
    }

    public static String templateMerge(String templateStr, String childTemplateName, String childTemplate){
        String resultStr = null;
        String replacementStr = (childTemplate instanceof String ? childTemplate : "null");
        String regexStr = "(?i)" + "[" + Pattern.quote("$#") + "]_?" + Pattern.quote("{" + childTemplateName + "}");
        try {
            templateStr.replaceAll(regexStr, replacementStr);
        } catch (Exception error){
            //bleh.
            logger.info("Unable to replace template ("+childTemplateName+") placeholder with the relevant content: "+error.getMessage());
        }
        if (resultStr == null) try {
            String[] templateParts = templateStr.split(regexStr);
            String mergeStr = "";
            for (String part : templateParts){
                mergeStr += (((mergeStr.length() > 0) ? replacementStr : "") + part);
            }
            resultStr = mergeStr.length() > 0 ? mergeStr : resultStr;
        } catch (Exception error){
            //bleh.
        }
        return resultStr != null ? resultStr : templateStr;
    }


    public String getStringTemplate(String templateKey, boolean quoteStrings, Map<String, Object> params) {
        String templateStr = null;
        if (stringTemplates == null) {
            stringTemplates = new HashMap<String, String>();
        }
        //TODO: We need to actually load the templates from file.

        if (stringTemplates != null && stringTemplates.containsKey(templateKey)){
            templateStr= stringTemplates.get(templateKey);
            if (staticParams == null) {
                staticParams = new HashMap<String, String>();
            }
            if (staticParams != null){
                //Replace static params
                templateStr=populateTemplate(templateStr, staticParams, quoteStrings);
            }
        }
        if (params instanceof Map) {
            templateStr=populateTemplate(templateStr, params, quoteStrings);
        }
        return templateStr;
    }

    /**
     * Basic templating function which takes a template string and populates it with the relevant param values provided
     * @param templateStr
     * @param params
     * @param quoteStrings
     * @return
     */
    public String populateTemplate(String templateStr, Map<String, ?> params, boolean quoteStrings) {
        for (Map.Entry<String, ?> entry : params.entrySet()){
            String val = "null";
            if (entry.getValue() instanceof Object) {
                Object objVal = entry.getValue();
                if (objVal != null) {
                    if (objVal instanceof String[]) {
                        if (((String[]) objVal).length == 1) {
                            val = (quoteStrings?"\"":"") + ((String[]) objVal)[0] + (quoteStrings?"\"":"");
                        } else if (((String[]) objVal).length > 1) {
                            val = JsonUtils.listToJsonStr(Arrays.asList(entry.getValue()));
                        }
                    } else if (objVal instanceof List) {
                        val = JsonUtils.listToJsonStr((List) objVal);
                    } else if (objVal instanceof Map) {
                        val = JsonUtils.mapToJsonStr((Map) objVal);
                    } else if (objVal instanceof String) {
                        if (!"null".equalsIgnoreCase((String) objVal)) {
                            val = (quoteStrings?"\"":"") + objVal + (quoteStrings?"\"":"");
                        }
                    } else if (objVal instanceof Number) {
                        val = "" + objVal;
                    }
                }
                templateStr = templateStr.replaceAll("(?i)" + Pattern.quote("${"+entry.getKey()+"}"), val);
            } else if (entry.getValue() != null){
                templateStr = templateStr.replaceAll("(?i)" + Pattern.quote("${" + entry.getKey() + "}"), entry.getValue()+"");
            } else {
                templateStr = templateStr.replaceAll("(?i)" + Pattern.quote("${" + entry.getKey() + "}"), "null");
            }
        }
        return templateStr;
    }




    //******************************************************************************************************************//
    //    Borrowed and adapted from the Synthesis RegexGenerator project
    //******************************************************************************************************************//
    public Map<String,String> namedRegex(String regex, String data){
        Map<String,String> results = new HashMap<String,String>();
        if (data != null && data.length() > 0) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(data);
            List<String> namedGroups = getNamedGroupCandidates(regex);

            while (matcher.find()) {
                MatchResult matchResult = matcher.toMatchResult();
                String groupMatchString = "";
                if (matchResult.groupCount() > 0) {
                    for (int j = 1; j <= matchResult.groupCount(); j++) {
                        String group = matchResult.group(j);
                        if (groupMatchString.isEmpty() && !group.isEmpty()) {
                            groupMatchString = group;
                        }
                        String groupName = "";
                        //extract the group name for the matched group.
                        if (!namedGroups.isEmpty()) {
                            for (String name : namedGroups) {
                                if (matcher.group(name) != null &&
                                        matcher.group(name).equals(group) &&
                                        matcher.start(name) == matchResult.start(j) &&
                                        matcher.end(name) == matchResult.end(j)) {
                                    groupName = name;
                                    break;
                                }

                            }
                        }
                        if (group != null && !group.isEmpty() && !groupName.isEmpty()) {
                            results.put(groupName, group);
                        }
                    }
                }
                // else
                //Always add the base match string...
                { //No actual groups, matched entire string.
                    String group = matchResult.group(0);
                    results.put("BASEMATCH", group);
                }
            }
        }
        return results;
    }

    private static List<String> getNamedGroupCandidates(String regex) {
        List<String> namedGroups = new ArrayList<String>();
        Matcher m = Pattern.compile("\\(\\?<([a-zA-Z][a-zA-Z0-9]*)>").matcher(regex);
        while (m.find()) {
            namedGroups.add(m.group(1));
        }
        return namedGroups;
    }


    public static String epochSecondsToDate(String epochSeconds){
        Long epochSecs;
        if (epochSeconds != null)
            try{
                epochSecs = Long.parseLong(epochSeconds);
                return epochSecondsToDate(epochSecs);
            }  catch (Exception e){
                logger.info("Failed to parse epoch seconds value ("+epochSeconds+"): "+e.getMessage());
            }
        return null;
    }

    public static String epochSecondsToDate(long epochSeconds) {
        return epochMillisToDate(epochSeconds*1000L);
    }

    public static String epochMillisToDate(String epochMillis){
        Long epochMils;
        if (epochMillis != null)
            try{
                epochMils = Long.parseLong(epochMillis);
                return epochMillisToDate(epochMils);
            }  catch (Exception e){
                logger.info("Failed to parse epoch milliseconds value ("+epochMillis+"): "+e.getMessage());
            }
        return null;
    }

    public static String epochMillisToDate(long millis){
        Date date = new Date(millis);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setTimeZone(TimeZone.getTimeZone("Africa/Johannesburg"));
        String formattedDate = sdf.format(date);
        logger.info(formattedDate +" (TimezoneID: Africa/Johannesburg -> GMT+2)"); // Tuesday,November 1,2011 12:00,AM
        return formattedDate;
    }

    public static String getDateTimeIdStr(){
        String res = "";
        LocalDateTime dt = LocalDateTime.now();
        res += ""+
                dt.getYear() +
                (dt.getMonthValue()>9?dt.getMonthValue():"0"+dt.getMonthValue()) +
                (dt.getDayOfMonth()>9?dt.getDayOfMonth():"0"+dt.getDayOfMonth()) +
                (dt.getHour()>9?dt.getHour():"0"+dt.getHour()) +
                (dt.getMinute()>9?dt.getMinute():"0"+dt.getMinute()) +
                (dt.getSecond()>9?dt.getSecond():"0"+dt.getSecond());
        return res;
    }


    /**
     * TODO: BORROWED FROM THE INTEGR8R PROJECT TO BE USED AS A TEMPLATE FOR ENRICHING VALUES...
     * @param params
     */

    private void enrichParams(Map<String, Object> params) {
        Map<String, String> paramsReplace = new HashMap<String, String>();
        //Strip out (replace with space) all carriage returns and line feeds - they cause havoc with the JSON
        for (Map.Entry<String,Object> param : params.entrySet()){
            if (param.getValue() instanceof String && ((String)param.getValue()).matches(".*(\\r\\n|\\r|\\n|\\\\r\\\\n|\\\\r|\\\\n)+.*")){
                String val = (String) param.getValue();
                val = val.replaceAll("(\\r\\n|\\r|\\n|\\\\r\\\\n|\\\\r|\\\\n)", " ");
                if (!val.equalsIgnoreCase((String)param.getValue())) {
                    paramsReplace.put(param.getKey() + "_BK", (String) param.getValue());
                    paramsReplace.put(param.getKey(), val);
                }
            }
        }
        params.putAll(paramsReplace);


        //-----  POSTAL CODES PADDING  -----//
        //Requested enrichment to LEFT pad postal codes of less than 4 characters (1..3 chars), with "0" chars...
        //osStreetPostalCodeDesc
        if (params.containsKey("osStreetPostalCodeDesc") && params.get("osStreetPostalCodeDesc") != null) {
            String postCodeStr = "" + params.get("osStreetPostalCodeDesc");
            if (postCodeStr.matches("^\\d{1,3}$")) {
                params.put("osStreetPostalCodeDesc", postCodeStr);
                postCodeStr = "0000".substring(postCodeStr.length()) + postCodeStr;
                params.put("osStreetPostalCodeDesc", postCodeStr);
                logger.info("ENRICHMENT: 0-padded street postal code");
            }
        }
        //osPostalCodeDesc
        if (params.containsKey("osPostalCodeDesc") && params.get("osPostalCodeDesc") != null) {
            String postCodeStr = "" + params.get("osPostalCodeDesc");
            if (postCodeStr.matches("^\\d{1,3}$")) {
                params.put("osPostalCodeDesc", postCodeStr);
                postCodeStr = "0000".substring(postCodeStr.length()) + postCodeStr;
                params.put("osPostalCodeDesc", postCodeStr);
                logger.info("ENRICHMENT: 0-padded postal address postal code");
            }
        }
        //osThirdPartyStreetPostCodeDesc
        if (params.containsKey("osThirdPartyStreetPostCodeDesc") && params.get("osThirdPartyStreetPostCodeDesc") != null) {
            String postCodeStr = "" + params.get("osThirdPartyStreetPostCodeDesc");
            if (postCodeStr.matches("^\\d{1,3}$")) {
                params.put("osThirdPartyStreetPostCodeDesc", postCodeStr);
                postCodeStr = "0000".substring(postCodeStr.length()) + postCodeStr;
                params.put("osThirdPartyStreetPostCodeDesc", postCodeStr);
                logger.info("ENRICHMENT: 0-padded 3rd party street postal code");
            }
        }
        //osThirdPartyPostalCodeDesc
        if (params.containsKey("osThirdPartyPostalCodeDesc") && params.get("osThirdPartyPostalCodeDesc") != null) {
            String postCodeStr = "" + params.get("osThirdPartyPostalCodeDesc");
            if (postCodeStr.matches("^\\d{1,3}$")) {
                params.put("osThirdPartyPostalCodeDesc", postCodeStr);
                postCodeStr = "0000".substring(postCodeStr.length()) + postCodeStr;
                params.put("osThirdPartyPostalCodeDesc", postCodeStr);
                logger.info("ENRICHMENT: 0-padded 3rd party postal address postal code");
            }
        }
        //-----  POSTAL CODES PADDING  -----//



        //All Epoch Seconds to be converted to a date string...
        if (params.containsKey("odtValueDate") && params.get("odtValueDate") != null) {
            String epochValueDate = "" + params.get("odtValueDate");
            if (epochValueDate.matches("-?\\d{9,15}")) {
                String newValueDate = epochMillisToDate(epochValueDate);
                params.put("ValueDate", newValueDate);
                logger.info("ENRICHMENT: Converted epoch ValueDate field (odtValueDate) : "+epochValueDate+" => "+newValueDate);
            } else {
                params.put("ValueDate", epochValueDate);
            }
        }

        if (params.containsKey("DateOfBirth") && params.get("DateOfBirth") instanceof String) {
            String epochValueDate = "" + params.get("DateOfBirth");
            if (epochValueDate.matches("-?\\d{9,15}")) {
                String newValueDate = epochMillisToDate(epochValueDate);
                params.put("DateOfBirth", newValueDate);
                logger.info("ENRICHMENT: Converted epoch DOB field (DateOfBirth) : "+epochValueDate+" => "+newValueDate);
            } else if (epochValueDate.trim().matches("^\\d{4}[-/\\\\.]\\d{2}[-/\\\\.]\\d{2}.+$")) {
                String newValueDate = epochValueDate.trim().substring(0,10);
                params.put("DateOfBirth", newValueDate);
                logger.info("ENRICHMENT: Trimmed DOB field (DateOfBirth) : "+epochValueDate+" => "+newValueDate);
            } else {
                params.put("DateOfBirth", epochValueDate);
            }
        } else if (params.containsKey("osIdNumber") && params.get("osIdNumber") instanceof String) {  /*  -----  TRY DERIVE FROM ID NO IF AVAILABLE (osIdNumber)  -----  */
            String idNo = ((String) params.get("osIdNumber")).trim();
            if (idNo instanceof String & idNo.matches("\\d{13}")) {
                Map<String, String> idParts = namedRegex("^(?<YEAR>\\d{2})(?<MONTH>\\d{2})(?<DAY>\\d{2}).*$", idNo);
                if (idParts.size() >= 3){
                    String year = (Integer.parseInt(idParts.get("YEAR")) < 10?"20":"19")+idParts.get("YEAR");
                    String dob = year + "-" + idParts.get("MONTH") + "-" + idParts.get("DAY");
                    if (dob.length() == 10) {
                        params.put("DateOfBirth", dob);
                        logger.info("ENRICHMENT: Calculated DOB field (DateOfBirth) from ID field (osIdNumber) : "+idNo+" => "+dob);
                    }
                }
            }
        }

        if (params.containsKey("odtBirthDate") && params.get("odtBirthDate") instanceof String) {
            String epochDate = "" + params.get("odtBirthDate");
            if (epochDate.trim().matches("^-?\\d{9,15}$")) {
                String newValueDate = epochMillisToDate(epochDate);
                params.put("odtBirthDate_BK", epochDate);
                if (!newValueDate.matches("^-?\\d{9,15}$")) {
                    params.put("odtBirthDate", newValueDate);
                    logger.info("ENRICHMENT: Converted epoch DOB field (odtBirthDate) : "+epochDate+" => "+newValueDate);
                } else {
                    params.put("odtBirthDate", null);
                }
            } else if (epochDate.trim().matches("^\\d{4}[-/\\\\.]\\d{2}[-/\\\\.]\\d{2}.+$")) {
                String newValueDate = epochDate.trim().substring(0,10);
                params.put("odtBirthDate_BK", epochDate);
                params.put("odtBirthDate", newValueDate);
                logger.info("ENRICHMENT: Trimmed DOB field (odtBirthDate) : "+epochDate+" => "+newValueDate);
            } else {
                params.put("odtBirthDate", epochDate);
            }
        }
        if (params.containsKey("osIdNumber") && params.get("osIdNumber") instanceof String &&
                (
                        (!params.containsKey("odtBirthDate")) ||
                                (!(params.get("odtBirthDate") instanceof String)) ||
                                ((String) params.get("odtBirthDate")).trim().isEmpty())
        ) {
            /*  -----  TRY DERIVE FROM ID NO IF AVAILABLE (osIdNumber)  -----  */
            String idNo = ((String) params.get("osIdNumber")).trim();
            if (idNo instanceof String & idNo.matches("\\d{13}")) {
                Map<String, String> idParts = namedRegex("^(?<YEAR>\\d{2})(?<MONTH>\\d{2})(?<DAY>\\d{2}).*$", idNo);
                if (idParts.size() >= 3){
                    String year = (Integer.parseInt(idParts.get("YEAR")) < 10 ? "20":"19")+idParts.get("YEAR");
                    String dob = year + "-" + idParts.get("MONTH") + "-" + idParts.get("DAY");
                    if (dob.length() == 10) {
                        params.put("odtBirthDate", dob);
                        logger.info("ENRICHMENT: Calculated DOB field (odtBirthDate) from ID field (osIdNumber) : "+idNo+" => "+dob);
                    }
                }
            }
        }

        if (params.containsKey("odtThirdPartyBirthDate") && params.get("odtThirdPartyBirthDate") instanceof String) {
            String epochValueDate = "" + params.get("odtThirdPartyBirthDate");
            if (epochValueDate.matches("\\d{9,15}")) {
                String newValueDate = epochMillisToDate(epochValueDate);
                params.put("odtThirdPartyBirthDate", newValueDate);
                logger.info("ENRICHMENT: Converted epoch DOB field (odtThirdPartyBirthDate) : "+epochValueDate+" => "+newValueDate);
            } else {
                params.put("odtThirdPartyBirthDate", epochValueDate);
            }
        }


        //-----  EMAIL Enrichments  -----//

        String validEmailRegex = "^[\\w\\-]+(?:\\.[\\w\\-]+)*(?:\\+[\\w\\-]+(?:\\.[\\w\\-]+)*)?@[\\w\\-]+(?:\\.[\\w\\-]+)+$";
        if (params.containsKey("osEmail") && params.get("osEmail") instanceof String) {
            String emailStr = ("" + params.get("osEmail")).trim();
            params.put("osEmail_BK", emailStr);
            if (emailStr.matches("^[^;\\s]{5,}(\\s*;\\s*([^;\\s]{5,})?)*$")) {
                String[] emailStrings = emailStr.split("\\s*;\\s*");
                String primaryMailStr = null;
                for (String email : emailStrings){
                    if (email.trim().matches(validEmailRegex)) {
                        primaryMailStr = email;
                        params.put("osEmail", primaryMailStr);
                        break;
                    }
                }
                if (primaryMailStr instanceof String) {
                    logger.info("(osEmail) Primary email extracted from list: "+primaryMailStr);
                } else {
                    logger.info("(osEmail) No valid primary email available in list: "+emailStr);
                    params.put("osEmail", null);  //remove any invalid email address content for these locked down fields.
                }
            } else {
                if (!emailStr.trim().matches(validEmailRegex)) {
                    logger.info("(osEmail) Not a valid email address: " + emailStr);
                    params.put("osEmail", null); //remove any invalid email address content for these locked down fields.
                }
            }
        }


        if (params.containsKey("osThirdPartyEmail") && params.get("osThirdPartyEmail") instanceof String) {
            String emailStr = "" + params.get("osThirdPartyEmail");
            if (emailStr.matches("(([\\w\\-\\.\\@])+(\\s*;\\s*)?)+")) {
                String[] emailStrings = emailStr.split("\\s*;\\s*");
                String primaryMailStr = null;
                for (String email : emailStrings){
                    if (email.trim().matches(validEmailRegex)) {
                        primaryMailStr = email;
                        params.put("osThirdPartyEmail", primaryMailStr);
                        break;
                    }
                }
                if (primaryMailStr instanceof String) {
                    logger.info("(osThirdPartyEmail) Primary email extracted from list: "+primaryMailStr);
                } else {
                    logger.info("(osThirdPartyEmail) No valid primary email available in list: "+emailStr);
                    params.put("osThirdPartyEmail", null); //Clear out invalid email addresses for these locked-down fields.
                }
            } else {
                if (!emailStr.trim().matches(validEmailRegex)) {
                    logger.info("(osThirdPartyEmail) Not a valid email address: " + emailStr);
                    params.put("osThirdPartyEmail", null); //Clear out invalid email addresses for these locked-down fields.
                }
            }
        }

        //-------------------------------//


        //default the Branch Code and Branch Name...
        //TODO: default BranchCode and BranchName should be externalized and should also be made as a lookup and override as NEEDED etc.
        params.put("BranchCode", "99500800");
        params.put("BranchName", "Forex Head Office");
        logger.info("ENRICHMENT: Defaulted BranchCode and BranchName: (\"99500800\" : \"Forex Head Office\")");

    /*
    "IndustrialClassification": ${osIndustrialClassification},
    */

        params.put("osIndustrialClassification_BK", params.get("osIndustrialClassification"));
        params.put("IndustrialClassification", params.get("osIndustrialClassification"));
        if (params.get("osIndustrialClassification") instanceof String) {
            String ic = ((String) params.get("osIndustrialClassification")).trim();
            //Perform mapping as requested
            if (ic.matches("^\\d{1,2}$")) {
                try {
                    Integer icInt = Integer.parseInt(ic);
                    if (icInt == 1) {
                        //    1	Agriculture, Fo	Agriculture, Fo	Growing of Crops,Mkt.Gardening	1
                        ic = "01";
                    } else if (icInt == 2) {
                        //    2	Construction	Construction	Construction & Engineering	5
                        ic = "05";
                    } else if (icInt == 3){
                        //    3	Finance and Ins	Finance and Ins	Monetary Intermediation	8
                        ic = "08";
                    } else if (icInt == 4){
                        //    4	Government	Government	Regional Serv.Council Activity	10
                        ic = "10";
                    } else if (icInt == 5){
                        //    5	Manufacturing	Manufacturing	Prod,Collect.& Distrib.Elect.	3
                        ic = "03";
                    } else if (icInt == 6){
                        //    6	Mining	Mining	Mining and Quarrying	2
                        ic = "02";
                    } else if (icInt == 7){
                        //    7	Other Services	Other Services	R & D on Natural Science	4
                        ic = "04";
                    } else if (icInt == 8){
                        //    8	Personal & Con	Personal & Con	Priv.H/hold with Employ Person	10
                        ic = "10";
                    } else if (icInt == 9){
                        //    9	Trade, Retail	Trade, Retail	Parts - Access.for MV & Engine	6
                        ic = "06";
                    } else if (icInt == 10){
                        //    10	Real Estate	Real Estate	Real Estate & Bus.Serv	8
                        ic = "08";
                    } else if (icInt == 11){
                        //    11	Technology, Med	Technology, Med	Technology, Media and Telecommun	7
                        ic = "07";
                    } else if (icInt == 12){
                        //    12	Transportation	Transportation	Transport & Communication	7
                        ic = "07";
                    } else {
                        //    999	Unclassified		Unclassified	10
                        ic = "10";
                    }
                } catch (Exception err){
                    logger.info("Unable to parse osIndustrialClassification value for mapping: "+ic);
                }
            }
            params.put("IndustrialClassification", ic);
            if (ic.matches("^\\d$")) {
                params.put("osIndustrialClassification_BK", ic);
                params.put("osIndustrialClassification", "0" + ic);
                params.put("IndustrialClassification", "0" + ic);
            } else if (ic.matches("^\\d{2}$")) {
                params.put("IndustrialClassification", ic);
            } else if (ic.matches("^(\\w+\\s*)+$")) {
                params.put("osIndustrialClassification_BK", ic);
                //TODO: Industrial classification description may have been provided, perform code lookup...
                params.put("osIndustrialClassification", "0" + ic);
                params.put("IndustrialClassification", ic);
            } else {
                logger.debug("Incorrect Industrial Classification Code provided: " + ic);
            }
        }

    /*
    "InstitutionalSector": ${osInstitutionalSector},
     */
        params.put("InstitutionalSector", params.get("osInstitutionalSector"));
        if (params.get("osInstitutionalSector") instanceof String) {
            String ic = ((String) params.get("osInstitutionalSector")).trim();
            params.put("InstitutionalSector", ic);
            if (ic.matches("^\\d$")) {
                params.put("osInstitutionalSector_BK", ic);
                params.put("osInstitutionalSector", "0" + ic);
                params.put("InstitutionalSector", "0" + ic);
            } else if (ic.matches("^\\d{2}$")) {
                params.put("InstitutionalSector", ic);
            } else if (ic.matches("^(\\w+\\s?)+$")) {
                params.put("osInstitutionalSector_BK", ic);
                params.put("InstitutionalSector", ic);
                params.put("InstitutionalSectorDescription", ic);
                updateInstitutionalSectorsCache(params);
                if (InstitutionalSectors instanceof JSArray) {
                    String icCode = "";
                    for (Object obj : InstitutionalSectors) {
                        if (obj instanceof JSObject) {
                            JSObject jso = (JSObject) obj;
                            if (jso.get("Description") instanceof String && ((String) jso.get("Description")).equalsIgnoreCase(ic)) {
                                params.put("InstitutionalSector", ((String) jso.get("Code")));
                                break;
                            }
                        }
                    }
//          for (Map.Entry<String,Object> entry : InstitutionalSectors.entrySet()){
//            if (entry.getKey().equalsIgnoreCase(ic)){
//              params.put("InstitutionalSector", entry.getValue());
//            }
//          }
                }
            } else {
                logger.debug("Incorrect Industrial Classification Code provided: " + ic);
            }
        }

        //Case Sensitive Province Values...
        //TODO: We should be validating that the provinces provided are correct and maybe intelligently choose the correct one where possible?
        if (params.get("osStreetProvince") instanceof String) {
            String prov = ((String) params.get("osStreetProvince"));
            params.put("osStreetProvince_BK", prov);
            prov = prov.replaceAll("[^a-zA-Z\\s]", "");  //strip all non Alpha and space characters.
            prov = prov.trim().toUpperCase();
            params.put("osStreetProvince", prov);
        }
        if (params.get("osPostalProvince") instanceof String) {
            String prov = ((String) params.get("osPostalProvince"));
            params.put("osPostalProvince_BK", prov);
            prov = prov.replaceAll("[^a-zA-Z\\s]", "");  //strip all non Alpha and space characters.
            prov = prov.trim().toUpperCase();
            params.put("osPostalProvince", prov);
        }
        if (params.get("osThirdPartyStreetProvince") instanceof String) {
            String prov = ((String) params.get("osThirdPartyStreetProvince"));
            params.put("osThirdPartyStreetProvince_BK", prov);
            prov = prov.replaceAll("[^a-zA-Z\\s]", "");  //strip all non Alpha and space characters.
            prov = prov.trim().toUpperCase();
            params.put("osThirdPartyStreetProvince", prov);
        }
        if (params.get("osThirdPartyPostalProvince") instanceof String) {
            String prov = ((String) params.get("osThirdPartyPostalProvince"));
            params.put("osThirdPartyPostalProvince_BK", prov);
            prov = prov.replaceAll("[^a-zA-Z\\s]", "");  //strip all non Alpha and space characters.
            prov = prov.trim().toUpperCase();
            params.put("osThirdPartyPostalProvince", prov);
        }

        //-----  ENRICHMENTS TO ASSIST EVALUATION  -----//
        /*
        if ((params.get("osOriginatingBank") instanceof String) &&
                (!((String)params.get("osOriginatingBank")).matches(thisBICRegex)) &&
                (!(params.get("osReceivingBank") instanceof String))) {
            params.put("osReceivingBank", thisBIC);
            logger.info("ENRICHMENT: Defaulted missing Receiving BIC to \""+thisBIC+"\"");
        }

        if ((params.get("osReceivingBank") instanceof String) &&
                (!((String)params.get("osReceivingBank")).matches(thisBICRegex)) &&
                (!(params.get("osOriginatingBank") instanceof String))) {
            params.put("osOriginatingBank", thisBIC);
            logger.info("ENRICHMENT: Defaulted missing Originating BIC to \""+thisBIC+"\"");
        }
        */

        //-----  END OF ENRICHMENTS  -----//
        //--------------------------------//
    }

    /**
     * TODO: Use this as a template for implementing cached lookup values?
     * @param params
     */
    public void updateInstitutionalSectorsCache(Map<String, Object> params){
        /*
        Object bopDataUrl = getInstance().getStringTemplate("Lookup_InstitutionalSector_Url", false, params);
        if (InstitutionalSectorsCache == null || InstitutionalSectorsCache.isBefore(LocalDateTime.now().minusHours(1))) {
            if (bopDataUrl instanceof String) {
                String resultStr = getInstance().doHttpCall(getInstance().GET, (String) bopDataUrl, "application/json", null);

                try {
                    JSArray bopData = (JSArray) JsonUtils.jsonStrToList(resultStr);
                    if (bopData instanceof JSArray) {
                        InstitutionalSectors.addAll(bopData);
                        InstitutionalSectorsCache = LocalDateTime.now();
                    }
                } catch (Exception error) {
                    logger.info("Exception while updating the Institutional Sector lookup list: " + error.getMessage());
                    error.printStackTrace();
                }
            }
        }
         */
    }



}
