package za.co.synthesis.finsurvlocal.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Various functions used to produce, consume or compare checksums of data for finger-printing and integrity checking reasons.
 */
public class Checksum {

    /**
     *  A hash checksum is created from an inputstream using the specified hash algorithm.
     *  used for finger-printing and integrity checking.
     * @param inputStream
     * @param hashType
     * @return
     * @throws Exception
     */
    public static byte[] createChecksum(InputStream inputStream, String hashType) throws Exception {
        //poached from : https://www.rgagnon.com/javadetails/java-0416.html
        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance(hashType);
        int numRead;
        do {
            numRead = inputStream.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        inputStream.close();
        return complete.digest();
    }


    public static String getChecksumString(InputStream inputStream, String hashType) throws Exception {
        byte[] b = createChecksum(inputStream, hashType);
        String result = "";
        for (int i=0; i < b.length; i++) {
            result +=
                    Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }


    public static String getChecksumForDataString(String data, String hashType) throws Exception {
        return getChecksumString(new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)), hashType);
    }

    /**
     * Builds up a list of different checksum algorithms
     * @param algorithms
     * @return
     */
    public static List<String> getDefaultChecksumAlgorithms(List<String> algorithms){
        if (algorithms == null){
            algorithms = new ArrayList<String>();
        }
        if (algorithms.size() == 0){
            //TODO: these should be pulled or configured externally somewhere.
            algorithms.add("MD5");
            algorithms.add("SHA1");
            algorithms.add("SHA-256");
            algorithms.add("SHA-384");
            algorithms.add("SHA-512");
        }
        return algorithms;
    }

    /**
     * Builds up a map with the data string as a checksum created from each of the different algorithms
     * @param dataStr
     * @param algorithms
     * @return
     */
    public static Map<String, String> getChecksumValues(String dataStr, List<String> algorithms) {
        HashMap<String, String> checksumMap = new HashMap<String, String>();
        if (algorithms == null || algorithms.size() == 0){
            algorithms = getDefaultChecksumAlgorithms(algorithms);
        }
        for (String algorithm : algorithms) {
            try {
                checksumMap.put(algorithm, getChecksumForDataString(dataStr, algorithm));
            } catch (Exception e) {
                System.err.println("Unable to create or add checksum ("+algorithm+"): " + e.getMessage());
            }
        }
        return checksumMap;
    }

    /**
     * Takes in a data string, a checksum algorithm and checksum value. The provided checksum algorithm and value are compared to the checksums created from the data string.
     * If no checksum value are provided, the generated checksums are returned
     * @param dataStr
     * @param checksumAlgorithm
     * @param checksumValue
     * @return
     */
    public static Map<String, String> checkForChecksumMatch(String dataStr, String checksumAlgorithm, String checksumValue) {
        ArrayList<String> checksumAlgorithms = new ArrayList<String>();
        if (checksumAlgorithm != null && !checksumAlgorithm.isEmpty()) {
            checksumAlgorithms.add(checksumAlgorithm);
        }
        Map<String, String> checksums = getChecksumValues(dataStr, checksumAlgorithms);
        if (checksumValue != null && !checksumValue.isEmpty()){
            boolean checksumMatched = false;
            for (Map.Entry<String, String> checksum : checksums.entrySet()) {
                if (checksum.getValue().equals(checksumValue)){
                    if (checksumAlgorithm == null || checksumAlgorithm.isEmpty() || checksumAlgorithm.equalsIgnoreCase(checksum.getKey())) {
                        checksums.put("MatchedAlgorithm", checksum.getKey());
                        if (checksumAlgorithm != null && !checksumAlgorithm.isEmpty()) {
                            checksums.put("MatchedChecksum", checksum.getValue());
                        }
                        checksumMatched = true;
                        checksums.put("MATCH", "YES");
                        break;
                    }
                }
            }
            checksums.put("ProvidedChecksum", checksumValue);
            if (checksumAlgorithm != null && !checksumAlgorithm.isEmpty()) {
                checksums.put("ProvidedAlgorithm", checksumAlgorithm);
            }
            if (!checksumMatched){
                checksums.put("MATCH", "NO");
            }
        }
        return checksums;
    }

}
