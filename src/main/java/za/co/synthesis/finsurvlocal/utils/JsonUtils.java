package za.co.synthesis.finsurvlocal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.rule.core.BankAccountType;
import za.co.synthesis.rule.core.IEvaluationDecision;
import za.co.synthesis.rule.core.IEvaluationScenarioDecision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.finsurvlocal.utils.CommonHelperFunctions.getFileContent;

//TODO: Complete (client-friendly) java doc for the code below...

/**
 * Convenience class containing various functions to facilitate working with JSON data in Java.
 * Uses inheritence to load and convert JSON Strings to and from POJO Map and Array structures for processing and persistence.
 */
public class JsonUtils {
  private static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);


  public static JSObject mapToJson(Map<String, Object> map){
    JSObject obj = new JSObject();
    if (map != null && map.size() > 0) {
      for (String key : map.keySet()) {
        Object item = map.get(key);
        if (item instanceof List) {
          obj.put(key, mapToJson((List) item));
        } else if (item instanceof Map) {
          obj.put(key, mapToJson((Map) item));
        } else { //TODO: check for null?
          obj.put(key, item);
        }
      }
    }
    return obj;
  }

  public static JSArray mapToJson(List<Object> list){
    JSArray arr = new JSArray();
    if (list != null && list.size() > 0) {
      for (Object item : list) {
        if (item instanceof List) {
          arr.add(mapToJson((List) item));
        } else if (item instanceof Map) {
          arr.add(mapToJson((Map) item));
        } else { //TODO: check for null?
          arr.add(item);
        }
      }
    }
    return arr;
  }

  public static String listToJsonStr(List list) {
    return mapToJsonStr(list);
  }

  public static String mapToJsonStr(List list) {
    JSArray jsArray = mapToJson(list);
    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent("  ");
    writer.setNewline("\n");
    jsArray.compose(writer);
    return writer.toString();
  }

  public static String mapToJsonStr(Map<String, Object> map) {
    JSObject jsObject = mapToJson(map);
    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent("  ");
    writer.setNewline("\n");
    jsObject.compose(writer);
    return writer.toString();
  }


  public static List jsonStrToList(String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      return (List) parser.parse();
    }
    return new ArrayList<Object>();
  }

  public static Map<String, Object> jsonStrToMap(String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      return (Map<String, Object>) parser.parse();
    }
    return new ConcurrentHashMap<String, Object>();
  }

  public static Object jsonStrToObj(String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      return (Object) parser.parse();
    }
    return (Object) new ConcurrentHashMap<String, Object>();
  }


  public static Object loadJsonFile(String filename){
    Object jsonString = getFileContent(filename);
    if (jsonString instanceof String) {
      try{
        return jsonStrToMap((String) jsonString);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return null;
  }


    public static synchronized Map<String, Object> evaluationScenarioDecisionToMap(IEvaluationScenarioDecision evalResult) {
      Map<String, Object> eval = new ConcurrentHashMap<String, Object>();
      eval.put("Information", evalResult.getInformation());
      eval.put("Scenario", evalResult.getScenario());
      ArrayList<Map<String, Object>> decisions = new ArrayList<Map<String, Object>>();
      for (IEvaluationDecision decision: evalResult.getDecisions()){
        Map<String, Object> decisionMap = new HashMap<String, Object>();
        decisionMap.put("Decision",decision.getDecision() != null ?  decision.getDecision().name() : null);
        decisionMap.put("ReportingSide", decision.getReportingSide() != null? decision.getReportingSide().name(): null);
        decisionMap.put("ReportingQualifier", decision.getReportingQualifier());
        decisionMap.put("Flow", decision.getFlow() != null ? decision.getFlow().name(): null);
        decisionMap.put("ResSide", decision.getResSide() != null ? decision.getResSide().name(): null);
        decisionMap.put("ResAccountType", decision.getResAccountType() != null ? decision.getResAccountType().getName(): null);
        decisionMap.put("NonResSide", decision.getNonResSide() != null ? decision.getNonResSide().name(): null);
        decisionMap.put("NonResAccountType", decision.getNonResAccountType() != null ? decision.getNonResAccountType().getName(): null);
        decisionMap.put("ManualSection", decision.getManualSection());
        decisionMap.put("Category", decision.getCategory() );
        decisionMap.put("NotCategory", decision.getNotCategory());
        ArrayList<String> possibleDrAccs = new ArrayList<String>();
        for (BankAccountType acc: decision.getPossibleCrAccountTypes()){
          possibleDrAccs.add(acc.name());
        }
        decisionMap.put("PossibleDrAccountTypes", possibleDrAccs);
        ArrayList<String> possibleCrAccs = new ArrayList<String>();
        for (BankAccountType acc: decision.getPossibleCrAccountTypes()){
          possibleCrAccs.add(acc.name());
        }
        decisionMap.put("PossibleCrAccountTypes", possibleCrAccs);
        decisionMap.put("ResException", decision.getResException());
        decisionMap.put("NonResException", decision.getNonResException());
        decisionMap.put("AccStatusFilter", decision.getAccStatusFilter());
        decisionMap.put("Context", decision.getContext());
        decisionMap.put("DrSideSwift", decision.getDrSideSwift());
        decisionMap.put("LocationCountry", decision.getLocationCountry());
        decisionMap.put("Reportable", decision.getReportable());
        decisions.add(decisionMap);
      }
      eval.put("Evaluations", decisions);
      return eval;
    }
}
