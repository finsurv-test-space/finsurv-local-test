package artefact;

import org.junit.Test;
import za.co.synthesis.finsurvlocal.FinsurvLocal;


public class FunctionalTest_ArtefactUpdateTest {
    public static String[] channelNames = {"sbZAQPay", "sbZA"};
    public static String localArtefactDirectory = "./producer/api/rules/";
    public static String reportDataStoreInstanceUrl = "http://localhost:8084/report-data-store/";
    public static String reportDataStoreUsername = "";
    public static String reportDataStorePassword = "";
    public static String finsurvReportTemplateDirectory = null;
    public static String rootFinsurvReportTemplate = null;


    @Test
    public void checkArtefactRetrieval(){
        FinsurvLocal finsurv = new FinsurvLocal(localArtefactDirectory, reportDataStoreInstanceUrl, reportDataStoreUsername, reportDataStorePassword,finsurvReportTemplateDirectory,rootFinsurvReportTemplate);
        finsurv.addChannels(channelNames);
        finsurv.waitTillReadyForProcessing();

        //This is to give the background worker time to complete pulling artefacts
        int cntr = 100;
        while(--cntr > 0 ){
            try {
                System.out.print(".");
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
